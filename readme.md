# MetaBrush: if METAFONT had a GUI

**MetaBrush** is an application for creating brush-based fonts. It works
on the level of brush strokes, unlike most font-editors that work with
outline-based fonts.

<p align="center">
  <img src="MetaBrush_screenshot.png" />
</p>

## Installation instructions

`cabal run exe:MetaBrush` will build and launch the application.

You will need certain C libraries to be installed on your system and recognised
by `pkg-config`. See the [Install guide](installation_notes.md) for more information, or use the provided `shell.nix` Nix file (NB: not necessarily up-to-date).

## Tips and tricks

### Change UI size

To increase or decrease the size of the UI, set the `GDK_SCALE` environment
variable. The current default is `setenv GDK_SCALE=2`.

### Theming

You can customise the theme by editing `theme.css`.  
There isn't (yet) a way to switch between themes in the UI; you will need to
relaunch the application for changes to take effect.
