#include <xmmintrin.h>

// Function to set the CSR rounding mode for SSE operations
void set_sse_rounding_mode(unsigned int rounding_mode) {
    unsigned int csr = _mm_getcsr();
    csr &= ~_MM_ROUND_MASK; // Clear existing rounding mode bits
    csr |= rounding_mode;   // Set new rounding mode
    _mm_setcsr(csr);
}
