{-# LANGUAGE ScopedTypeVariables #-}

module Math.Epsilon
  ( epsilon, nearZero )
  where

--------------------------------------------------------------------------------

{-# SPECIALISE epsilon :: Float  #-}
{-# SPECIALISE epsilon :: Double #-}
epsilon :: forall r. RealFloat r => r
epsilon = encodeFloat 1 ( 1 - floatDigits ( 0 :: r ) )

nearZero :: RealFloat r => r -> Bool
nearZero x = abs x < epsilon
