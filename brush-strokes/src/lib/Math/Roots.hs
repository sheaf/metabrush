{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ScopedTypeVariables   #-}

{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Math.Roots
  ( -- * Quadratic solver
    solveQuadratic

    -- * Laguerre method
  , roots, realRoots
  , laguerre, eval, derivative

    -- * Newton–Raphson
  , newtonRaphson

    -- * Modified Halley's method M2
  , halleyM2

    -- * N-dimensional Newton's method
  , newton
  )
  where

-- base
import Control.Monad
  ( unless, when )
import Control.Monad.ST
  ( ST, runST )
import Data.Complex
  ( Complex(..), conjugate, magnitude, realPart, imagPart )
import Data.Fixed
  ( mod' )
import Data.Maybe
  ( mapMaybe )
import GHC.TypeNats
  ( KnownNat )

-- deepseq
import Control.DeepSeq
  ( NFData, force )

-- eigen
import qualified Eigen.Matrix as Eigen
  ( Matrix )
import qualified Eigen.Solver.LA as Eigen
  ( Decomposition(..), solve )

-- fp-ieee
import Numeric.Floating.IEEE.NaN
  ( RealFloatNaN(copySign) )

-- primitive
import Control.Monad.Primitive
  ( PrimMonad(PrimState) )
import Data.Primitive.PrimArray
  ( PrimArray, MutablePrimArray
  , primArrayFromList
  , getSizeofMutablePrimArray, sizeofPrimArray
  , unsafeThawPrimArray, cloneMutablePrimArray
  , shrinkMutablePrimArray, readPrimArray, writePrimArray
  )
import Data.Primitive.Types
  ( Prim )

-- rounded-hw
import Numeric.Rounded.Hardware.Internal
  ( fusedMultiplyAdd )

-- brush-strokes
import Math.Algebra.Dual
import Math.Epsilon
  ( epsilon, nearZero )
import Math.Linear
import Math.Linear.Solve
  ( fromEigen, toEigen, withEigenSem )
import Math.Module
import Math.Monomial

--------------------------------------------------------------------------------

-- | Real solutions to a quadratic equation.
--
-- Coefficients are given in order of increasing degree.
--
-- Implementation taken from https://pbr-book.org/4ed/Utilities/Mathematical_Infrastructure#Quadratic.
solveQuadratic
  :: forall a. RealFloatNaN a
  => a -- ^ constant coefficient
  -> a -- ^ linear coefficient
  -> a -- ^ quadratic coefficient
  -> [ a ]
solveQuadratic c b a
  | isNaN a || isNaN b || isNaN c
  || isInfinite a || isInfinite b || isInfinite c
  = []
  | otherwise
  = case (a == 0, c == 0) of
      -- First, handle all cases in which a or c is zero.

      -- bx = 0
      (True , True )
        | b == 0
        -> [ 0, 0.5, 1 ] -- convention
        | otherwise
        -> [ 0 ]
      -- bx + c = 0
      (True , False)
        | b == 0
        -> [ ]
        | otherwise
        -> [ -c / b ]
      -- ax² + bx = 0
      (False, True )
        | b == 0
        -> [ 0 ]
        | signum a == signum b
        -> [ -b / a, 0 ]
        | otherwise
        -> [ 0, -b / a ]
      -- General case: ax² + bx + c = 0
      (False, False)
        | discr < 0
        -> []
        | otherwise
        -> let rootDiscr = sqrt discr
               q = -0.5 * ( b + copySign rootDiscr b )
               x1 = q / a
               x2 = c / q
            in if x1 > x2
               then [ x2, x1 ]
               else [ x1, x2 ]
        where discr = discriminant a b c
{-# INLINEABLE solveQuadratic #-}
{-# SPECIALISE solveQuadratic :: Float -> Float -> Float -> [ Float ] #-}
{-# SPECIALISE solveQuadratic :: Double -> Double -> Double -> [ Double ] #-}
-- TODO: implement the version from the paper
--   "The Ins and Outs of Solving Quadratic Equations with Floating-Point Arithmetic"
-- which is even more robust.

-- | Kahan's method for computing the discriminant \( b^2 - 4ac \),
-- using a fused multiply-add operation to avoid cancellation in the naive
-- formula (if \( b^2 \) and \( 4ac \) are close).
--
-- From "The Ins and Outs of Solving Quadratic Equations with Floating-Point Arithmetic",
-- (Frédéric Goualard, 2023).
discriminant :: RealFloat a => a -> a -> a -> a
discriminant a b c
  -- b² and 4ac are different enough that b² - 4ac gives a good answer
  | 3 * abs d_naive >= b² - m4ac
  = d_naive
  | otherwise
  = let dp = fma b b -b²
        dq = fma ( 4 * a ) c m4ac
    in d_naive + ( dp - dq )
  where
    b² = b * b
    m4ac = -4 * a * c
    d_naive = b² + m4ac
    fma = fusedMultiplyAdd
{-# INLINEABLE discriminant #-}
{-# SPECIALISE discriminant :: Float -> Float -> Float -> Float #-}
{-# SPECIALISE discriminant :: Double -> Double -> Double -> Double #-}

--------------------------------------------------------------------------------
-- Root finding using Laguerre's method

-- | Find real roots of a polynomial with real coefficients.
--
-- Coefficients are given in order of decreasing degree, e.g.:
--  x² + 7 is given by [ 1, 0, 7 ].
realRoots :: forall a. ( RealFloat a, Prim a, NFData a ) => Int -> [ a ] -> [ a ]
realRoots maxIters coeffs = mapMaybe isReal ( roots epsilon maxIters coeffs )
  where
    isReal :: Complex a -> Maybe a
    isReal ( a :+ b )
      | abs b < epsilon = Just a
      | otherwise       = Nothing

-- | Compute all roots of a polynomial with real coefficients using Laguerre's method and (forward) deflation.
--
-- Polynomial coefficients are given in order of descending degree (e.g. constant coefficient last).
--
-- N.B. The forward deflation process is only guaranteed to be numerically stable
-- if Laguerre's method finds roots in increasing order of magnitude
-- (which this function attempts to do).
roots :: forall a. ( RealFloat a, Prim a, NFData a ) => a -> Int -> [ a ] -> [ Complex a ]
roots eps maxIters coeffs = runST do
  let
    coeffPrimArray :: PrimArray a
    coeffPrimArray = primArrayFromList coeffs
    sz :: Int
    sz = sizeofPrimArray coeffPrimArray
  ( p :: MutablePrimArray s a ) <- unsafeThawPrimArray coeffPrimArray
  let
    go :: Int -> [ Complex a ] -> ST s [ Complex a ]
    go !i rs = do
      -- Start at 0, attempting to find the root with smallest absolute value.
      -- This improves numerical stability of the forward deflation process.
      !r <- force <$> laguerre eps maxIters p 0
      if i <= 2
      then pure ( r : rs )
      else
        -- real root
        if abs ( imagPart r ) < epsilon
        then do
          deflate ( realPart r ) p
          go ( i - 1 ) ( r : rs )
        else do
          deflateConjugatePair r p
          go ( i - 2 ) ( r : conjugate r : rs )
  go sz []

-- | Forward deflation of a polynomial by a root: factors out the root.
--
-- Mutates the input polynomial.
deflate :: forall a m s. ( Num a, Prim a, PrimMonad m, s ~ PrimState m ) => a -> MutablePrimArray s a -> m ()
deflate r p = do
  deg <- subtract 1 <$> getSizeofMutablePrimArray p
  when ( deg >= 2 ) do
    shrinkMutablePrimArray p deg
    let
      go :: a -> Int -> m ()
      go !b !i = unless ( i >= deg ) do
        ai <- readPrimArray p i
        let
          bi :: a
          !bi = ai + r * b
        writePrimArray p i bi
        go bi ( i + 1 )
    a0 <- readPrimArray p 0
    go a0 1

-- | Forward deflation of a polynomial with real coefficients by a pair of complex-conjugate roots.
--
-- Mutates the input polynomial.
deflateConjugatePair :: forall a m s. ( Num a, Prim a, PrimMonad m, s ~ PrimState m ) => Complex a -> MutablePrimArray s a -> m ()
deflateConjugatePair ( x :+ y ) p = do
  deg <- subtract 1 <$> getSizeofMutablePrimArray p
  when ( deg >= 3 ) do
    shrinkMutablePrimArray p ( deg - 1 )
    let
      c1, c2 :: a
      !c1 = 2 * x
      !c2 = x * x + y * y
    a0 <- readPrimArray p 0
    a1 <- readPrimArray p 1
    let
      b1 :: a
      !b1 = a1 + c1 * a0
    writePrimArray p 1 b1
    let
      go :: a -> a -> Int -> m ()
      go !b !b' !i = unless ( i >= deg - 1 ) do
        ai <- readPrimArray p i
        let
          bi :: a
          !bi = ai + c1 * b - c2 * b'
        writePrimArray p i bi
        go bi b ( i + 1 )
    go b1 a0 2

-- | Laguerre's method.
--
-- Does not perform any mutation.
laguerre
  :: forall a m s
  .  ( RealFloat a, Prim a, PrimMonad m, s ~ PrimState m )
  => a                    -- ^ error tolerance
  -> Int                  -- ^ max number of iterations
  -> MutablePrimArray s a -- ^ polynomial
  -> Complex a            -- ^ initial point
  -> m ( Complex a )
laguerre eps maxIters p x0 = do
  p'  <- derivative p
  p'' <- derivative p'
  let
    go :: Int -> Complex a -> m ( Complex a )
    go !iterationsLeft !x = do
      x' <- laguerreStep eps p p' p'' x
      if iterationsLeft <= 1 || magnitude ( x' - x ) < eps
      then pure x'
      else go ( iterationsLeft - 1 ) x'
  go maxIters x0

-- | Take a single step in Laguerre's method.
--
-- Does not perform any mutation.
laguerreStep
  :: forall a m s
  .  ( RealFloat a, Prim a, PrimMonad m, s ~ PrimState m )
  => a                    -- ^ error tolerance
  -> MutablePrimArray s a -- ^ polynomial
  -> MutablePrimArray s a -- ^ first derivative of polynomial
  -> MutablePrimArray s a -- ^ second derivative of polynomial
  -> Complex a            -- ^ initial point
  -> m ( Complex a )
laguerreStep eps p p' p'' x = do
  n  <- fromIntegral @_ @a <$> getSizeofMutablePrimArray p
  px <- eval p x
  if magnitude px < eps
  then pure x
  else do
    p'x  <- eval p'  x
    p''x <- eval p'' x
    let
      g     = p'x / px
      g²    = g * g
      h     = g² - p''x / px
    --m     = min n . max 1 . fromIntegral . sanitise . realPart $ negate ( log p'x / log g )
      delta = sqrt $ ( n - 1 ) *: ( n *: h - g² ) -- sqrt $ ( ( n - m ) / m ) *: ( n *: h - g² )
      gp    = g + delta
      gm    = g - delta
      denom
        | magnitude gm > magnitude gp
        = recip gm
        | otherwise
        = recip gp
    pure $ x - n *: denom

  where
    (*:) :: a -> Complex a -> Complex a
    r *: (u :+ v) = ( r * u ) :+ ( r * v )

{-
    sanitise :: a -> Int
    sanitise u
      | isNaN u || isInfinite u
      = 1
      | otherwise
      = round u
-}

-- | Evaluate a polynomial with real coefficients at a complex number.
--
-- Does not perform any mutation.
eval
  :: forall a m s
  . ( RealFloat a, Prim a, PrimMonad m, s ~ PrimState m )
  => MutablePrimArray s a -> Complex a -> m ( Complex a )
eval p x = do
  n <- getSizeofMutablePrimArray p
  let
    go :: Complex a -> Int -> m ( Complex a )
    go !a !i
      | i >= n
      = pure a
      | otherwise
      = do
        b <- readPrimArray p i
        go ( ( b :+ 0 ) + x * a ) ( i + 1 )
  an <- readPrimArray p 0
  go ( an :+ 0 ) 1

-- | Derivative of a polynomial.
--
-- Does not mutate its argument.
derivative
  :: forall a m s
  . ( Num a, Prim a, PrimMonad m, s ~ PrimState m )
  => MutablePrimArray s a -> m ( MutablePrimArray s a )
derivative p = do
  deg <- subtract 1 <$> getSizeofMutablePrimArray p
  p' <- cloneMutablePrimArray p 0 deg
  let
    go :: Int -> m ()
    go !i = unless ( i >= deg - 1 ) do
      a <- readPrimArray p' i
      writePrimArray p' i ( a * fromIntegral ( deg - i ) )
      go ( i + 1 )
  go 0
  pure p'

{-# SPECIALISE maxRealFloat :: Float #-}
{-# SPECIALISE maxRealFloat :: Double #-}
maxRealFloat :: forall r. RealFloat r => r
maxRealFloat = encodeFloat m n
  where
    !b = floatRadix @r 0
    !e = floatDigits @r 0
    !(_, !e') = floatRange @r 0
    !m = b ^ e - 1
    !n = e' - e

{-
for some reason GHC isn't able to optimise maxRealFloat @Double into

maxDouble :: Double
maxDouble = D# 1.7976931348623157e308##
-}

--------------------------------------------------------------------------------
-- Newton–Raphson

-- Newton–Raphson implementation taken from Boost C++ library:
-- https://github.com/boostorg/math/blob/0dc6a70caa6bbec2b6ae25eede36c430f0ccae13/include/boost/math/tools/roots.hpp#L217
{-# SPECIALISE
  newtonRaphson
    :: Word -> Int -> Double -> ( Double -> (# Double, Double #) ) -> Double -> (Maybe Double, [(Double,Double,Double)])
  #-}
{-# INLINEABLE newtonRaphson #-}
newtonRaphson :: ( RealFloat r, Show r )
              => Word               -- ^ maximum number of iterations
              -> Int                -- ^ desired digits of precision
              -> r                  -- ^ period
              -> ( r -> (# r, r #) ) -- ^ function and its derivative
              -> r                  -- ^ initial guess
              -> (Maybe r, [(r,r,r)])
newtonRaphson maxIters digits ω f x0 =
  doNewtonRaphson f maxIters factor ω 0 x0 maxRealFloat maxRealFloat
  where
    !factor = encodeFloat 1 ( 1 - digits )

doNewtonRaphson :: ( RealFrac r, Ord r, Show r )
                => ( r -> (# r, r #) )
                -> Word
                -> r
                -> r
                -> r
                -> r
                -> r -> r
                -> (Maybe r, [(r,r,r)])
doNewtonRaphson f maxIters factor ω f_x_prev x δ1 δ2 =
  go [] f_x_prev 0 x δ1 δ2
  where
  go prev_acc f_x_prev !iters !x !δ1 !δ2 =
    let (# f_x, f'_x #) = f x
        acc = ((x, f_x, f'_x) : prev_acc)
    in if
        | f_x == 0
        -> (Just x, acc)
        | ( new_x, δ, δ1 ) <- newtonRaphsonStep ω f_x_prev x f_x f'_x δ1 δ2
        -> if
            | abs δ <= abs ( new_x * factor )
            -> (Just x, acc)
            | iters >= maxIters
            -> (Nothing, acc)
            | otherwise
            -> go acc f_x ( iters + 1 ) new_x δ δ1

newtonRaphsonStep :: ( RealFrac r, Ord r, Show r )
                  => r
                  -> r
                  -> r
                  -> r -> r
                  -> r -> r
                  -> ( r, r, r )
newtonRaphsonStep ω f_x_prev x f_x f'_x δ1 δ2
  | δ <-
     if | f'_x == 0
        -> zeroDerivativeStep ω f_x_prev x f_x δ1
        | otherwise
        -> f_x / f'_x
  , ( δ, δ1 ) <-
     if | abs ( δ * δ ) > abs δ2
        -> bisectStep ω x δ
        | otherwise
        -> ( δ, δ1 )
  , let !new_x = ( x - δ ) `mod'` ω
  = ( new_x, δ, δ1 )

-- Handle \( f'(x_0) = 0 \).
zeroDerivativeStep :: ( Fractional r, Ord r, Show r )
                   => r
                   -> r
                   -> r
                   -> r -> r
                   -> r
zeroDerivativeStep ω f_x_prev x f_x δ
  -- Handle zero derivative on first iteration.
  | f_x_prev == 0
  , x_prev <- if x <= 0.5 * ω then ω else 0
  , δ <- x_prev - x
  = finish f_x_prev δ
  | otherwise
  = finish f_x_prev δ
  where
    finish f_x_prev δ
      | signum f_x_prev * signum f_x < 0
      = if δ < 0 then 0.5 * x else 0.5 * ( x - ω )
      | otherwise
      = if δ < 0 then 0.5 * ( x - ω ) else 0.5 * x

-- Handle failure of convergence in the past two steps.
bisectStep :: ( Fractional r, Ord r, Show r ) => r -> r -> r -> ( r, r )
bisectStep ω x δ
  | let !shift = if δ > 0 then 0.5 * x else 0.5 * ( x - ω )
  , δ <- if x /= 0 && ( abs shift > abs x )
         then signum δ * abs x * 1.1
         else shift
  = ( δ, 3 * δ )

--------------------------------------------------------------------------------
-- Halley's method


-- | Take a single step in the M2 modified Halley method.
--
-- Taken from @Some variants of Halley’s method with memory and their applications for solving several chemical problems@
-- by A. Cordero, H. Ramos & J.R. Torregrosa, J Math Chem 58, 751–774 (2020).
--
-- @https://doi.org/10.1007/s10910-020-01108-3@
halleyM2Step :: ( RealFloat a, Show a )
             => a -> a -> (# a, (# a, a #) #) -> a -> (# a, (# a, a #) #) -> a
halleyM2Step ω δ_nm1 (# x_nm1, (# f_nm1, f'_nm1 #) #) δ_n (# x_n, (# f_n, f'_n #) #)
  | nearZero ( x_n - x_nm1 )
  -- Simple Newton step
  = let ( nxt, _, _ ) = newtonRaphsonStep ω f_nm1 x_n f_n f'_n δ_n δ_nm1
    in nxt
  -- TODO: handle zero derivative?
  | nearZero num && nearZero denom
  = 0.001 * signum num * signum denom
  | nearZero num
  = num
  | otherwise
  = num / denom
  where
    u = f_n * f_nm1 * (f_n - f_nm1)
    dx = x_n - x_nm1
    g1 = f_nm1 ^ ( 2 :: Int ) * f'_n
    g2 = f_n ^ ( 2 :: Int ) * f'_nm1
    num = (x_n + x_nm1) * u - dx * ( g1 * x_n + g2 * x_nm1)
    denom = 2 * u - dx * ( g1 + g2 )
{-# SPECIALISE halleyM2Step
  :: Double -> Double -> (# Double, (# Double, Double #) #) -> Double -> (# Double, (# Double, Double #) #) -> Double #-}
{-# INLINEABLE halleyM2Step #-}

{-# SPECIALISE
  halleyM2
    :: Word -> Int -> Double -> ( Double -> (# Double, Double #) ) -> Double -> (Maybe Double, [(Double, Double, Double)])
  #-}
{-# INLINEABLE halleyM2 #-}
halleyM2 :: forall r
         .  ( RealFloat r, Show r )
         => Word              -- ^ maximum number of iterations
         -> Int               -- ^ desired digits of precision
         -> r                 -- ^ period
         -> ( r -> (# r, r #) ) -- ^ function and its derivative
         -> r                 -- ^ initial guess
         -> (Maybe r, [(r,r,r)])
halleyM2 maxIters digits ω f x0 =
  let
    y0 = (# x0, df_x0 #)
    !df_x0@(# f_x0, f'_x0 #) = f x0
    -- Start off with a Newton step to get two initial x values.
    ( x1, δ1, δ2 ) =
      newtonRaphsonStep ω 0 x0 f_x0 f'_x0 maxRealFloat maxRealFloat
    y1 = (# x1, f x1 #)
  in go [] 0 δ1 y0 δ2 y1
  where
    !factor = encodeFloat 1 ( 1 - digits )
    go :: [(r,r,r)] -> Word -> r -> (# r, (# r, r #) #) -> r -> (# r, (# r, r #) #) -> (Maybe r, [(r,r,r)])
    go prev_acc i δ_nm1 y_nm1 δ_nm y_n@(# x_n, (# f_x_n, f'_x_n #) #) =
      let acc = (x_n, f_x_n, f'_x_n) : prev_acc
          x_np1 = halleyM2Step ω δ_nm1 y_nm1 δ_nm y_n
      in if | i >= maxIters
            || abs ( x_np1 - x_n ) < abs ( x_n * factor )
            -> (Just x_np1, acc)
            | otherwise
            -> go acc (i+1) δ_nm y_n (x_np1 - x_n) (# x_np1, f x_np1 #)

--------------------------------------------------------------------------------

-- | @n@-dimensional Newton's method.
newton
  :: forall n d
  .  ( KnownNat n, KnownNat d
     , Show ( ℝ n ), Show ( ℝ d )
     , NFData ( ℝ n )
     , Module Double ( T ( ℝ n ) )
     , MonomialBasis ( D 1 n ), Deg ( D 1 n ) ~ 1, Vars ( D 1 n ) ~ n
     , Representable Double ( ℝ d )
     , Representable Double ( ℝ n )
     )
  => ( ℝ n -> D 1 n ( ℝ d ) )
      -- ^ equations
  -> ℝ n
     -- ^ initial guess
  -> ℝ n
newton f x0 = go 0 x0
  where
    maxIters :: Int
    maxIters = 40
    relError, absError :: Double
    relError = 1e-9
    absError = 1e-11

    go :: Int -> ℝ n -> ℝ n
    go !i !x
      | i >= maxIters
      = x
      | norm ( unT δ ) < absError + relError * norm x
      = x'
      | otherwise
      = go ( i + 1 ) x'
      where
        ( _norm_fx, δ ) = newtonStep x
        x' = unT $ T x ^-^ δ

    norm :: forall i. Representable Double ( ℝ i ) => ℝ i -> Double
    norm x =
      sqrt $ sum ( ( \ x -> x * x ) <$> coordinates x )
    {-# INLINEABLE norm #-}

    newtonStep :: ℝ n -> ( Double, T ( ℝ n ) )
    newtonStep x =
      let df_x = f x
          f_x :: ℝ d
          f_x = df_x `monIndex` zeroMonomial
          j_x :: Vec n ( ℝ d )
          j_x = fmap ( \ i -> df_x `monIndex` linearMonomial i ) ( universe @n )
          δ :: ℝ n
          !δ = withEigenSem $
            ( \ ( Vec cols ) -> case cols of [ !c ] -> c; _ -> error "internal error in Newton's method" ) $
              fromEigen $
                Eigen.solve Eigen.JacobiSVD
                  ( toEigen j_x :: Eigen.Matrix d n Double )
                  ( toEigen ( Vec [ f_x ] ) :: Eigen.Matrix d 1 Double )
      in ( norm f_x, T δ )

{-# INLINEABLE newton #-}
{-# SPECIALISE newton :: ( ℝ 1 -> D 1 1 ( ℝ 1 ) ) -> ℝ 1 -> ℝ 1 #-}
{-# SPECIALISE newton :: ( ℝ 1 -> D 1 1 ( ℝ 2 ) ) -> ℝ 1 -> ℝ 1 #-}
{-# SPECIALISE newton :: ( ℝ 1 -> D 1 1 ( ℝ 3 ) ) -> ℝ 1 -> ℝ 1 #-}

{-# SPECIALISE newton :: ( ℝ 2 -> D 1 2 ( ℝ 2 ) ) -> ℝ 2 -> ℝ 2 #-}
{-# SPECIALISE newton :: ( ℝ 2 -> D 1 2 ( ℝ 3 ) ) -> ℝ 2 -> ℝ 2 #-}

-- TODO: implement the W4 method?