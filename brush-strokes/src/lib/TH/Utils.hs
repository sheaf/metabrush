{-# LANGUAGE TemplateHaskell #-}

module TH.Utils where

-- template-haskell
import Language.Haskell.TH
  ( CodeQ )

-- brush-strokes
import Math.Ring ( Ring )
import qualified Math.Ring as Ring

--------------------------------------------------------------------------------

foldQ :: CodeQ ( a -> a -> a ) -> CodeQ a -> [ CodeQ a ] -> CodeQ a
foldQ _ a0 []  = a0
foldQ _ _  [a] = a
foldQ f a0 (a:as) = [|| $$f $$a $$( foldQ f a0 as ) ||]

powQ :: Ring a => CodeQ a -> Word -> CodeQ a
powQ _ 0 = [|| Ring.fromInteger ( 1 :: Integer ) ||]
powQ x 1 = x
powQ x n = [|| $$x Ring.^ ( n :: Word ) ||]
