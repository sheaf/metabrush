{-# LANGUAGE AllowAmbiguousTypes   #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RebindableSyntax      #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE UndecidableInstances  #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Main where

-- base
import Prelude
  hiding
    ( Num(..), Fractional(..), Floating(..), (^), (/)
    , fromInteger, fromRational
    )
import qualified Prelude
import GHC.Generics
  ( Generic )

-- acts
import Data.Act
  ( Torsor(..) )

-- containers
import qualified Data.Sequence as Seq
  ( fromList, index )

-- generic-lens
import Data.Generics.Product.Typed
  ( HasType )

-- brush-strokes
import Math.Algebra.Dual
import qualified Math.Bezier.Quadratic as Quadratic
import qualified Math.Bezier.Cubic     as Cubic
import Math.Bezier.Spline
import Math.Bezier.Stroke
  ( brushStrokeData, pathAndUsedParams )
import Math.Bezier.Stroke.EnvelopeEquation
  ( HasBézier(..), StrokeDatum(..) )
import Math.Differentiable
  ( I )
import Math.Linear
  ( ℝ(..), T(..), Segment(..)
  , Fin(..)
  , Representable(..)
  )
import Math.Module
  ( Module(..), Cross(..)
  , lerp
  )
import Math.Ring
  ( AbelianGroup(..), Ring(..)
  , Field(..), Floating(..), Transcendental(..)
  , ifThenElse
  )

-- brush-strokes:inspect-cusps
import Math.Interval.Abstract

--------------------------------------------------------------------------------

main :: IO ()
main = print ellipseTest

data PointData params
  = PointData
  { pointCoords :: !( ℝ 2 )
  , brushParams :: !params
  }
  deriving stock ( Show, Generic )

outlineFunction
  :: forall {t} (i :: t) brushParams
  .  ( Show brushParams
     , HasType ( ℝ 2 ) ( PointData brushParams )
     , Cross  ( I i Double ) ( T ( I i 2 ) )
     , Module ( I i Double ) ( T ( I i brushParams ) )
     , Module Double (T brushParams)
     , Torsor (T brushParams) brushParams
     , Representable Double brushParams
     , Torsor ( T ( I i 2 ) ) ( I i 2 )
     , Torsor ( T ( I i brushParams ) ) ( I i brushParams )
     , HasChainRule ( I i Double ) 3 ( I i 1 )
     , HasChainRule ( I i Double ) 3 ( I i brushParams )
     , Traversable ( D 3 brushParams )
     , HasBézier 3 i
     )
  => ( I i Double -> I i 1 )
  -> ( I i 1 -> I i Double )
  -> ( forall a. a -> I i a )
  -> C 3 ( I i brushParams ) ( Spline Closed () ( I i 2 ) )
      -- ^ brush shape
  -> Int -- ^ brush segment index to consider
  -> PointData brushParams -> Curve Open () ( PointData brushParams )
      -- ^ stroke path
  -> ( I i 1 -> I i 1 -> StrokeDatum 3 i )
outlineFunction co1 co2 single brush brush_index sp0 crv = strokeData

  where
    path   :: C 3 ( I i 1 ) ( I i 2 )
    params :: C 3 ( I i 1 ) ( I i brushParams )
    (path, params) =
      pathAndUsedParams @3 @i co2 single brushParams sp0 crv

    strokeData :: I i 1 -> I i 1 -> StrokeDatum 3 i
    strokeData =
      fmap ( `Seq.index` brush_index ) $
        brushStrokeData @3 @brushParams co1 co2 path params brush


boo :: ( HasType ( ℝ 2 ) ( PointData brushParams )
       , Show brushParams
       , Representable Double brushParams
       , Module Double ( T brushParams )
       , Module ( AI Double ) ( T ( AI brushParams ) )
       , Torsor ( T brushParams ) brushParams
       , Torsor ( T ( AI brushParams ) ) ( AI brushParams )
       , HasChainRule ( AI Double ) 3 ( AI brushParams )
       , Traversable ( D 3 brushParams )
       )
    => C 3 ( AI brushParams ) ( Spline Closed () ( AI ( ℝ 2 ) ) )
    -> Int
    -> PointData brushParams -> Curve Open () ( PointData brushParams )
    -> StrokeDatum 3 AI
boo x i y z
  = outlineFunction @AI
       Coerce1 Coerce2 (Pt . Val)
       x i y z
       (Coerce1 $ Pt $ Var "t") (Coerce1 $ Pt $ Var "s")

ellipseTest :: StrokeDatum 3 AI
ellipseTest =
  boo ( ellipseBrush ( Pt . Val ) scaleI ) 1
    ( mkPoint ( ℝ2 0 0 ) 10 25 0 )
    ( LineTo { curveEnd = NextPoint $ mkPoint ( ℝ2 100 0 ) 15 40 pi
             , curveData = () } )
  where
    mkPoint :: ℝ 2 -> Double -> Double -> Double -> PointData ( ℝ 3 )
    mkPoint pt a b phi = PointData pt ( ℝ3 a b phi )
    scaleI :: Double -> AI Double -> AI Double
    scaleI x iv = Scale (Pt $ Val x) iv

--------------------------------------------------------------------------------
-- EDSL for inspection

instance HasChainRule ( AI Double ) 3 ( AI ( ℝ 1 ) ) where

  konst x = D31 x o o o
    where o = T $ fromInteger (0 :: Integer)
  value ( D31 { _D31_v = x } ) = x
  linearD f x = D31 (f x) (T $ f $ Pt $ Val $ ℝ1 1) o o
    where o = origin

  chain :: forall w. Module ( AI Double ) ( T w ) => D3𝔸1 ( AI ( ℝ 1 ) ) -> D3𝔸1 w -> D3𝔸1 w
  chain !df !dg =
    let !o = origin @( AI Double ) @( T w )
        !p = (^+^)  @( AI Double ) @( T w )
        !s = (^*)   @( AI Double ) @( T w )
    in $$( chainRule1NQ
         [|| o ||] [|| p ||] [|| s ||]
         [|| df ||] [|| dg ||] )

instance HasChainRule ( AI Double ) 3 ( AI ( ℝ 2 ) ) where

  konst x = D32 x o o o o o o o o o
    where o = T $ fromInteger (0 :: Integer)
  value ( D32 { _D32_v = x } ) = x
  linearD f x =
    D32 (f x)
      (T $ f $ Pt $ Val $ ℝ2 1 0)
      (T $ f $ Pt $ Val $ ℝ2 0 1)
      o o o o o o o
    where o = origin

  chain :: forall w. Module ( AI Double ) ( T w ) => D3𝔸1 ( AI ( ℝ 2 ) ) -> D3𝔸2 w -> D3𝔸1 w
  chain !df !dg =
    let !o = origin @( AI Double ) @( T w )
        !p = (^+^)  @( AI Double ) @( T w )
        !s = (^*)   @( AI Double ) @( T w )
    in $$( chainRule1NQ
         [|| o ||] [|| p ||] [|| s ||]
         [|| df ||] [|| dg ||] )

instance HasChainRule ( AI Double ) 3 ( AI ( ℝ 3 ) ) where

  konst x = D33 x o o o o o o o o o o o o o o o o o o o
    where o = T $ fromInteger (0 :: Integer)
  value ( D33 { _D33_v = x } ) = x
  linearD f x =
    D33 (f x)
      (T $ f $ Pt $ Val $ ℝ3 1 0 0)
      (T $ f $ Pt $ Val $ ℝ3 0 1 0)
      (T $ f $ Pt $ Val $ ℝ3 0 0 1)
      o o o o o o o o o o o o o o o o
    where o = origin

  chain :: forall w. Module ( AI Double ) ( T w ) => D3𝔸1 ( AI ( ℝ 3 ) ) -> D3𝔸3 w -> D3𝔸1 w
  chain !df !dg =
    let !o = origin @( AI Double ) @( T w )
        !p = (^+^)  @( AI Double ) @( T w )
        !s = (^*)   @( AI Double ) @( T w )
    in $$( chainRule1NQ
         [|| o ||] [|| p ||] [|| s ||]
         [|| df ||] [|| dg ||] )

instance HasChainRule ( AI Double ) 3 ( AI ( ℝ 4 ) ) where

  konst x = D34 x o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o
    where o = T $ fromInteger (0 :: Integer)
  value ( D34 { _D34_v = x } ) = x
  linearD f x =
    D34 (f x)
      (T $ f $ Pt $ Val $ ℝ4 1 0 0 0)
      (T $ f $ Pt $ Val $ ℝ4 0 1 0 0)
      (T $ f $ Pt $ Val $ ℝ4 0 0 1 0)
      (T $ f $ Pt $ Val $ ℝ4 0 0 0 1)
      o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o
    where o = origin

  chain :: forall w. Module ( AI Double ) ( T w ) => D3𝔸1 ( AI ( ℝ 4 ) ) -> D3𝔸4 w -> D3𝔸1 w
  chain !df !dg =
    let !o = origin @( AI Double ) @( T w )
        !p = (^+^)  @( AI Double ) @( T w )
        !s = (^*)   @( AI Double ) @( T w )
    in $$( chainRule1NQ
         [|| o ||] [|| p ||] [|| s ||]
         [|| df ||] [|| dg ||] )

instance HasBézier 3 AI where
  line co ( Segment a b :: Segment b ) =
    D \ ( co -> t ) ->
      D31 ( lerp @( T b ) t a b )
          ( a --> b )
          origin
          origin

  bezier2 co ( bez :: Quadratic.Bezier b ) =
    D \ ( co -> t ) ->
      D31 ( EvalBez2 bez t )
          ( Quadratic.bezier'  bez t )
          ( Quadratic.bezier'' bez )
          origin

  bezier3 co ( bez :: Cubic.Bezier b ) =
    D \ ( co -> t ) ->
      D31 ( EvalBez3 bez t )
          ( T $ EvalBez2 ( fmap unT $ Cubic.derivative ( fmap T bez ) ) t )
          ( Cubic.bezier''  bez t )
          ( Cubic.bezier''' bez )

--------------------------------------------------------------------------------
-- Brushes (TODO copied from Calligraphy.Brushes)

κ :: Double
κ = 0.5519150244935105707435627227925

circleSpline :: forall {t} (i :: t) k u v
             .  Applicative ( D k ( Dim u ) )
             => ( Double -> Double -> D k ( Dim u ) ( I i v ) )
             -> D k ( Dim u ) ( Spline 'Closed () ( I i v ) )
circleSpline p = sequenceA $
  Spline { splineStart  = p 1 0
         , splineCurves = ClosedCurves crvs lastCrv }
  where
    crvs = Seq.fromList
      [ Bezier3To ( p  1  κ ) ( p  κ  1 ) ( NextPoint ( p  0  1 ) ) ()
      , Bezier3To ( p -κ  1 ) ( p -1  κ ) ( NextPoint ( p -1  0 ) ) ()
      , Bezier3To ( p -1 -κ ) ( p -κ -1 ) ( NextPoint ( p  0 -1 ) ) ()
      ]
    lastCrv =
        Bezier3To ( p  κ -1 ) ( p  1 -κ ) BackToStart ()
{-# INLINE circleSpline #-}

ellipseBrush :: forall k irec
             . ( irec ~ AI ( ℝ 3 )
               , Module
                  ( D k irec ( AI Double ) )
                  ( D k irec ( AI ( ℝ 2 ) ) )
               , Module ( AI Double ) ( T ( AI Double ) )
               , HasChainRule ( AI Double ) k irec
               , Representable ( AI Double ) irec
               , Applicative ( D k irec )
               , Transcendental ( D k irec ( AI Double ) )
               )
              => ( forall a. a -> AI a )
              -> ( Double -> AI Double -> AI Double )
              -> C k irec ( Spline 'Closed () ( AI ( ℝ 2 ) ) )
ellipseBrush mkI scaleI =
  D \ params ->
    let a, b, phi :: D k irec ( AI Double )
        a   = runD ( var @_ @k ( Fin 1 ) ) params
        b   = runD ( var @_ @k ( Fin 2 ) ) params
        phi = runD ( var @_ @k ( Fin 3 ) ) params
        mkPt :: Double -> Double -> D k irec ( AI ( ℝ 2 ) )
        mkPt x y
          = let !x' = a `scaledBy` x
                !y' = b `scaledBy` y
-- {-
                !c  = cos phi
                !s  = sin phi
            in   ( x' * c - y' * s ) *^ e_x
             ^+^ ( x' * s + y' * c ) *^ e_y
-- -}
{-
                !r  = sqrt $ x' ^ 2 + y' ^ 2
                !arctgt = atan ( y' / x' )
                -- a and b are always strictly positive, so we can compute
                -- the quadrant using only x and y, which are constants.
                !theta
                  | x > 0
                  = arctgt
                  | x < 0
                  = if y >= 0 then arctgt + pi else arctgt - pi
                  | otherwise
                  = if y >= 0 then 0.5 * pi else -0.5 * pi
                !phi' = phi + theta
            in
                ( r * cos phi' ) *^ e_x
            ^+^ ( r * sin phi' ) *^ e_y
-}

    in circleSpline @AI @k @( ℝ 3 ) @( ℝ 2 ) mkPt
  where
    e_x, e_y :: D k irec ( AI ( ℝ 2 ) )
    e_x = pure $ mkI $ ℝ2 1 0
    e_y = pure $ mkI $ ℝ2 0 1

    scaledBy :: D k irec ( AI Double ) -> Double -> D k irec ( AI Double )
    scaledBy d k = fmap ( scaleI k ) d
{-# INLINEABLE ellipseBrush #-}
