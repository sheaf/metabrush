{-# LANGUAGE AllowAmbiguousTypes  #-}
{-# LANGUAGE PolyKinds            #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TemplateHaskell      #-}
{-# LANGUAGE UndecidableInstances #-}

module Math.Interval.Abstract where

-- base
import Prelude hiding
  ( Num(..), Fractional(..), Floating(..), (^)
  , unzip )
import qualified Prelude
  ( Num(..), Fractional(..) )
import Data.Coerce
  ( coerce )
import Data.Dynamic
  ( Dynamic, toDyn, fromDynamic, dynTypeRep )
import Data.Foldable
  ( toList )
import Data.List
  ( intersperse )
import Data.List.NonEmpty
  ( unzip )
import Data.Proxy
  ( Proxy(..) )
import Data.Typeable
  ( Typeable, typeRep )
import GHC.Show
  ( showSpace, showCommaSpace )
import GHC.TypeNats
  ( KnownNat )

-- acts
import Data.Act
  ( Act(..), Torsor(..) )

-- containers
import Data.Map
  ( Map )
import qualified Data.Map.Strict as Map
  ( empty, insertLookupWithKey, lookup )

-- groups
import Data.Group
  ( Group(..) )

-- brush-strokes
import Math.Algebra.Dual
import qualified Math.Bezier.Quadratic as Quadratic
import qualified Math.Bezier.Cubic     as Cubic
import Math.Bezier.Stroke.EnvelopeEquation
  ( StrokeDatum(..) )
import Math.Differentiable
  ( I )
import Math.Interval
import Math.Linear
  ( ℝ(..), T(..)
  , Vec(..), (!)
  , Fin(..), RepDim, Representable(..), RepresentableQ(..)
  )
import Math.Module
  ( Module(..), Cross(..) )
import Math.Ring
  ( AbelianGroup(..), Ring(..), Field(..)
  , Floating(..), Transcendental(..)
  , ViaPrelude(..)
  )

--------------------------------------------------------------------------------
-- EDSL for inspection

type instance D k ( AI a ) = D k ( 𝕀 a )
  -- TODO: I think we want a custom representation for D k (AI a),
  -- so that we can recover sharing.

type instance I AI a = AI a

-- | A value: a constant, or a variable name.
data Value a where
  Val :: a -> Value a
  Var :: ( Typeable a, Eq a, Show a ) => String -> Value a

-- | A scalar or a vector.
data SV a where
  Scalar :: SV Double
  Vector :: IsVec v => SV v

instance Show a => Show ( Value a ) where
  show (Val v) = show v
  show (Var v) = v

-- | An abstract interval.
data AI a where
  Pt       :: Value a -> AI a
  Ival     :: Value a -> Value a -> AI a
  (:+:)    :: SV a -> AI a -> AI a -> AI a
  (:-:)    :: SV a -> AI a -> AI a -> AI a
  Negate   :: SV a -> AI a -> AI a
  (:*:)    :: AI Double -> AI Double -> AI Double
  (:^:)    :: AI Double -> Word -> AI Double
  Recip    :: AI Double -> AI Double
  Scale    :: IsVec v => AI Double -> AI v -> AI v
  Cross    :: AI ( ℝ 2 ) -> AI ( ℝ 2 ) -> AI Double
  Sqrt     :: AI Double -> AI Double
  Cos      :: AI Double -> AI Double
  Sin      :: AI Double -> AI Double
  Atan     :: AI Double -> AI Double
  Pi       :: AI Double
  Coerce1  :: AI Double -> AI ( ℝ 1 )
  Coerce2  :: AI ( ℝ 1 ) -> AI Double
  EvalBez2 :: Quadratic.Bezier (AI a) -> AI Double -> AI a
  EvalBez3 :: Cubic.Bezier (AI a) -> AI Double -> AI a
  Tabulate :: ( KnownNat n, Eq ( ℝ n ), Show ( ℝ n ), Representable Double ( ℝ n ) ) => Vec n ( AI Double ) -> AI ( ℝ n )
  Index    :: ( KnownNat n, Eq ( ℝ n ), Show ( ℝ n ), Representable Double ( ℝ n ) ) => AI ( ℝ n ) -> Fin n -> AI Double

instance ( Show a, Eq a, Typeable a ) => Show ( AI a ) where
  showsPrec prec ai = case fst $ normalise Map.empty ai of
    Pt v                 -> showsPrec prec v
    Ival lo hi           -> showString "[" . showsPrec 0 lo . showCommaSpace . showsPrec 0 hi . showString "]"
    (:+:) Scalar iv1 iv2 -> showParen (prec >  6) $ showsPrec  6 iv1 . showString " + " . showsPrec  7 iv2
    (:+:) Vector iv1 iv2 -> showParen (prec >  6) $ showsPrec  6 iv1 . showString " + " . showsPrec  7 iv2
    (:-:) Scalar iv1 iv2 -> showParen (prec >  6) $ showsPrec  6 iv1 . showString " - " . showsPrec  7 iv2
    (:-:) Vector iv1 iv2 -> showParen (prec >  6) $ showsPrec  6 iv1 . showString " - " . showsPrec  7 iv2
    iv1 :*: iv2          -> showParen (prec >  7) $ showsPrec  7 iv1 . showString " * " . showsPrec  8 iv2
    Cross iv1 iv2        -> showParen (prec >  7) $ showsPrec  8 iv1 . showString " × " . showsPrec  8 iv2
    iv :^: n             -> showParen (prec >  8) $ showsPrec  9 iv  . showString " ^ " . showsPrec  8 n
    Scale k v            -> showParen (prec >  9) $ showsPrec 10  k  . showString " * " . showsPrec 10 v
    Index iv ( Fin i )   -> showParen (prec >  9) $ showsPrec  9 iv  . showString " ! " . showsPrec 10 i
    Negate Scalar iv     -> showParen (prec > 10) $ showString " -"  . showsPrec 11 iv
    Negate Vector iv     -> showParen (prec > 10) $ showString " -"  . showsPrec 11 iv
    Recip iv             -> showParen (prec > 10) $ showString " 1 / "   . showsPrec 11 iv
    Coerce1 iv           -> showsPrec prec iv
    Coerce2 iv           -> showsPrec prec iv
    EvalBez2 bez t       -> showParen (prec > 10) $ showString "bez2" . showSpace . showsPrec 11 t . showSpace . showString "[" . showBez2 bez . showString "]"
    EvalBez3 bez t       -> showParen (prec > 10) $ showString "bez3" . showSpace . showsPrec 11 t . showSpace . showString "[" . showBez3 bez . showString "]"
    Sqrt iv              -> showParen (prec > 10) $ showString "sqrt "    . showsPrec 11 iv
    Cos iv               -> showParen (prec > 10) $ showString "cos "     . showsPrec 11 iv
    Sin iv               -> showParen (prec > 10) $ showString "sin "     . showsPrec 11 iv
    Atan iv              -> showParen (prec > 10) $ showString "atan "    . showsPrec 11 iv
    Pi                   -> showString "pi"
    Tabulate vs          -> showString "(" . foldr (.) id ( intersperse showCommaSpace fields ) . showString ")"
      where
        fields :: [ ShowS ]
        fields = [ showsPrec 0 v
                 | v <- toList vs
                 ]

showBez2 (Quadratic.Bezier p0 p1 p2   ) = foldr (.) id ( intersperse showCommaSpace $ map (showsPrec 0) [p0, p1, p2] )
showBez3 (    Cubic.Bezier p0 p1 p2 p3) = foldr (.) id ( intersperse showCommaSpace $ map (showsPrec 0) [p0, p1, p2, p3] )

infixl 6 :+:
infixl 6 :-:
infixl 7 :*:
infixr 8 :^:

index_maybe :: ( Show ( ℝ n ), Representable Double ( ℝ n ) )
            => AI ( ℝ n ) -> Fin n -> Maybe ( AI Double )
index_maybe expr i = case expr of
  Pt ( Val v )
    -> Just $ Pt $ Val $ v `index` i
  Ival ( Val lo ) ( Val hi )
    -> Just $ Ival ( Val $ lo `index` i ) ( Val $ hi `index` i )
  (:+:) Vector iv1 iv2
    | Just v1 <- iv1 `index_maybe` i
    , Just v2 <- iv2 `index_maybe` i
    -> Just $ v1 + v2
  (:-:) Vector iv1 iv2
    | Just v1 <- iv1 `index_maybe` i
    , Just v2 <- iv2 `index_maybe` i
    -> Just $ v1 - v2
  Negate Vector iv
    | Just v <- iv `index_maybe` i
    -> Just $ negate v
  Scale k iv
    | Just v <- iv `index_maybe` i
    -> Just $ k * v
  Tabulate v
    -> Just $ v ! i
  Coerce1 v
    -> Just $ v
  EvalBez2 bez t
    | Just bez_i <- traverse ( `index_maybe` i ) bez
    -> Just $ EvalBez2 bez_i t
  EvalBez3 bez t
    | Just bez_i <- traverse ( `index_maybe` i ) bez
    -> Just $ EvalBez3 bez_i t
  _ -> Nothing

ival :: 𝕀 a -> AI a
ival ( 𝕀 lo hi ) = Ival ( Val lo ) ( Val hi )

normalise :: forall a. ( Eq a, Typeable a )
          => Map String Dynamic -> AI a -> ( AI a, Maybe ( 𝕀 a ) )
normalise vars expr = case expr of
  Pt ( Val v )
    -> ( expr, Just $ 𝕀 v v )
  Pt ( Var v )
    | Just d <- Map.lookup v vars
    , Just a <- fromDynamic @( AI a ) d
    , Just r <- snd $ normalise vars a
    -> ( ival r, Just r )
    | otherwise
    -> ( expr, Nothing )
  Ival ( Val v1 ) ( Val v2 )
    | v1 == v2
    -> ( Pt ( Val v1 ), Just $ 𝕀 v1 v2 )
    | otherwise
    -> ( expr, Just $ 𝕀 v1 v2 )
  Ival {}                    -> ( expr, Nothing )
  (:+:) sv iv1 iv2
    | Scalar <- sv
    , Just ( 𝕀 0 0 ) <- mv1
    -> ( iv2', mv2 )
    | Vector <- sv
    , Just o <- mv1
    , T o == origin
    -> ( iv2', mv2 )
    | Scalar <- sv
    , Just ( 𝕀 0 0 ) <- mv2
    -> ( iv1', mv1 )
    | Vector <- sv
    , Just o <- mv2
    , T o == origin
    -> ( iv1', mv1 )
    | Just v1 <- mv1
    , Just v2 <- mv2
    -> let r = case sv of { Scalar -> v1 + v2; Vector -> unT $ T v1 ^+^ T v2 }
       in ( ival r, Just $ r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = (:+:) sv iv1' iv2'
      (iv1', mv1) = normalise vars iv1
      (iv2', mv2) = normalise vars iv2
  (:-:) sv iv1 iv2
    | Scalar <- sv
    , Just ( 𝕀 0 0 ) <- mv1
    -> normalise vars $ Negate sv iv2
    | Vector <- sv
    , Just o <- mv1
    , T o == origin
    -> normalise vars $ Negate sv iv2
    | Scalar <- sv
    , Just ( 𝕀 0 0 ) <- mv2
    -> ( iv1', mv1 )
    | Vector <- sv
    , Just o <- mv2
    , T o == origin
    -> ( iv1', mv1 )
    | Just v1 <- mv1
    , Just v2 <- mv2
    -> let r = case sv of { Scalar -> v1 - v2; Vector -> unT $ T v1 ^-^ T v2 }
       in ( ival r, Just $ r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = (:-:) sv iv1' iv2'
      (iv1', mv1) = normalise vars iv1
      (iv2', mv2) = normalise vars iv2
  Negate sv iv
    | Just v <- mv
    -> let r = case sv of { Scalar -> unT $ negate $ T v; Vector -> unT $ origin ^-^ T v }
       in ( ival r , Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Negate sv iv'
      (iv', mv) = normalise vars iv
  iv1 :*: iv2
    | Just ( 𝕀 0 0 ) <- mv1
    -> zero
    | Just ( 𝕀 1 1 ) <- mv1
    -> (iv2', mv2)
    | Just ( 𝕀 1 1 ) <- mv2
    -> (iv1', mv1)
    | Just ( 𝕀 0 0 ) <- mv2
    -> zero
    | Just v1 <- mv1
    , Just v2 <- mv2
    -> let r = v1 + v2
       in ( ival r, Just $ r )
    | otherwise
    -> ( expr', Nothing )
    where
      zero = ( ival $ 𝕀 0 0, Just $ 𝕀 0 0 )
      expr' = (:*:) iv1' iv2'
      (iv1', mv1) = normalise vars iv1
      (iv2', mv2) = normalise vars iv2
  iv :^: n
    | Just v <- mv
    -> let r = v ^ n
       in ( ival r , Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = iv' :^: n
      (iv', mv) = normalise vars iv
  Recip iv
    | Just v <- mv
    -> let r = recip v
       in ( ival r , Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Recip iv'
      (iv', mv) = normalise vars iv
  Scale k iv
    | Just ( 𝕀 0 0 ) <- mk
    -> ( ival $ unT $ origin, Just $ unT origin )
    | Just o <- mv
    , T o == origin
    -> ( ival $ unT $ origin, Just $ unT origin )
    | Just ( 𝕀 1 1 ) <- mk
    -> ( iv', mv )
    | Just val_k <- mk
    , Just val_v <- mv
    -> let r = unT $ val_k *^ T val_v
       in ( ival r, Just $ r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Scale k' iv'
      (k' , mk) = normalise vars k
      (iv', mv) = normalise vars iv
  Cross iv1 iv2
    | Just v1 <- mv1
    , Just v2 <- mv2
    -> let r = T v1 × T v2
       in ( ival r, Just $ r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Cross iv1' iv2'
      (iv1', mv1) = normalise vars iv1
      (iv2', mv2) = normalise vars iv2
  Sqrt iv
    | Just v <- mv
    -> let r = sqrt v
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Sqrt iv'
      (iv', mv) = normalise vars iv
  Cos iv
    | Just v <- mv
    -> let r = cos v
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Cos iv'
      (iv', mv) = normalise vars iv
  Sin iv
    | Just v <- mv
    -> let r = sin v
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Sin iv'
      (iv', mv) = normalise vars iv
  Atan iv
    | Just v <- mv
    -> let r = atan v
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Atan iv'
      (iv', mv) = normalise vars iv
  Pi -> ( Pi, Just pi )
  Coerce1 iv
    | Just v <- mv
    -> let r = coerce v
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Coerce1 iv'
      (iv', mv) = normalise vars iv
  Coerce2 iv
    | Just v <- mv
    -> let r = coerce v
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Coerce2 iv'
      (iv', mv) = normalise vars iv
  EvalBez2 @v (Quadratic.Bezier p0 p1 p2) t
    -> ( EvalBez2 (Quadratic.Bezier p0' p1' p2') t'
       , Nothing
       --  Quadratic.bezier
       --  <$> (Quadratic.Bezier <$> m0 <*> m1 <*> m2)
       --  <*> mt
       )
    where
      (p0', m0) = normalise vars p0
      (p1', m1) = normalise vars p1
      (p2', m2) = normalise vars p2
      (t' , mt) = normalise vars t
  EvalBez3 @v (Cubic.Bezier p0 p1 p2 p3) t
    -> ( EvalBez3 (Cubic.Bezier p0' p1' p2' p3') t'
       , Nothing
       --  Cubic.bezier
       --  <$> (Cubic.Bezier <$> m0 <*> m1 <*> m2 <*> m3)
       --  <*> mt
       )
    where
      (p0', m0) = normalise vars p0
      (p1', m1) = normalise vars p1
      (p2', m2) = normalise vars p2
      (p3', m3) = normalise vars p3
      (t' , mt) = normalise vars t
  Tabulate @n v
    | Just xs <- sequence mxs
    -> let r = tabulate ( xs ! )
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Tabulate v'
      v'  :: Vec n ( AI Double )
      mxs :: Vec n ( Maybe 𝕀 )
      (v', mxs) = unzip $ fmap ( normalise vars ) v
  Index iv i
    | Just r <- index_maybe iv' i
    -> normalise vars r
    | Just v <- mv
    -> let r = v `index` i
       in ( ival r, Just r )
    | otherwise
    -> ( expr', Nothing )
    where
      expr' = Index iv' i
      (iv', mv) = normalise vars iv

val :: forall a. Typeable a => Map String Dynamic -> Value a -> a
val _    ( Val a ) = a
val vars ( Var v ) =
  case Map.lookup v vars of
    Nothing -> error $ "Variable '" ++ v ++ "' not in environment."
    Just d ->
      case fromDynamic d of
        Nothing -> error $ unlines [ "Incorrect type for variable '" ++ v ++ "'."
                                   , "  Expected: " ++ show ( typeRep $ Proxy @a )
                                   , "    Actual: " ++ show ( dynTypeRep d ) ]
        Just a  -> a

eval :: Typeable a => Map String Dynamic -> AI a -> 𝕀 a
eval vars = \ case
  Pt v                 -> let f = val vars v in 𝕀 f f
  Ival lo hi           -> 𝕀 (val vars lo) (val vars hi)
  (:+:) Scalar iv1 iv2 -> eval vars iv1  +  eval vars iv2
  (:+:) Vector iv1 iv2 -> unT $ (^+^) ( T ( eval vars iv1 ) ) ( T ( eval vars iv2 ) )
  (:-:) Scalar iv1 iv2 -> eval vars iv1  -  eval vars iv2
  (:-:) Vector iv1 iv2 -> unT $ (^-^) ( T ( eval vars iv1 ) ) ( T ( eval vars iv2 ) )
  Negate Scalar iv     -> unT $ negate $ T $ eval vars iv
  Negate Vector iv     -> unT $ invert $ T $ eval vars iv
  iv1 :*: iv2          -> eval vars iv1 * eval vars iv2
  iv :^: n             -> eval vars iv ^ n
  Recip iv             -> recip $ eval vars iv
  Scale k v            -> unT $ eval vars k *^ T ( eval vars v )
  Cross iv1 iv2        -> T ( eval vars iv1 ) × T ( eval vars iv2 )
  Sqrt iv              -> sqrt $ eval vars iv
  Cos iv               -> cos $ eval vars iv
  Sin iv               -> sin $ eval vars iv
  Atan iv              -> atan $ eval vars iv
  Pi                   -> pi
  Coerce1 a            -> coerce $ eval vars a
  Coerce2 a            -> coerce $ eval vars a
  EvalBez2 bez t       -> error "TODO" -- Quadratic.bezier (fmap (eval vars) bez) (eval vars t)
  EvalBez3 bez t       -> error "TODO" --     Cubic.bezier (fmap (eval vars) bez) (eval vars t)
  Tabulate v           -> let !w = fmap ( eval vars ) v
                          in tabulate ( w ! )
  Index v i            -> let !w = eval vars v
                          in index w i

instance Prelude.Num ( AI Double ) where
  (+) = (:+:) Scalar
  (-) = (:-:) Scalar
  negate = Negate Scalar
  (*) = (:*:)
  abs = error "No abs for abstract intervals"
  signum = error "No signum for abstract intervals"
  fromInteger = Pt . Val . fromInteger

instance Prelude.Fractional ( AI Double ) where
  recip = Recip
  a / b = a :*: Recip b
  fromRational = Pt . Val . fromRational

instance Ring ( AI Double ) where
  (*) = (:*:)
  (^) = (:^:)

deriving via ViaPrelude ( AI Double )
  instance AbelianGroup ( AI Double )
deriving via ViaPrelude ( AI Double )
  instance AbelianGroup ( T ( AI Double ) )
deriving via ViaPrelude ( AI Double )
  instance Field ( AI Double )

instance Floating ( AI Double ) where
  sqrt = Sqrt

instance Transcendental ( AI Double ) where
  pi  = Pi
  cos = Cos
  sin = Sin
  atan = Atan

class    ( Eq v, Show v, Module 𝕀 ( T ( 𝕀 v ) ), Group ( T ( 𝕀 v ) ) )
      => IsVec v
instance ( Eq v, Show v, Module 𝕀 ( T ( 𝕀 v ) ), Group ( T ( 𝕀 v ) ) )
      => IsVec v

instance IsVec v => Module ( AI Double ) ( T ( AI v ) ) where
  origin = T $ Pt $ Val $ inf $ unT ( origin :: T ( 𝕀 v ) )
  (^+^) = coerce $ (:+:) Vector
  (^-^) = coerce $ (:-:) Vector
  (*^)  = coerce Scale
instance Cross  ( AI Double ) ( T ( AI ( ℝ 2 ) ) ) where
  (×) = coerce Cross

instance IsVec v => Semigroup ( T ( AI v ) ) where
  (<>) = (^+^)
instance IsVec v => Monoid    ( T ( AI v ) ) where
  mempty = origin
instance IsVec v => Group     ( T ( AI v ) ) where
  invert = coerce $ Negate Vector

instance IsVec v => Act    ( T ( AI v ) ) ( AI v ) where
  u • v = unT $ u ^+^ T v
instance IsVec v => Torsor ( T ( AI v ) ) ( AI v ) where
  u --> v = T v ^-^ T u

deriving stock instance Show ( StrokeDatum 3 AI )

type instance RepDim ( AI u ) = RepDim u

instance Representable ( AI Double ) ( AI ( ℝ 1 ) ) where
  tabulate f = Coerce1 ( f ( Fin 1 ) )
  index x _ = Coerce2 x
instance Representable ( AI Double ) ( AI ( ℝ 2 ) ) where
  tabulate f = Tabulate $ ( f ( Fin 1 ) ) `VS` ( f ( Fin 2 ) ) `VS` VZ
  index x i = Index x i
instance Representable ( AI Double ) ( AI ( ℝ 3 ) ) where
  tabulate f = Tabulate $ ( f ( Fin 1 ) ) `VS` ( f ( Fin 2 ) ) `VS` ( f ( Fin 3 ) ) `VS` VZ
  index x i = Index x i
instance Representable ( AI Double ) ( AI ( ℝ 4 ) ) where
  tabulate f = Tabulate $ ( f ( Fin 1 ) ) `VS` ( f ( Fin 2 ) ) `VS` ( f ( Fin 3 ) ) `VS` ( f ( Fin 4 ) ) `VS` VZ
  index x i = Index x i

instance RepresentableQ ( AI Double ) ( AI ( ℝ 1 ) ) where
  tabulateQ f = [|| Coerce1 $$( f ( Fin 1 ) ) ||]
  indexQ x _ = [|| Coerce2 $$x ||]
instance RepresentableQ ( AI Double ) ( AI ( ℝ 2 ) ) where
  tabulateQ f = [|| Tabulate $ $$( f ( Fin 1 ) ) `VS` $$( f ( Fin 2 ) ) `VS` VZ ||]
  indexQ x ( Fin i ) = [|| Index $$x ( Fin i ) ||]
instance RepresentableQ ( AI Double ) ( AI ( ℝ 3 ) ) where
  tabulateQ f = [|| Tabulate $ $$( f ( Fin 1 ) ) `VS` $$( f ( Fin 2 ) ) `VS` $$( f ( Fin 3 ) ) `VS` VZ ||]
  indexQ x ( Fin i ) = [|| Index $$x ( Fin i ) ||]
instance RepresentableQ ( AI Double ) ( AI ( ℝ 4 ) ) where
  tabulateQ f = [|| Tabulate $ $$( f ( Fin 1 ) ) `VS` $$( f ( Fin 2 ) ) `VS` $$( f ( Fin 3 ) ) `VS` $$( f ( Fin 4 ) ) `VS` VZ ||]
  indexQ x ( Fin i ) = [|| Index $$x ( Fin i ) ||]
