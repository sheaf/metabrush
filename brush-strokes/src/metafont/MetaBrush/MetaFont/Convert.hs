module MetaBrush.MetaFont.Convert
  ( MetaFontError(..)
  , SomeSpline(..)
  , parseMetaFontPath
  , trailToSpline
  )
  where

-- base
import Data.Kind
  ( Type )
import Data.Foldable
  ( toList )
import qualified Data.Bifunctor as Bi
  ( first )
import GHC.Exts
  ( RealWorld, newMutVar#, runRW# )
import GHC.STRef
  ( STRef(..) )

-- acts
import Data.Act
  ( Act((•)) )

-- containers
import qualified Data.Sequence as Seq
  ( fromList )

-- diagrams-contrib
import qualified Diagrams.TwoD.Path.Metafont as MetaFont
  ( fromString )

-- diagrams-lib
import qualified Diagrams.Located as Diagrams
  ( Located(..) )
import qualified Diagrams.Segment as Diagrams
  ( Segment(..), Offset(..)
  , Open, Closed
  )
import qualified Diagrams.Trail as Diagrams
  ( Trail(..), Trail'(..), SegTree(..)
  , Line, Loop
  )

-- linear
import qualified Linear.V2 as Linear
  ( V2(..) )
import qualified Linear.Affine as Linear
  ( Point(..) )

-- parsec
import qualified Text.Parsec.Error as Parsec
  ( ParseError )

-- text
import Data.Text
  ( Text )

-- brush-strokes
import Math.Bezier.Spline
  ( Spline(..), SplineType(..), KnownSplineType
  , Curves(..), Curve(..), NextPoint(..)
  )
import Math.Bezier.Stroke
  ( CachedStroke(..) )
import Math.Module
  ( lerp )
import Math.Linear
  ( ℝ(..), T(..) )

-- metabrushes
import MetaBrush.DSL.Interpolation
  ( Interpolatable(Diff) )
import MetaBrush.Document
  ( PointData(..) )

--------------------------------------------------------------------------------

data MetaFontError
  = PathParseError Parsec.ParseError
  | TooFewBrushParams
  | TooManyBrushParams
  deriving stock Show

type Openness :: Type -> SplineType
type family Openness l = clo | clo -> l where
  Openness Diagrams.Line = Open
  Openness Diagrams.Loop = Closed

type Openness' :: Type -> SplineType
type family Openness' c = clo | clo -> c where
  Openness' Diagrams.Closed = Open
  Openness' Diagrams.Open   = Closed

type LocatedTrail    = Diagrams.Located (Diagrams.Trail    Linear.V2 Double)
type LocatedTrail' l = Diagrams.Located (Diagrams.Trail' l Linear.V2 Double)

data SomeSpline ptData where
  SomeSpline :: KnownSplineType clo => Spline clo (CachedStroke RealWorld) ptData -> SomeSpline ptData

parseMetaFontPath :: Interpolatable Double ptParams => Text -> [ptParams] -> Either MetaFontError (SomeSpline (PointData ptParams))
parseMetaFontPath pathText ptParams = do
  locTrail <- Bi.first PathParseError $ MetaFont.fromString @LocatedTrail pathText
  let
    loc = Diagrams.loc locTrail
  case Diagrams.unLoc locTrail of
    Diagrams.Trail trail'@(Diagrams.Line {}) ->
      SomeSpline <$> trailToSpline @Diagrams.Line ( Diagrams.Loc { Diagrams.loc = loc, Diagrams.unLoc = trail' } ) ptParams
    Diagrams.Trail trail'@(Diagrams.Loop {}) ->
      SomeSpline <$> trailToSpline @Diagrams.Loop ( Diagrams.Loc { Diagrams.loc = loc, Diagrams.unLoc = trail' } ) ptParams

trailToSpline :: forall l ptParams
              .  Interpolatable Double ptParams
              => LocatedTrail' l
              -> [ptParams]
              -> Either MetaFontError (Spline (Openness l) (CachedStroke RealWorld) (PointData ptParams))
trailToSpline _ [] = Left TooFewBrushParams
trailToSpline (Diagrams.Loc { Diagrams.loc = Linear.P ( Linear.V2 sx sy ), Diagrams.unLoc = trail }) (ptDatum:ptData) =
  case trail of
    Diagrams.Line ( Diagrams.SegTree segs ) -> do
      ( curves, _, _ ) <- go start (toList segs) ptData
      pure $
        Spline
          { splineStart  = start
          , splineCurves = OpenCurves { openCurves = Seq.fromList curves }
          }
    Diagrams.Loop ( Diagrams.SegTree prevSegs ) lastSeg -> do
      ( prevCurves, lastStart, endParams ) <- go start (toList prevSegs) ptData
      case endParams of
        []              -> Left TooFewBrushParams
        ( _ : _ : _ )   -> Left TooManyBrushParams
        [ lastParams ] ->
          pure $
            Spline
              { splineStart = start
              , splineCurves =
                ClosedCurves
                  { prevOpenCurves  = Seq.fromList prevCurves
                  , lastClosedCurve = segmentToCurve lastStart lastParams lastSeg
                  }
              }
  where
    start :: PointData ptParams
    start = PointData
              { pointCoords = ℝ2 sx sy
              , pointState  = Normal
              , brushParams = ptDatum
              }
    go :: PointData ptParams
       -> [Diagrams.Segment Diagrams.Closed Linear.V2 Double]
       -> [ptParams]
       -> Either MetaFontError ( [Curve Open (CachedStroke RealWorld) (PointData ptParams)], PointData ptParams, [ptParams] )
    go p0 []         pars        = Right ( [], p0, pars )
    go p0 (seg:segs) (par1:pars) =
      let
        nextStart :: PointData ptParams
        nextStart =
          case seg of
            Diagrams.Linear    ( Diagrams.OffsetClosed ( Linear.V2 ex ey ) ) ->
              V2 ex ey • ( p0 { brushParams = par1 } )
            Diagrams.Cubic _ _ ( Diagrams.OffsetClosed ( Linear.V2 ex ey ) ) ->
              V2 ex ey • ( p0 { brushParams = par1 } )
        curve :: Curve Open (CachedStroke RealWorld) (PointData ptParams)
        curve = segmentToCurve p0 par1 seg
      in
        fmap ( \ (crvs, end, endParams) -> (curve:crvs, end, endParams) ) $ go nextStart segs pars
    go _ (_:_) [] = Left TooFewBrushParams

segmentToCurve :: forall c ptParams
               .  Interpolatable Double ptParams
               => PointData ptParams -- ^ start point
               -> ptParams           -- ^ parameters at end of curve
               -> Diagrams.Segment c Linear.V2 Double
               -> Curve (Openness' c) (CachedStroke RealWorld) (PointData ptParams)
segmentToCurve p0@( PointData { brushParams = startParams } ) endParams = \case
  Diagrams.Linear end ->
    LineTo
      { curveEnd  = offsetToNextPoint ( p0 { brushParams = endParams } ) end
      , curveData = NoCache
      }
  Diagrams.Cubic ( Linear.V2 x1 y1 ) ( Linear.V2 x2 y2 ) end ->
    Bezier3To
      { controlPoint1 = V2 x1 y1 • ( p0 { brushParams = lerpParams (1/3) startParams endParams } )
      , controlPoint2 = V2 x2 y2 • ( p0 { brushParams = lerpParams (2/3) startParams endParams } )
      , curveEnd      = offsetToNextPoint ( p0 { brushParams = endParams } ) end
      , curveData     = NoCache
      }

  where
    lerpParams :: Double -> ptParams -> ptParams -> ptParams
    lerpParams = lerp @( Diff ptParams )

offsetToNextPoint :: PointData ptParams
                  -> Diagrams.Offset c Linear.V2 Double
                  -> NextPoint (Openness' c) (PointData ptParams)
offsetToNextPoint _ Diagrams.OffsetOpen
  = BackToStart
offsetToNextPoint p0 ( Diagrams.OffsetClosed ( Linear.V2 ex ey ) )
  = NextPoint $ V2 ex ey • p0
