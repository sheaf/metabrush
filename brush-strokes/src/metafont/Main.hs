{-# LANGUAGE OverloadedStrings #-}

module Main where

-- base
import System.Exit
  ( exitSuccess, exitFailure )
import GHC.Exts
  ( RealWorld )

-- containers
import qualified Data.Sequence as Seq
  ( singleton )

-- diagrams-contrib
import qualified Diagrams.TwoD.Path.Metafont as MetaFont
  ( metafont )
import Diagrams.TwoD.Path.Metafont.Combinators
  ( (.--.), (.-), (-.)
  , endpt, arriving, leaving
  )

-- diagrams-lib
import Diagrams.Prelude
  ( p2 )
import qualified Diagrams.Trail as Diagrams
  ( Line )

-- linear
import qualified Linear.V2 as Linear
  ( V2(..) )

-- transformers
import Control.Monad.Trans.Reader
  ( runReaderT )

-- brush-strokes
import Math.Bezier.Spline
  ( Spline, SplineType(..) )
import Math.Bezier.Stroke
  ( CachedStroke(..) )
import Math.Linear
  ( ℝ(..) )

-- metabrushes
import MetaBrush.Asset.Brushes
  ( EllipseBrushFields, ellipse )
import MetaBrush.Document
  ( Document(..), DocumentContent(..)
  , StrokeHierarchy(..), Stroke(..)
  , PointData
  )
import MetaBrush.Document.Serialise
  ( saveDocument )
import MetaBrush.Records
  ( Rec, I(..) )
import qualified MetaBrush.Records as Rec
  ( empty, insert )
import MetaBrush.Unique
  ( newUniqueSupply, freshUnique )

-- convert-metafont
import MetaBrush.MetaFont.Convert
  ( MetaFontError, trailToSpline )

--------------------------------------------------------------------------------

test :: Either MetaFontError (Spline Open (CachedStroke RealWorld) (PointData (Rec EllipseBrushFields)))
test = trailToSpline @Diagrams.Line
         ( MetaFont.metafont path ) params
  where
    z1 = p2 (  2  ,   5   )
    z2 = p2 (  3  ,  -8   )
    z3 = p2 (  3  , -14   )
    z4 = p2 (  6.5, -16   )
    z5 = p2 ( 10  , -12   )
    z6 = p2 (  7.5,  -7.5 )
    z7 = p2 (  4  ,  -7.5 )
    z8 = p2 (  7.5,  -7.5 )
    z9 = p2 ( 11.5,  -2   )
    z0 = p2 (   5 ,  -1   )
    path =
       z1 .--.
       z2 .--.
       z3 .--.
       z4 .--.
       z5 .--.
       z6 .- up -.
       z7 .--.
       z8 .--.
       z9 .- up -.
       endpt z0

    params :: [ Rec EllipseBrushFields ]
    params =
      map ( uncurry mk_ellipse )
        [ ( 2  , -0.349066 )
        , ( 0.5,  0 )
        , ( 1  ,  0.785398 )
        , ( 0.8,  1.5708 )
        , ( 1.5,  3.14159 )
        , ( 0.4,  3.66519 )
        , ( 0.4,  6.283185 )
        , ( 0.4,  8.90 )
        , ( 1.5,  9.4247 )
        , ( 0.3, 12.217 )
        ]


    mk_ellipse :: Double -> Double -> Rec EllipseBrushFields
    mk_ellipse a phi =
      Rec.insert @"a" (I $ 0.5 * a) $ Rec.insert @"b" (I 0.05) $ Rec.insert @"phi" (I phi) $ Rec.empty

    up = arriving $ Linear.V2 0 -1

main :: IO ()
main = case test of
  Left err     -> do
    print err
    exitFailure
  Right spline -> do

    uniqueSupply <- newUniqueSupply
    docUnique    <- runReaderT freshUnique uniqueSupply
    strokeUnique <- runReaderT freshUnique uniqueSupply
    ellipseBrush <- ellipse uniqueSupply

    let
      stroke :: Stroke
      stroke =
        Stroke
          { strokeName    = "beta"
          , strokeVisible = True
          , strokeUnique  = strokeUnique
          , strokeBrush   = Just ellipseBrush
          , strokeSpline  = spline
          }
      doc :: Document
      doc =
        Document
          { displayName     = "beta"
          , mbFilePath      = Just "betamf.mb"
          , viewportCenter  = ℝ2 0 0
          , zoomFactor      = 16
          , documentUnique  = docUnique
          , documentContent =
              Content
                { unsavedChanges = False
                , latestChange   = "Parsed from MetaFont"
                , guides         = mempty
                , strokes        = Seq.singleton ( StrokeLeaf stroke ) }
          }
    saveDocument "betamf.mb" doc
    exitSuccess
