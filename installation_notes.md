
# Installation notes

On Windows, we need to install an up-to-date MinGW/MSYS2 toolchain, 
together with the packages required by the `haskell-gi` libraries (for `GTK`).

With an `MSYS2` installation at `C:\Haskell\ghcup\msys64`, the following environment variables will need to be set :

`PATH`: `C:\Haskell\ghcup\msys64\mingw64\bin;C:\Haskell\ghcup\msys64\usr\bin`    
`PKG_CONFIG_PATH`: `C:\Haskell\ghcup\msys64\mingw64\lib\pkgconfig`    
`XDG_DATA_DIRS`: `C:\Haskell\ghcup\msys64\mingw64\share;C:\Haskell\ghcup\msys64\usr\share`    

Then we need `pkg-config` and the suite of `GTK` libraries.  
After updating the package database if necessary with `pacman -Sy msys2-keyring`, we can do:

```bash
pacman -S -q --noconfirm mingw64/mingw-w64-x86_64-pkg-config mingw64/mingw-w64-x86_64-gobject-introspection mingw64/mingw-w64-x86_64-atk mingw64/mingw-w64-x86_64-gtksourceview5
```

NB: at the time of writing, the relevant gtksource version is 5; this might need to be
updated if you run into issues, as the API of the auto-generated Haskell gi libraries might be
different dependent on the version numbers of the gi foreign libraries.

Note that GHC 9.2 and below are affected by some bugs on Windows, e.g. [#21109](https://gitlab.haskell.org/ghc/ghc/-/issues/21109)
and [#21111](https://gitlab.haskell.org/ghc/ghc/-/issues/21111). So you should use GHC 9.4 (or above),
and make sure that you are using `cabal-install > 3.8` (due to [#21990](https://gitlab.haskell.org/ghc/ghc/-/issues/21990)).

## Possible errors

### pkg-config could not be found

The error

```bash
The program 'pkg-config' is required but it could not be found.
```

means that `pkg-config` couldn't be found on the path. Run the command

```bash
pacman -S -q --noconfirm mingw64/mingw-w64-x86_64-pkg-config
```

to install it.


### gobject could not be found

An error such as

```bash
The pkg-config package 'gobject' is required but it could not be found.
```

indicates missing dependencies for the `haskell-gi` suite of packages.
Ensure these are installed by running the aforementioned `pacman` command.
Also check that the packages are registered with `pkg-config` using the command

```bash
pkg-config --list-all
```

If packages such as `cairo`, `gtk` etc do not appear, this means `PKG_CONFIG_PATH` is not set correctly.
Search for `*.pc` files in your `MSYS2` installation, and ensure their paths are included in `PKG_CONFIG_PATH`.

You might also have multiple different `pkg-config` executables in your `PATH`;
if so, ensure that `cabal` is using the correct one, e.g. by modifying the `extra-prog-path`
field in your `cabal.config` file; see e.g. [ghcup issue #371](https://gitlab.haskell.org/haskell/ghcup-hs/-/issues/371).

### Missing (or bad) C libraries

An error of the form:

```bash
Missing dependencies on foreign libraries:
* Missing (or bad) C libraries: gobject-2.0
```

indicates that `cabal` is not able to find an external C dependency. If the
C dependency is indeed installed, then perhaps `cabal` is calling the wrong
`pkg-config` executable. This could be due to an incorrect `extra-prog-path`
in your `cabal.config` file; see e.g. [ghcup issue #371](https://gitlab.haskell.org/haskell/ghcup-hs/-/issues/371).  

To fix this, you need to ensure the `pkg-config` executable is correct, e.g.
by re-ordering the paths in the `extra-prog-path` stanza so that the path
containing the correct `pkg-config` appears first.


### Failed to load shared library

An error of the form

```bash
Failed to load shared library 'libcairo-gobject-2.dll' referenced by the typelib: 'libcairo-gobject-2.dll': The specified procedure could not be found.
gi-cairo> Could not resolve symbol "cairo_gobject_context_get_type" in namespace "cairo"
```

indicates an issue finding exported functions in the `DLL`s that we depend upon.
I'm not yet sure how to fix this issue; see [haskell-gi issue #391](https://github.com/haskell-gi/haskell-gi/issues/391).

### Error compiling C standard libraries

An error of the form

```bash
C:\Haskell\ghcup\ghc\9.4.3\lib\..\mingw\include\c++\v1\cmath:642:26: error:
     error: no template named 'numeric_limits'
```

signals that GHC is getting confused about which Unix toolchain to use. This
might be due to having

```cabal
extra-include-dirs: C:\Haskell\ghcup\msys64\mingw64\include
```

in your global `cabal.config` file, or in your `cabal.project.local` file.  
You should **not** specify `extra-include-dirs` on a per-package
basis in your `cabal.project.local` file either: these should be handled by `pkg-config`.

Remove any `extra-include-dirs` and `extra-lib-dirs`, both in your global `cabal.config`
and in local `cabal.project`/`cabal.project.local` files to proceed.

### ...is a relative path which makes no sense

Any error of the form:
```bash
include-dirs: /mingw64/bin is a relative path which makes no sense
```

indicates that one of the packages is incorrectly configured with a Unix-style path.

The version of `pkg-config` packaged with MSYS2 should automatically convert into the correct full filepaths.
So you might need to uninstall the currently installed `pkg-config`, and install the `MSYS2` one:

```
pacman -S -q --noconfirm mingw64/mingw-w64-x86_64-pkg-config
```

This should fix the issue. Another solution is to pass the `--force` flag to `ghc-pkg` for the problematic packages.  
One dependency which used to present this problem was `graphite2`, which is a dependency of `cairo`.
This causes a problem with the `gi-cairo-render` library, so in the `cabal.project` file we can specify:

```
package gi-cairo-render
  ghc-pkg-options: "--force"
```

We can otherwise manually fix this issue by changing the `pkg-config` package file `graphite2.pc` as follows:

```
>>> before:
Libs: -L/mingw64/lib -lgraphite2
Cflags: -I/mingw64/include

>>> after:
Libs: -L${libdir} -lgraphite2
Cflags: -I${includedir}
```

See [this patch](https://github.com/msys2/MINGW-packages/pull/6966).
The package `fontconfig` also presented this issue; this was resolved in [this patch](https://github.com/msys2/MINGW-packages/issues/872).
