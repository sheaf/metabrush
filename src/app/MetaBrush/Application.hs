{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo       #-}

module MetaBrush.Application
  ( runApplication )
  where

-- base
import Control.Monad
  ( forever, void )
import Control.Monad.ST
  ( stToIO )
import Data.Foldable
  ( for_ )
import Data.Int
  ( Int32 )
import Data.Maybe
  ( catMaybes )
import GHC.Conc
  ( forkIO )

-- containers
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

-- directory
import qualified System.Directory as Directory
  ( canonicalizePath )

-- filepath
import System.FilePath
  ( (</>) )

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo
  ( Render )

-- gi-cairo-connector
import qualified GI.Cairo.Render.Connector as Cairo
  ( renderWithContext )

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-gio
import qualified GI.Gio as GIO

-- gi-glib
import qualified GI.GLib as GLib

-- gi-gtk
import qualified GI.Gtk as GTK

-- stm
import qualified Control.Concurrent.STM as STM

-- brush-strokes
import Math.Root.Isolation
  ( defaultRootIsolationOptions )
import Math.Bezier.Cubic.Fit
  ( FitParameters(..) )
import Math.Bezier.Stroke
  ( RootSolvingAlgorithm(..) )

-- MetaBrush
import MetaBrush.Application.Action
  ( ActionOrigin(..), openFile )
import MetaBrush.Application.Context
  ( UIElements(..), Variables(..), TabLocation(..) )
import MetaBrush.Asset.Colours
  ( getColours, renderColoursFromColours )
import MetaBrush.Asset.Logo
  ( drawLogo )
import MetaBrush.Application.UpdateDocument
  ( activeDocument, withActiveDocument )
import MetaBrush.Document
  ( Document(..) )
import MetaBrush.Document.History
  ( DocumentHistory(..) )
import MetaBrush.Event
  ( handleEvents )
import MetaBrush.GTK.Util
  ( widgetShow )
import MetaBrush.Render.Document
  ( blankRender, getDocumentRender )
import MetaBrush.Render.Rulers
  ( renderRuler )
import MetaBrush.UI.BrushList
  ( newBrushView )
import MetaBrush.UI.FileBar
  ( FileBar(..), createFileBar )
import MetaBrush.UI.InfoBar
  ( InfoBar(..), createInfoBar, updateInfoBar )
import MetaBrush.UI.Menu
  ( createMenuBar, createMenuActions )
import MetaBrush.UI.Panels
import MetaBrush.UI.StrokeTreeView
  ( DragSourceData(..), newLayerView )
import MetaBrush.UI.ToolBar
  ( Tool(..), Mode(..), createToolBar )
import MetaBrush.UI.Viewport
  ( Viewport(..), ViewportEventControllers(..)
  , Ruler(..)
  , createViewport
  )
import MetaBrush.Unique
  ( newUniqueSupply )
import MetaBrush.GTK.Util
  ( fileGetFilePath, widgetAddClass, widgetAddClasses )
import qualified Paths_MetaBrush as Cabal
  ( getDataDir, getDataFileName )

--------------------------------------------------------------------------------

runApplication :: GTK.Application -> IO ()
runApplication application = do

  ---------------------------------------------------------
  -- Initialise state

  uniqueSupply <- newUniqueSupply

  redrawStrokesTVar      <- STM.newTVarIO False
  documentRenderTVar     <- STM.newTVarIO ( const $ pure () )
  activeDocumentTVar     <- STM.newTVarIO Nothing
  openDocumentsTVar      <- STM.newTVarIO Map.empty
  strokeListModelsTVar   <- STM.newTVarIO Map.empty
  parStoresTVar          <- STM.newTVarIO Map.empty
  mousePosTVar           <- STM.newTVarIO Nothing
  mouseHoldTVar          <- STM.newTVarIO Nothing
  documentDragTVar       <- STM.newTVarIO Nothing
  modifiersTVar          <- STM.newTVarIO Set.empty
  toolTVar               <- STM.newTVarIO Selection
  modeTVar               <- STM.newTVarIO PathMode
  selectedBrushTVar      <- STM.newTVarIO Nothing
  debugTVar              <- STM.newTVarIO False
  partialPathTVar        <- STM.newTVarIO Nothing
  fileBarTabsTVar        <- STM.newTVarIO Map.empty
  showGuidesTVar         <- STM.newTVarIO True
  maxHistorySizeTVar     <- STM.newTVarIO 1000
  fitParametersTVar      <- STM.newTVarIO $
                              FitParameters
                                { maxSubdiv  = 2
                                , nbSegments = 6
                                , dist_tol   = 5e-3
                                , t_tol      = 1e-4
                                , maxIters   = 10
                                }
  rootsAlgoTVar          <- STM.newTVarIO $
                              HalleyM2
                                { maxIters = 30, precision = 6 }
  cuspFindingOptionsTVar <- STM.newTVarIO $
                              Just
                                ( defaultRootIsolationOptions @1 @1
                                , defaultRootIsolationOptions @1 @2
                                , defaultRootIsolationOptions @2 @3
                                )

  -- Put all these stateful variables in a record for conciseness.
  let
    variables :: Variables
    variables = Variables {..}

  ---------------------------------------------------------
  -- GTK UI

  window      <- GTK.applicationWindowNew application
  display     <- GTK.rootGetDisplay window

  dataPath    <- Directory.canonicalizePath =<< Cabal.getDataDir
  themePath   <- Directory.canonicalizePath =<< Cabal.getDataFileName "theme.css"
  cssProvider <- GTK.cssProviderNew
  GTK.cssProviderLoadFromPath cssProvider themePath
  GTK.styleContextAddProviderForDisplay display cssProvider 1000

  windowKeys <- GTK.eventControllerKeyNew

  GTK.eventControllerSetPropagationPhase windowKeys GTK.PropagationPhaseBubble

  iconTheme <- GTK.iconThemeGetForDisplay display
  let iconSearchPath = dataPath </> "icons"
  GTK.iconThemeAddSearchPath iconTheme iconSearchPath
  GTK.windowSetIconName window ( Just "MetaBrush" )

  widgetAddClasses window [ "metabrush", "window" ]
  GTK.setWindowResizable window True
  GTK.setWindowDecorated window True
  GTK.setWindowTitle window "MetaBrush"
  GTK.windowSetDefaultSize window 1024 768

  colours <- getColours cssProvider
  renderColours <- renderColoursFromColours colours

  ---------------------------------------------------------
  -- Create base UI elements

  baseOverlay <- GTK.overlayNew
  GTK.windowSetChild window ( Just baseOverlay )

  uiGrid <- GTK.gridNew
  GTK.overlaySetChild baseOverlay ( Just uiGrid )

  toolBar  <- GTK.boxNew   GTK.OrientationVertical   0
  mainPane <- GTK.panedNew GTK.OrientationHorizontal
  GTK.panedSetWideHandle mainPane True
  panelBox <- GTK.boxNew   GTK.OrientationVertical   0

  GTK.gridAttach uiGrid toolBar   0 0 2 1
  GTK.gridAttach uiGrid mainPane  2 0 1 1

  mainView <- GTK.boxNew GTK.OrientationVertical 0

  GTK.panedSetStartChild mainPane ( Just mainView )
  GTK.panedSetEndChild   mainPane ( Just panelBox )

  viewportGrid <- GTK.gridNew

  ---------------------------------------------------------
  -- Background

  widgetAddClass uiGrid "bg"

  ---------------------------------------------------------
  -- Title bar

  titleBar <- GTK.headerBarNew

  GTK.windowSetTitlebar window ( Just titleBar )
  widgetAddClass titleBar "titleBar"

  logo <- GTK.boxNew GTK.OrientationVertical 0
  GTK.headerBarPackStart titleBar logo

  GTK.headerBarSetShowTitleButtons titleBar False
  GTK.headerBarSetDecorationLayout titleBar ( Just "icon:minimize,maximize,close" )

  titleLabel <- GTK.labelNew ( Just "MetaBrush" )
  GTK.widgetSetHexpand titleLabel True
  widgetAddClasses titleLabel [ "text", "title", "plain" ]
  GTK.headerBarSetTitleWidget titleBar ( Just titleLabel )

  --------
  -- Logo

  widgetAddClass logo "logo"

  logoArea <- GTK.drawingAreaNew
  GTK.boxAppend logo logoArea

  GTK.widgetSetHexpand logoArea True
  GTK.widgetSetVexpand logoArea True

  GTK.drawingAreaSetDrawFunc logoArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawLogo colours ) cairoContext

  GTK.widgetQueueDraw logoArea

  ---------------------------------------------------------
  -- Main viewport

  viewport@( Viewport {..} ) <- createViewport viewportGrid

  -----------------
  -- Viewport rendering

  -- Update the document render data in a separate thread.
  _ <- forkIO $ forever do
      getRenderDoc <- STM.atomically do
        needsRedraw <- STM.readTVar redrawStrokesTVar
        case needsRedraw of
          False -> STM.retry
          True  -> do
            STM.writeTVar redrawStrokesTVar False
            mbDocNow <- fmap ( present . snd ) <$> activeDocument variables
            case mbDocNow of
              Nothing -> pure ( pure . const $ blankRender colours )
              Just doc -> do
                modifiers      <- STM.readTVar modifiersTVar
                mbMousePos     <- STM.readTVar mousePosTVar
                mbHoldAction   <- STM.readTVar mouseHoldTVar
                mbPartialPath  <- STM.readTVar partialPathTVar
                mode           <- STM.readTVar modeTVar
                showGuides     <- STM.readTVar showGuidesTVar
                debug          <- STM.readTVar debugTVar
                fitParameters  <- STM.readTVar fitParametersTVar
                rootsAlgo      <- STM.readTVar rootsAlgoTVar
                mbCuspOptions  <- STM.readTVar cuspFindingOptionsTVar
                let
                  addRulers :: ( ( Int32, Int32 ) -> Cairo.Render () ) -> ( ( Int32, Int32 ) -> Cairo.Render () )
                  addRulers newRender viewportSize = do
                    newRender viewportSize
                    renderRuler
                      colours viewportSize ViewportOrigin viewportSize
                      mbMousePos mbHoldAction showGuides
                      doc
                pure
                  ( addRulers <$> getDocumentRender
                      renderColours
                      rootsAlgo mbCuspOptions fitParameters
                      mode debug
                      modifiers mbMousePos mbHoldAction mbPartialPath
                      doc
                  )
      renderDoc <- stToIO getRenderDoc
      STM.atomically do
        STM.writeTVar documentRenderTVar renderDoc
      void do
        GLib.idleAdd GLib.PRIORITY_HIGH_IDLE do
          for_
            [ viewportDrawingArea
            , rulerCornerDrawingArea
            , topRulerDrawingArea
            , leftRulerDrawingArea
            ]
            GTK.widgetQueueDraw
          pure False

  -- Render the document using the latest available draw data.
  GTK.drawingAreaSetDrawFunc viewportDrawingArea $ Just \ _ cairoContext _ _ -> void $ do
    viewportWidth  <- GTK.widgetGetWidth  viewportDrawingArea
    viewportHeight <- GTK.widgetGetHeight viewportDrawingArea
    -- Get the Cairo instructions for rendering the current document
    mbDoc <- fmap ( present . snd ) <$> STM.atomically ( activeDocument variables )
    render <- case mbDoc of
      Nothing -> pure ( blankRender colours )
      Just _  -> STM.atomically do
        render <- STM.readTVar documentRenderTVar
        pure ( render ( viewportWidth, viewportHeight ) )
    Cairo.renderWithContext render cairoContext

  for_
    [ ( rulerCornerDrawingArea , RulerCorner )
    , ( topRulerDrawingArea    , TopRuler    )
    , ( leftRulerDrawingArea   , LeftRuler   )
    ]
    ( \ ( rulerDrawingArea, ruler ) ->
      GTK.drawingAreaSetDrawFunc rulerDrawingArea $ Just \ _ cairoContext _ _ -> void do
        viewportWidth  <- GTK.widgetGetWidth  viewportDrawingArea
        viewportHeight <- GTK.widgetGetHeight viewportDrawingArea
        width          <- GTK.widgetGetWidth  rulerDrawingArea
        height         <- GTK.widgetGetHeight rulerDrawingArea
        mbRender       <- STM.atomically $ withActiveDocument variables \ _ doc -> do
          mbMousePos   <- STM.readTVar mousePosTVar
          mbHoldAction <- STM.readTVar mouseHoldTVar
          showGuides   <- STM.readTVar showGuidesTVar
          pure do
            renderRuler
              colours ( viewportWidth, viewportHeight ) ( RulerOrigin ruler ) ( width, height )
              mbMousePos mbHoldAction showGuides
              doc
        for_ mbRender \ render ->
          Cairo.renderWithContext render cairoContext
    )

  ---------------------------------------------------------
  -- Tool bar

  _ <- createToolBar variables colours toolBar

  ---------------------------------------------------------
  -- Info bar

  infoBar@( InfoBar { infoBarArea } ) <- createInfoBar colours

  menuActions <- createMenuActions

  ---------------------------------------------------------
  -- Panels bar

  panelsBar <- createPanelBar panelBox

  rec

    ---------------------------------------------------------
    -- File bar

    fileBar@( FileBar { fileBarBox } ) <-
      createFileBar
        colours variables
        application window windowKeys titleBar titleLabel viewport infoBar
        menuBar menuActions
        panelsBar strokesListView

    let
      uiElements :: UIElements
      uiElements = UIElements {..}

    ------------
    -- Menu bar

    menuBar <- createMenuBar uiElements variables colours

    ---------------------------------------------------------
    -- Strokes view

    strokesListView <- newLayerView uiElements variables

  GTK.scrolledWindowSetChild
    ( layersScrolledWindow panelsBar )
    ( Just strokesListView )

  ---------------------------------------------------------
  -- Brushes view

  brushesListView <- newBrushView window variables DragBrush
  GTK.scrolledWindowSetChild
    ( brushesScrolledWindow panelsBar )
    ( Just brushesListView )

  GTK.boxAppend mainView fileBarBox
  GTK.boxAppend mainView viewportGrid
  GTK.boxAppend mainView infoBarArea

  ---------------------------------------------------------
  -- Actions

  handleEvents uiElements variables

  ---------------------------------------------------------
  -- Dragging files onto the application

  fileListGType <- GIO.glibType @GDK.FileList

  fileDropTarget <- GTK.dropTargetNew fileListGType [ GDK.DragActionCopy ]
  void $ GTK.onDropTargetAccept fileDropTarget $ \ dropObj -> do
    dragFormats <- GDK.dropGetFormats dropObj
    hasFileList <- GDK.contentFormatsContainMimeType dragFormats "text/uri-list"
    return hasFileList
  void $ GTK.onDropTargetDrop fileDropTarget $ \ val _x _y -> do
    mbFileList <- GTK.fromGValue @( Maybe GDK.FileList ) val
    case mbFileList of
      Nothing -> return False
      Just fileList -> do
        files <- GDK.fileListGetFiles fileList
        filePaths <- catMaybes <$> traverse fileGetFilePath files
        for_ filePaths $
          openFile uiElements variables LastTab
        return True
  GTK.widgetAddController window fileDropTarget

  ---------------------------------------------------------
  -- Finishing up

  mbDoc <- fmap ( present . snd ) <$> STM.atomically ( activeDocument variables )
  updateInfoBar viewportDrawingArea infoBar variables ( fmap documentMetadata mbDoc )
    -- need to update the info bar after widgets have been realized

  widgetShow window

  GTK.widgetSetCanFocus     viewportDrawingArea True
  GTK.widgetSetFocusOnClick viewportDrawingArea True
  GTK.widgetSetFocusable    viewportDrawingArea True
  _ <- GTK.widgetGrabFocus  viewportDrawingArea

  -- need to add the controllers after the callbacks have been registered
  GTK.widgetAddController window windowKeys
  GTK.widgetAddController    viewportDrawingArea ( viewportMotionController    viewportEventControllers )
  GTK.widgetAddController    viewportDrawingArea ( viewportScrollController    viewportEventControllers )
  GTK.widgetAddController    viewportDrawingArea ( viewportClicksController    viewportEventControllers )
  GTK.widgetAddController rulerCornerDrawingArea ( viewportMotionController rulerCornerEventControllers )
  GTK.widgetAddController rulerCornerDrawingArea ( viewportScrollController rulerCornerEventControllers )
  GTK.widgetAddController rulerCornerDrawingArea ( viewportClicksController rulerCornerEventControllers )
  GTK.widgetAddController   leftRulerDrawingArea ( viewportMotionController   leftRulerEventControllers )
  GTK.widgetAddController   leftRulerDrawingArea ( viewportScrollController   leftRulerEventControllers )
  GTK.widgetAddController   leftRulerDrawingArea ( viewportClicksController   leftRulerEventControllers )
  GTK.widgetAddController    topRulerDrawingArea ( viewportMotionController    topRulerEventControllers )
  GTK.widgetAddController    topRulerDrawingArea ( viewportScrollController    topRulerEventControllers )
  GTK.widgetAddController    topRulerDrawingArea ( viewportClicksController    topRulerEventControllers )
