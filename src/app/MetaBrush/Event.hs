{-# LANGUAGE ImplicitParams      #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MetaBrush.Event
  ( handleEvents )
  where

-- base
import Control.Monad
  ( void )
import Data.Foldable
  ( for_ )
import Data.Int
  ( Int32 )
import Data.Word
  ( Word32 )

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-gtk
import qualified GI.Gtk as GTK

-- stm
import qualified Control.Concurrent.STM.TVar as STM
  ( readTVarIO )

-- brush-strokes
import Math.Linear
  ( ℝ(..), T(..) )

-- MetaBrush
import MetaBrush.Application.Action
  ( HandleAction(..)
  , ActionOrigin(..)
  , MouseMove(..), MouseClick(..), MouseClickType(..), MouseRelease(..)
  , Scroll(..), KeyboardPress(..), KeyboardRelease(..)
  , quitEverything
  )
import MetaBrush.Application.Context
  ( UIElements(..), Variables(..) )
import MetaBrush.UI.Viewport
  ( Viewport(..), ViewportEventControllers(..), Ruler(..) )

--------------------------------------------------------------------------------

handleEvents :: UIElements -> Variables -> IO ()
handleEvents elts@( UIElements { viewport = Viewport {..}, .. } ) vars = do

  -- Mouse events
  afterWidgetMouseEvent    viewportEventControllers ViewportOrigin
  afterWidgetMouseEvent rulerCornerEventControllers ( RulerOrigin RulerCorner )
  afterWidgetMouseEvent   leftRulerEventControllers ( RulerOrigin LeftRuler   )
  afterWidgetMouseEvent    topRulerEventControllers ( RulerOrigin TopRuler    )

  -- Keyboard events
  void $ GTK.onEventControllerKeyKeyPressed  windowKeys
    ( handleKeyboardPressEvent   elts vars )
  void $ GTK.onEventControllerKeyKeyReleased windowKeys
    ( handleKeyboardReleaseEvent elts vars )

  -- Window quit
  void $ GTK.onApplicationQueryEnd application ( quitEverything application window )

    where
      afterWidgetMouseEvent :: ViewportEventControllers -> ActionOrigin -> IO ()
      afterWidgetMouseEvent ( ViewportEventControllers {..}) eventOrigin = do
        void $ GTK.afterEventControllerMotionMotion  viewportMotionController
          ( handleMotionEvent        elts vars eventOrigin )
        void $ GTK.afterEventControllerScrollScroll  viewportScrollController
          ( handleScrollEvent        elts vars )
        void $ GTK.afterGestureClickPressed          viewportClicksController
          ( handleMouseButtonEvent   elts vars eventOrigin ?self )
        void $ GTK.afterGestureClickReleased         viewportClicksController
          ( handleMouseButtonRelease elts vars eventOrigin ?self )

--------------------------------------------------------------------------------
-- Mouse events.

handleMotionEvent :: UIElements -> Variables -> ActionOrigin -> ( Double -> Double -> IO () )
handleMotionEvent elts vars eventOrigin x y = do
  mousePos <- adjustMousePosition ( viewport elts ) eventOrigin ( ℝ2 x y )
  handleAction elts vars ( MouseMove mousePos )

handleScrollEvent :: UIElements -> Variables -> ( Double -> Double -> IO Bool )
handleScrollEvent elts vars dx dy = do
  mbMousePos <- STM.readTVarIO ( mousePosTVar vars )
  handleAction elts vars ( Scroll mbMousePos ( V2 dx dy ) )
  pure False

handleMouseButtonEvent
  :: UIElements -> Variables -> ActionOrigin -> GTK.GestureClick
  -> ( Int32 -> Double -> Double -> IO () )
handleMouseButtonEvent elts@( UIElements{ viewport = Viewport {..} } ) vars eventOrigin gestureClick nbClicks x y = do
  let
    mbClick :: Maybe MouseClickType
    mbClick
      | nbClicks >= 2
      = Just DoubleClick
      | nbClicks == 1
      = Just SingleClick
      | otherwise
      = Nothing
  for_ mbClick \ click -> do
    _ <- GTK.widgetGrabFocus viewportDrawingArea
    button <- max 1 <$> GTK.gestureSingleGetCurrentButton gestureClick
    --        ^^^^^ use button number 1 if no button number is reported (button == 0)
    mousePos <- adjustMousePosition ( viewport elts ) eventOrigin ( ℝ2 x y )
    handleAction elts vars ( MouseClick eventOrigin click button mousePos )

handleMouseButtonRelease
  :: UIElements -> Variables -> ActionOrigin -> GTK.GestureClick
  -> ( Int32 -> Double -> Double -> IO () )
handleMouseButtonRelease elts vars eventOrigin gestureClick _ x y = do
  button <- max 1 <$> GTK.gestureSingleGetCurrentButton gestureClick
  --        ^^^^^ same as above
  mousePos <- adjustMousePosition ( viewport elts ) eventOrigin ( ℝ2 x y )
  handleAction elts vars ( MouseRelease button mousePos )

adjustMousePosition :: Viewport -> ActionOrigin -> ℝ 2 -> IO ( ℝ 2 )
adjustMousePosition   _                   ViewportOrigin        pt            = pure pt
adjustMousePosition ( Viewport {..} ) ( RulerOrigin ruler ) ( ℝ2 x y ) =
  case ruler of
    RulerCorner -> do
      dx <- fromIntegral @_ @Double <$> GTK.widgetGetWidth  rulerCornerDrawingArea
      dy <- fromIntegral @_ @Double <$> GTK.widgetGetHeight rulerCornerDrawingArea
      pure ( ℝ2 ( x - dx ) ( y - dy ) )
    LeftRuler -> do
      dx <- fromIntegral @_ @Double <$> GTK.widgetGetWidth  leftRulerDrawingArea
      pure ( ℝ2 ( x - dx ) y )
    TopRuler -> do
      dy <- fromIntegral @_ @Double <$> GTK.widgetGetHeight topRulerDrawingArea
      pure ( ℝ2 x ( y - dy ) )

--------------------------------------------------------------------------------
-- Keyboard events.

handleKeyboardPressEvent
  :: UIElements -> Variables
  -> ( Word32 -> Word32 -> [ GDK.ModifierType ] -> IO Bool )
handleKeyboardPressEvent elts vars keyVal _ _ = do
  handleAction elts vars ( KeyboardPress keyVal )
  pure False -- allow the default handler to run


handleKeyboardReleaseEvent
  :: UIElements -> Variables
  -> ( Word32 -> Word32 -> [ GDK.ModifierType ] -> IO () )
handleKeyboardReleaseEvent elts vars keyVal _ _ =
  handleAction elts vars ( KeyboardRelease keyVal )
