{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MetaBrush.GTK.Util
  ( withRGBA, withRGBAndAlpha
  , showRGBA
  , widgetAddClasses, widgetAddClass
  , widgetShow
  , (>=?=>), (>>?=)
  , editableLabelNew
  , fileGetFilePath
  )
  where

-- base
import Control.Monad
  ( (>=>), unless )
import Data.Coerce
  ( coerce )
import Data.Foldable
  ( for_ )
import GHC.Stack
  ( HasCallStack )

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-gtk
import qualified GI.Gtk as GTK

-- text
import Data.Text
  ( Text )

-- transformers
import Control.Monad.IO.Class
  ( MonadIO )
import Control.Monad.Trans.Maybe
  ( MaybeT(..) )

--------------------------------------------------------------------------------

withRGBA :: MonadIO m => GDK.RGBA -> ( Double -> Double -> Double -> Double -> m b ) -> m b
withRGBA rgba f = do
  r <- realToFrac <$> GDK.getRGBARed   rgba
  g <- realToFrac <$> GDK.getRGBAGreen rgba
  b <- realToFrac <$> GDK.getRGBABlue  rgba
  a <- realToFrac <$> GDK.getRGBAAlpha rgba
  f r g b a

withRGBAndAlpha :: MonadIO m => GDK.RGBA -> Double -> ( Double -> Double -> Double -> Double -> m b ) -> m b
withRGBAndAlpha rgba alpha f = do
  r <- realToFrac <$> GDK.getRGBARed   rgba
  g <- realToFrac <$> GDK.getRGBAGreen rgba
  b <- realToFrac <$> GDK.getRGBABlue  rgba
  f r g b alpha

showRGBA :: MonadIO m => GDK.RGBA -> m String
showRGBA rgba = withRGBA rgba \ r g b a ->
  pure $ "rgba(" ++ show r ++ "," ++ show g ++ "," ++ show b ++ "," ++ show a ++ ")"

widgetAddClasses :: ( HasCallStack, GTK.IsWidget widget, MonadIO m ) => widget -> [Text] -> m ()
widgetAddClasses widget classNames =
  for_ classNames ( GTK.widgetAddCssClass widget )

widgetAddClass :: ( HasCallStack, GTK.IsWidget widget, MonadIO m ) => widget -> Text -> m ()
widgetAddClass widget className =
  GTK.widgetAddCssClass widget className

--------------------------------------------------------------------------------

widgetShow :: ( HasCallStack, GTK.IsWidget widget, MonadIO m ) => widget -> m ()
widgetShow widget =
  GTK.widgetSetVisible widget True

--------------------------------------------------------------------------------

infixr 1 >=?=>
(>=?=>) :: forall m a b c. Monad m => ( a -> m ( Maybe b ) ) -> ( b -> m ( Maybe c ) ) -> ( a -> m ( Maybe c ) )
(>=?=>) = coerce ( (>=>) @( MaybeT m ) @a @b @c )

infixl 1 >>?=
(>>?=) :: forall m a b. Monad m => m ( Maybe a ) -> ( a -> m ( Maybe b ) ) -> m ( Maybe b )
(>>?=) = coerce ( (>>=) @( MaybeT m ) @a @b )

--------------------------------------------------------------------------------

-- | Create a new editable label, but remove any 'DragSource' or 'DropTarget'
-- controllers attached to it, as we don't want the label to participate in
-- drag-and-drop operations, especially because having it participate in
-- drag-and-drop operations triggers segfaults due to a GTK bug.
editableLabelNew :: Text -> IO GTK.EditableLabel
editableLabelNew txt = do
  label <- GTK.editableLabelNew txt
  widget <- GTK.toWidget label
  removeDNDControllers widget
  -- We will manually make the label editable on double click.
  GTK.editableSetEditable label False
  widgetAddClasses label [ "monospace" ]
  return label

  where
    removeDNDControllers :: GTK.Widget -> IO ()
    removeDNDControllers widget = do
      controllers <- GTK.widgetObserveControllers widget
      nbControllers <- GIO.listModelGetNItems controllers
      unless ( nbControllers == 0 ) $
        for_ [ 0 .. nbControllers - 1 ] $ \ i -> do
          mbController <- GIO.listModelGetItem controllers i
          for_ mbController $ \ controller -> do
            mbDrag <- GTK.castTo GTK.DragSource controller
            mbDrop <- GTK.castTo GTK.DropTarget controller
            for_ mbDrag $ GTK.widgetRemoveController widget
            for_ mbDrop $ GTK.widgetRemoveController widget
      mbChild <- GTK.widgetGetFirstChild widget
      case mbChild of
        Nothing -> return ()
        Just c -> do
          removeDNDControllers c
          removeControllersSiblings c
    removeControllersSiblings c = do
      mbNext <- GTK.widgetGetNextSibling c
      case mbNext of
        Nothing -> return ()
        Just next -> do
          removeDNDControllers next
          removeControllersSiblings next

--------------------------------------------------------------------------------

-- | Get the underlying 'FilePath' of a 'GIO.File'.
fileGetFilePath :: GIO.File -> IO ( Maybe FilePath )
fileGetFilePath file = GIO.fileGetPath file
  -- TODO: this seems to work for now, but I'm keeping it separate
  -- to handle any encoding issues that seem to occasionally pop up.
