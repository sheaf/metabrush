module MetaBrush.Application.Context where

-- base
import Data.Int
  ( Int32 )
import Data.Word
  ( Word32 )
import GHC.Generics
  ( Generic )

-- containers
import Data.Set
  ( Set )
import Data.Map.Strict
  ( Map )

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo
  ( Render )

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gtk
import qualified GI.Gtk as GTK

-- hashable
import Data.Hashable
  ( Hashable )

-- stm
import qualified Control.Concurrent.STM as STM

-- text
import Data.Text
  ( Text )

-- unordered-containers
import Data.HashMap.Strict
  ( HashMap )

-- brush-strokes
import Math.Bezier.Cubic.Fit
  ( FitParameters )
import Math.Bezier.Stroke
  ( RootSolvingAlgorithm )
import Math.Linear
  ( ℝ(..) )
import Math.Root.Isolation
  ( RootIsolationOptions )

-- MetaBrush
import MetaBrush.Action
  ( ABrushWidgetActionState(..) )
import MetaBrush.Asset.Colours
  ( Colours )
import MetaBrush.Brush
  ( SomeBrush )
import MetaBrush.Document
  ( Zoom )
import MetaBrush.Document.Diff
  ( DragMoveSelect )
import MetaBrush.Document.History
  ( DocumentHistory(..) )
import MetaBrush.Draw
  ( DrawAnchor )
import MetaBrush.Hover
  ( inSmallPointClickRange )
import MetaBrush.Layer
  ( Parent )
import MetaBrush.UI.Panels
  ( PanelsBar )
import MetaBrush.UI.Viewport
  ( Viewport(..), Ruler(..) )
import MetaBrush.Unique
  ( UniqueSupply, Unique )

--------------------------------------------------------------------------------

data UIElements
  = UIElements
  { application     :: !GTK.Application
  , window          :: !GTK.ApplicationWindow
  , windowKeys      :: !GTK.EventControllerKey
  , titleBar        :: !GTK.HeaderBar
  , titleLabel      :: !GTK.Label
  , fileBar         :: !FileBar
  , viewport        :: !Viewport
  , infoBar         :: !InfoBar
  , menuBar         :: GTK.PopoverMenuBar -- needs to be lazy for RecursiveDo
  , menuActions     :: !( HashMap ActionName GIO.SimpleAction )
  , panelsBar       :: !PanelsBar
  , strokesListView :: GTK.ListView
  , colours         :: !Colours
  }

data Variables
  = Variables
  { uniqueSupply           :: !UniqueSupply
  , redrawStrokesTVar      :: !( STM.TVar Bool                                    )
  , documentRenderTVar     :: !( STM.TVar ( ( Int32, Int32 ) -> Cairo.Render () ) )
  , activeDocumentTVar     :: !( STM.TVar ( Maybe Unique )                        )
  , openDocumentsTVar      :: !( STM.TVar ( Map Unique DocumentHistory )          )
  , strokeListModelsTVar   :: !( STM.TVar ( Map Unique GTK.MultiSelection )       )

    -- | This TVar allows us to look up which 'GIO.ListStore' is used
    -- for the children of a given parent.
    --
    -- This allows us to know, given a parent and a child index,
    -- how to insert/delete from the 'GTK.TreeListModel'.
  , parStoresTVar          :: !( STM.TVar ( Map Unique ( Map ( Parent Unique ) GIO.ListStore ) ) )

  , selectedBrushTVar      :: !( STM.TVar ( Maybe SomeBrush ) )

  , mousePosTVar           :: !( STM.TVar ( Maybe ( ℝ 2 ) )   )
  , mouseHoldTVar          :: !( STM.TVar ( Maybe MouseHold ) )

    -- | Initial point for dragging document around.
    --
    -- Separate from 'mouseHoldTVar' in order to allow dragging the document
    -- while in the middle of another hold action.
  , documentDragTVar       :: !( STM.TVar ( Maybe ( ℝ 2 ) )         )
  , modifiersTVar          :: !( STM.TVar ( Set Modifier )          )
  , toolTVar               :: !( STM.TVar Tool                      )
  , modeTVar               :: !( STM.TVar Mode                      )
  , debugTVar              :: !( STM.TVar Bool                      )
  , partialPathTVar        :: !( STM.TVar ( Maybe PartialPath )     )
  , fileBarTabsTVar        :: !( STM.TVar ( Map Unique FileBarTab ) )
  , showGuidesTVar         :: !( STM.TVar Bool                      )
  , maxHistorySizeTVar     :: !( STM.TVar Int                       )
  , fitParametersTVar      :: !( STM.TVar FitParameters             )
  , rootsAlgoTVar          :: !( STM.TVar RootSolvingAlgorithm      )
  , cuspFindingOptionsTVar :: !( STM.TVar ( Maybe ( RootIsolationOptions 1 1, RootIsolationOptions 1 2, RootIsolationOptions 2 3 ) ) )
  }

--------------------------------------------------------------------------------

data LR = L | R
  deriving stock ( Show, Eq, Ord )

data Modifier
  = Control LR
  | Alt     LR
  | Shift   LR
  deriving stock ( Show, Eq, Ord )

modifierKey :: Word32 -> Maybe Modifier
modifierKey n = case fromIntegral n of
  GDK.KEY_Control_L -> Just ( Control L )
  GDK.KEY_Control_R -> Just ( Control R )
  GDK.KEY_Shift_L   -> Just ( Shift L )
  GDK.KEY_Shift_R   -> Just ( Shift R )
  GDK.KEY_Alt_L     -> Just ( Alt L )
  GDK.KEY_Alt_R     -> Just ( Alt R )
  _                 -> Nothing

data GuideAction
  = CreateGuide !Ruler
  | MoveGuide   !Unique
  deriving stock Show

-- | Keep track of a mouse hold action:
--
--  - start a rectangular selection,
--  - move objects by dragging,
--  - draw a control point,
--  - create/modify a guide,
--  - modify brush parameters through a brush widget.
data MouseHold
  = MouseHold
  { holdStartPos   :: !( ℝ 2 )
  , holdStartTime  :: !Double
  , holdNonTrivial :: !Bool
  , holdAction     :: !HoldAction
  } deriving stock Show

nonTrivialHold :: Zoom -> ℝ 2 -> Double -> MouseHold -> Bool
nonTrivialHold zoom pos now
  ( MouseHold { holdStartPos = pos0, holdStartTime = clickTime, holdNonTrivial } )
    -- The hold was already non-trivial
    =  holdNonTrivial
    -- The mouse has moved from the original starting position
    || not ( inSmallPointClickRange zoom pos0 pos )
    -- Enough time has elapsed (0.05 seconds)
    || ( now - clickTime > 0.05 )

data HoldAction
  = SelectionHold
  | DragMoveHold
      { dragAction        :: !DragMoveSelect
      }
  | DrawHold
  | GuideAction
      { guideAction       :: !GuideAction
      }
  | BrushWidgetAction
      { brushWidgetAction :: !ABrushWidgetActionState
      }
  deriving stock Show

-- | Keep track of a path that is in the middle of being drawn.
data PartialPath
  = PartialPath
  { partialPathAnchor   :: !DrawAnchor
  , partialControlPoint :: !( Maybe ( ℝ 2 ) )
  , firstPoint          :: !( Maybe ( ℝ 2 ) )
  }
  deriving stock Show

--------------------------------------------------------------------------------

data Tool
  = Selection
  | Pen
  deriving stock ( Show, Eq )

data Mode
  = PathMode
  | BrushMode
  | MetaMode
  deriving stock ( Show, Eq )

data ToolBar
  = ToolBar
  { selectionTool, penTool, pathTool, brushTool, metaTool, debugTool
      :: !GTK.ToggleButton
  }

--------------------------------------------------------------------------------

data FileBar
  = FileBar
  { fileBarBox  :: !GTK.Box
  , fileTabsBox :: !GTK.Box
  }

data FileBarTab
  = FileBarTab
  { fileBarTab          :: !GTK.Box
  , fileBarTabLabel     :: !GTK.EditableLabel
  , fileBarTabCloseArea :: !GTK.DrawingArea
  }

data TabLocation
  = AfterCurrentTab
  | LastTab
  deriving stock Show

--------------------------------------------------------------------------------

data InfoBar
  = InfoBar
  { infoBarArea :: !GTK.Box
  , zoomText    :: !GTK.Label -- make this editable
  , cursorPosText, topLeftPosText, botRightPosText :: !GTK.Label
  }

--------------------------------------------------------------------------------

data ActionName
  = AppAction { actionSimpleName :: !Text }
  | WinAction { actionSimpleName :: !Text }
  deriving stock ( Eq, Show, Generic )
  deriving anyclass Hashable

--------------------------------------------------------------------------------
