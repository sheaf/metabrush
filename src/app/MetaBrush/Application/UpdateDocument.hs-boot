module MetaBrush.Application.UpdateDocument
  ( DocumentUpdate(..)
  , PureDocModification(..), DocModification(..)
  , ActiveDocChange(..)
  )
where

-- MetaBrush
import MetaBrush.Document
  ( Document(..) )
import MetaBrush.Document.Diff
  ( Diff )
import MetaBrush.Unique
  ( Unique )

--------------------------------------------------------------------------------

data DocumentUpdate
  = CloseDocument
  | SaveDocument     !( Maybe FilePath )
  | UpdateDocumentTo
    { newDocument  :: !Document
    , documentDiff :: !Diff
    }

data PureDocModification
  = Don'tModifyDoc
  | UpdateDoc !DocumentUpdate

data DocModification
  = Don'tModifyDocAndThen
    { postModifAction :: IO () }
  | UpdateDocAndThen
    { modifDocument   :: !DocumentUpdate
    , postModifAction :: IO ()
    }

data ActiveDocChange
  = NoActiveDocChange
  | ActiveDocChange
    { mbOldDocUnique :: Maybe Unique
    }
