{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MetaBrush.Application.Action where

-- base
import Control.Arrow
  ( second )
import Control.Concurrent
  ( forkIO )
import Control.Monad
  ( when, unless, void )
import Data.Foldable
  ( for_, sequenceA_ )
import Data.Function
  ( on )
import Data.List
  ( uncons )
import qualified Data.List.NonEmpty as NE
import Data.Maybe
  ( catMaybes, fromMaybe, isNothing, maybeToList )
import Data.String
  ( IsString )
import Data.Traversable
  ( for )
import Data.Word
  ( Word32 )
import GHC.Clock
  ( getMonotonicTime )
import Text.Read
  ( readMaybe )

-- acts
import Data.Act
  ( Act
    ( (•) )
  , Torsor
    ( (-->) )
  )

-- containers
import Data.Map.Strict
  ( Map )
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

-- directory
import System.Directory
  ( doesDirectoryExist, listDirectory )

-- filepath
import System.FilePath
  ( (</>), (<.>), takeExtension )

-- generic-lens
import Data.Generics.Product.Fields
  ( field' )

-- gi-cairo-connector
import qualified GI.Cairo.Render.Connector as Cairo
  ( renderWithContext )

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gtk
import qualified GI.Gtk as GTK

-- haskell-gi-base
import qualified Data.GI.Base as GI

-- lens
import Control.Lens
  ( over, set )
import Control.Lens.At
  ( ix, at )

-- stm
import Control.Concurrent.STM
  ( STM )
import qualified Control.Concurrent.STM as STM

-- transformers
import qualified Control.Monad.Trans.Reader as Reader
import Control.Monad.Trans.Maybe
  ( MaybeT(..) )

-- text
import Data.Text
  ( Text )
import qualified Data.Text as Text
  ( pack, unpack )

-- brush-strokes
import Math.Bezier.Spline
import Math.Module
  ( Module((*^)) )
import Math.Linear
  ( ℝ(..), T(..) )

-- MetaBrush
import MetaBrush.Action
import MetaBrush.Application.Context
import MetaBrush.Document
  ( Document(..), DocumentContent(..), DocumentMetadata(..)
  , Zoom(..)
  , Guide(..)
  , StrokePoints(..)
  )
import MetaBrush.Application.UpdateDocument
  ( activeDocument
  , DocumentUpdate(..)
  , PureDocModification(..), DocModification(..)
  , ActiveDocChange (..)
  , modifyingCurrentDocument
  , updateUIAction, updateHistoryState, modifyingDocument
  )
import MetaBrush.Asset.WindowIcons
  ( drawClose )
import MetaBrush.Document
  ( DocumentSize(..) )
import MetaBrush.Document.Diff
  ( Diff(..)
  , HierarchyDiff(..), HistoryDiff(..), ContentDiff(..)
  , DragMoveSelect(..), SelectionMode(..)
  )
import MetaBrush.Document.History
  ( DocumentHistory(..), Do(..)
  , newHistory, back, fwd
  )
import qualified MetaBrush.Document.History as History
import MetaBrush.Document.Serialise
  ( saveDocument, loadDocument )

import MetaBrush.Draw
import MetaBrush.GTK.Util
  ( widgetShow )
import MetaBrush.Hover
  ( inLargePointClickRange, inSmallPointClickRange )
import MetaBrush.Guide
import MetaBrush.Layer
import MetaBrush.Stroke
import MetaBrush.UI.Coordinates
  ( toViewportCoordinates )
import MetaBrush.UI.InfoBar
  ( updateInfoBar )
import MetaBrush.UI.FileBar
  ( newFileTab, removeFileTab )
import MetaBrush.UI.StrokeTreeView
  ( applyDeletionToStrokeHierarchy, applyDiffToListModel, updateLayerHierarchy, DoLayerChange (..), LayerChange (..) )
import MetaBrush.UI.Viewport
  ( Viewport(..) )
import MetaBrush.Unique
  ( Unique, freshUnique )
import MetaBrush.GTK.Util
  ( (>=?=>), (>>?=)
  , fileGetFilePath
  , widgetAddClass, widgetAddClasses
  )

--------------------------------------------------------------------------------

actionPrefix :: ActionName -> Text
actionPrefix ( AppAction _ ) = "app."
actionPrefix ( WinAction _ ) = "win."

class HandleAction action where
  handleAction :: UIElements -> Variables -> action -> IO ()

--------------------------------------------------------------------------------
-- General actions

instance HandleAction () where
  handleAction _ _ _ = pure ()

--------------
-- New file --
--------------

data NewFile = NewFile !TabLocation
  deriving stock Show

instance HandleAction NewFile where
  handleAction uiElts vars ( NewFile tabLoc ) =
    newFileTab uiElts vars Nothing tabLoc

---------------
-- Open file --
---------------

data OpenFile = OpenFile !TabLocation
  deriving stock Show

instance HandleAction OpenFile where
  handleAction uiElts@( UIElements {..} ) vars ( OpenFile tabLoc ) = do
    fileDialog <- GTK.fileDialogNew
    GTK.fileDialogSetTitle fileDialog "Open MetaBrush document..."
    GTK.fileDialogSetModal fileDialog True
    GTK.fileDialogSetAcceptLabel fileDialog (Just "Open")

    fileFilter <- GTK.fileFilterNew
    GTK.fileFilterSetName fileFilter (Just "MetaBrush document")
    GTK.fileFilterAddPattern fileFilter "*.mb"
    GTK.setFileDialogDefaultFilter fileDialog fileFilter
    fileFilterType <- GTK.gvalueGType_ @(Maybe GTK.FileFilter)
    filterList <- GIO.listStoreNew fileFilterType
    void $ GIO.listStoreAppend filterList fileFilter
    GTK.fileDialogSetFilters fileDialog (Just filterList)

    GTK.fileDialogOpenMultiple fileDialog (Just window) (Nothing :: Maybe GIO.Cancellable) $
      Just $ \ _ result -> do
        response <- GTK.handleDialogError (\ _ _ -> return Nothing) $
                    GTK.fileDialogOpenMultipleFinish fileDialog result
        case response of
          Just files -> do
            nbFiles <- GIO.listModelGetNItems files
            filePaths <- catMaybes <$>
              for [ ( 0 :: Int ) .. fromIntegral nbFiles - 1 ] \ i ->
                GIO.listModelGetItem files ( fromIntegral i ) >>?=
                  ( GI.castTo GIO.File >=?=> fileGetFilePath )
            for_ filePaths $
              openFile uiElts vars tabLoc
          Nothing -> return ()

openFile :: UIElements -> Variables -> TabLocation -> FilePath -> IO ()
openFile uiElts@( UIElements {..} ) vars@( Variables {..} ) tabLoc filePath =
  when ( takeExtension filePath == ".mb" ) do
    mbDoc <- loadDocument uniqueSupply filePath
    case mbDoc of
      Left errMessage ->
        openFileWarningDialog window filePath errMessage
      Right doc -> do
        newDocUnique <- Reader.runReaderT freshUnique uniqueSupply
        let
          newDocHist :: DocumentHistory
          newDocHist = newHistory doc
        newFileTab uiElts vars ( Just ( newDocUnique, newDocHist ) ) tabLoc
        updateHistoryState uiElts ( Just newDocHist )

openFileWarningDialog
  :: GTK.IsWindow window
  => window -> FilePath -> Text -> IO ()
openFileWarningDialog window filePath errMess = do

  dialogWindow <- GTK.windowNew

  -- Show a header, but not a "X" close button.
  GTK.setWindowDecorated dialogWindow True
  GTK.windowSetDeletable dialogWindow False
  GTK.windowSetTitle dialogWindow ( Just "Warning" )
  GTK.windowSetTransientFor dialogWindow ( Just window )

  contentBox <- GTK.boxNew GTK.OrientationVertical 30
  GTK.widgetSetMarginStart  contentBox 20
  GTK.widgetSetMarginEnd    contentBox 20
  GTK.widgetSetMarginTop    contentBox 20
  GTK.widgetSetMarginBottom contentBox 20

  label <- GTK.labelNew $ Just $ "Could not load file at " <> Text.pack filePath <> ":\n" <> errMess
  GTK.boxAppend contentBox label

  closeButton <- GTK.buttonNew
  GTK.buttonSetLabel closeButton "OK"
  GTK.boxAppend contentBox closeButton

  GTK.windowSetChild dialogWindow ( Just contentBox )

  widgetAddClasses dialogWindow ["metabrush", "bg", "plain", "text", "dialog"]
  widgetAddClass closeButton "dialogButton"

  void $ GTK.onButtonClicked closeButton $ do
    GTK.windowDestroy dialogWindow

  GTK.widgetSetVisible dialogWindow True

-----------------
-- Open folder --
-----------------

data OpenFolder = OpenFolder !TabLocation
  deriving stock Show

instance HandleAction OpenFolder where
  handleAction uiElts@( UIElements {..} ) vars ( OpenFolder tabLoc ) = do

    fileDialog <- GTK.fileDialogNew
    GTK.fileDialogSetTitle fileDialog "Select folder..."
    GTK.fileDialogSetModal fileDialog True
    GTK.fileDialogSetAcceptLabel fileDialog (Just "Open")

    GTK.fileDialogSelectFolder fileDialog (Just window) (Nothing :: Maybe GIO.Cancellable) $
      Just $ \ _ result -> do
        mbFolder <- GTK.handleDialogError (\ _ _ -> return Nothing) $
                    GTK.fileDialogSelectFolderFinish fileDialog result
        for_ mbFolder \ folder -> do
          mbFolderPath <- fileGetFilePath folder
          for_ mbFolderPath \ folderPath -> do
            exists <- doesDirectoryExist folderPath
            when exists do
              filePaths0 <- listDirectory folderPath
              let filePaths =
                    case tabLoc of
                      LastTab -> filePaths0
                      -- If we are inserting several new tabs after the current tab,
                      -- then insert them in reverse order.
                      AfterCurrentTab -> reverse filePaths0
              for_ filePaths \ filePath ->
                openFile uiElts vars tabLoc ( folderPath </> filePath )

---------------------------
-- Save, Save as, Export --
---------------------------

data Save = Save
  deriving stock Show
data SaveAs = SaveAs
  deriving stock Show
data Export = Export
  deriving stock Show

instance HandleAction Save where
  handleAction uiElts vars _ =
    save uiElts vars True
instance HandleAction SaveAs where
  handleAction uiElts vars _ = saveAs uiElts vars True
instance HandleAction Export where
  handleAction uiElts vars _ = export uiElts vars

data SaveFormat
  = MetaBrush
  | SVG
  deriving stock Show

save :: UIElements -> Variables -> Bool -> IO ()
save uiElts vars keepOpen = do
  mbDoc <- fmap ( present . snd ) <$> STM.atomically ( activeDocument vars )
  for_ mbDoc \case
    doc@( Document { documentMetadata = Metadata { documentFilePath }, documentContent } )
      | Nothing <- documentFilePath
      -> saveAs uiElts vars keepOpen
      | False <- unsavedChanges documentContent
      -> pure ()
      | Just filePath <- documentFilePath
      -> modifyingCurrentDocument uiElts vars \ _ -> do
        let
          modif :: DocumentUpdate
          modif = if keepOpen then SaveDocument Nothing else CloseDocument
        pure $ UpdateDocAndThen modif ( saveDocument filePath doc )

saveAs :: UIElements -> Variables -> Bool -> IO ()
saveAs uiElts vars keepOpen =
  withSavePath uiElts MetaBrush \ savePath ->
    modifyingCurrentDocument uiElts vars \ doc -> do
      let
        modif :: DocumentUpdate
        modif = if keepOpen then SaveDocument ( Just savePath ) else CloseDocument
      pure $ UpdateDocAndThen modif ( saveDocument savePath doc )

export :: UIElements -> Variables -> IO ()
export uiElts vars@( Variables { .. } ) = do
  mbRender <- STM.atomically $ do
    mbDoc <- fmap ( present . snd ) <$> activeDocument vars
    case mbDoc of
      Nothing -> return Nothing
      Just _  -> Just <$> STM.readTVar documentRenderTVar
  for_ mbRender \ renderDoc -> do
    withSavePath uiElts SVG \ savePath -> do
      -- TODO: hard-coding the width & height.
      -- Should instead add these as document properties.
      let width, height :: Double
          width  = 1024
          height = 768
      Cairo.withSVGSurface savePath width height \ svgSurface -> do
        Cairo.renderWith svgSurface ( renderDoc ( 1024, 768 ) )
        Cairo.surfaceFinish svgSurface
      -- TODO: this renders the document as it appears on screen.
      -- It would be good to decouple this from the GTK canvas.

withSavePath :: UIElements -> SaveFormat -> ( FilePath -> IO () ) -> IO ()
withSavePath ( UIElements {..} ) saveFormat action = do
  fileDialog <- GTK.fileDialogNew
  GTK.fileDialogSetTitle fileDialog saveText
  GTK.fileDialogSetModal fileDialog True
  GTK.fileDialogSetAcceptLabel fileDialog (Just "Open")

  fileFilter <- GTK.fileFilterNew
  GTK.fileFilterSetName fileFilter (Just $ Text.pack $ show saveFormat <> " document")

  GTK.fileFilterAddPattern fileFilter ( "*." <> extension )
  GTK.setFileDialogDefaultFilter fileDialog fileFilter
  fileFilterType <- GTK.gvalueGType_ @(Maybe GTK.FileFilter)
  filterList <- GIO.listStoreNew fileFilterType
  void $ GIO.listStoreAppend filterList fileFilter
  GTK.fileDialogSetFilters fileDialog (Just filterList)

  GTK.fileDialogSave fileDialog (Just window) (Nothing :: Maybe GIO.Cancellable) $
    Just $ \ _ result -> do
      mbSaveFile <- GTK.handleDialogError (\ _ _ -> return Nothing) $
                    GTK.fileDialogSaveFinish fileDialog result
      for_ mbSaveFile $ \ saveFile -> do
        mbSavePath <- fmap fullFilePath <$> fileGetFilePath saveFile
        for_ mbSavePath action
  where
    saveText, saveOrExport :: Text
    saveText = saveOrExport <> " " <> Text.pack (show saveFormat) <> " document..."
    saveOrExport = case saveFormat of
      MetaBrush -> "Save"
      SVG       -> "Export"
    extension :: IsString s => s
    extension = case saveFormat of
      MetaBrush -> "mb"
      SVG       -> "svg"
    fullFilePath :: FilePath -> FilePath
    fullFilePath fp
      | Just ('.', ext) <- uncons $ takeExtension fp
      , ext == extension
      = fp
      | otherwise
      = fp <.> extension

-----------
-- Close --
-----------

data Close
  = CloseActive -- ^ Close active document.
  | CloseThis   -- ^ Close a specific tab.
    { docToClose :: !Unique }
  | CloseAll    -- ^ Close all open documents.
  deriving stock Show

instance HandleAction Close where
  handleAction
    uiElts@( UIElements {..} )
    vars@( Variables {..} )
    close = do
      (docsToClose :: [( Unique, ( Document, Bool ) )]) <-
        case close of
          CloseActive -> do
            mbActive <- STM.atomically ( activeDocument vars )
            return $
              fmap ( second ( ( , True ) . present ) ) $ maybeToList mbActive
          CloseThis unique -> do
            mbCurrentDoc <- fmap ( second present ) <$> STM.atomically ( activeDocument vars )
            mbDoc        <- fmap present . Map.lookup unique <$> STM.readTVarIO openDocumentsTVar
            case mbDoc of
              Nothing -> return []
              Just doc -> do
                return $
                  pure ( unique, ( doc, maybe False ( ( == unique ) . fst ) mbCurrentDoc ) )
          CloseAll -> do
            docs <- Map.assocs <$> STM.readTVarIO openDocumentsTVar
            pure $ map ( second ( ( , True ) . present ) ) docs
      for_ docsToClose \ ( closeDocUnique, ( Document { documentMetadata = Metadata { documentName }, documentContent }, isActiveDoc ) ) ->
        if unsavedChanges documentContent
        then do
            dialogWindow <- GTK.windowNew

            -- Show a header, but not a "X" close button.
            GTK.setWindowDecorated dialogWindow True
            GTK.windowSetDeletable dialogWindow False
            GTK.windowSetTitle dialogWindow ( Just "Warning" )
            GTK.windowSetTransientFor dialogWindow ( Just window )

            contentBox <- GTK.boxNew GTK.OrientationVertical 30
            GTK.widgetSetMarginStart  contentBox 20
            GTK.widgetSetMarginEnd    contentBox 20
            GTK.widgetSetMarginTop    contentBox 20
            GTK.widgetSetMarginBottom contentBox 20
            GTK.windowSetChild dialogWindow ( Just contentBox )

            label <- GTK.labelNew $ Just $ "\n\"" <> documentName <> "\" contains unsaved changes.\nClose anyway?"
            GTK.boxAppend contentBox label

            buttonBox <- GTK.boxNew GTK.OrientationHorizontal 0

            GTK.boxAppend contentBox buttonBox

            closeButton <- GTK.buttonNew
            GTK.buttonSetLabel closeButton "Close"
            GTK.boxAppend buttonBox closeButton
            saveCloseButton <- GTK.buttonNew
            GTK.buttonSetLabel saveCloseButton "Save and close"
            GTK.boxAppend buttonBox saveCloseButton
            cancelButton <- GTK.buttonNew
            GTK.buttonSetLabel cancelButton "Cancel"
            GTK.boxAppend buttonBox cancelButton

            widgetAddClasses dialogWindow [ "metabrush", "bg", "plain", "text", "dialog" ]
            for_ [ closeButton, saveCloseButton, cancelButton ] \ button ->
              widgetAddClass button "dialogButton"

            void $ GTK.onButtonClicked closeButton $ do
              closeDocument isActiveDoc closeDocUnique
              GTK.windowDestroy dialogWindow
            void $ GTK.onButtonClicked saveCloseButton $ do
              save uiElts vars False
              GTK.windowDestroy dialogWindow
            void $ GTK.onButtonClicked cancelButton $ do
              GTK.windowDestroy dialogWindow

            GTK.widgetSetVisible dialogWindow True

        else do
          closeDocument isActiveDoc closeDocUnique

      where
        closeDocument :: Bool -> Unique -> IO ()
        closeDocument isActiveDoc unique = do
          let change = ActiveDocChange { mbOldDocUnique = Just unique }
          neighbourTabs <- removeFileTab uiElts vars unique
          when isActiveDoc do
            mbNewDocUnique <- STM.atomically $ findNeighbourOpenDoc fileBarTabsTVar neighbourTabs
            case mbNewDocUnique of
              Just newDocUnique ->
                handleAction uiElts vars ( SwitchTo newDocUnique )
              Nothing -> do
                uiUpdateAction <- STM.atomically do
                  STM.writeTVar activeDocumentTVar Nothing
                  uiUpdateAction <- updateUIAction change uiElts vars
                  pure do
                    uiUpdateAction
                    updateHistoryState uiElts Nothing
                uiUpdateAction

findNeighbourOpenDoc :: STM.TVar ( Map Unique FileBarTab )
                     -> ( Maybe GTK.Box, Maybe GTK.Box )
                     -> STM ( Maybe Unique )
findNeighbourOpenDoc fileBarTabsTVar ( mbPrev, mbNext ) = do
  tabs <- STM.readTVar fileBarTabsTVar
  -- Slightly wasteful: use the fileBarTabs map "backwards"
  -- to find the unique of neighbouring file bar tabs.
  let tabFromUnique = map ( \ ( u, tab ) -> ( fileBarTab tab , u ) )
                    $ Map.toList tabs
  return $
    if | Just nextTab <- mbNext
       , Just nextTabUnique <- lookup nextTab tabFromUnique
       -> Just nextTabUnique
       | Just prevTab <- mbPrev
       , Just prevTabUnique <- lookup prevTab tabFromUnique
       -> Just prevTabUnique
       | otherwise
       -> Nothing

---------------------
-- Switch document --
---------------------

data SwitchTo =
  SwitchTo
    { newActiveDocUnique :: !Unique
    }
  deriving stock Show

instance HandleAction SwitchTo where
  handleAction
    uiElts
    vars@( Variables {..} )
    ( SwitchTo { newActiveDocUnique } ) = do
      uiAction <- STM.atomically $ do
        mbPrevActiveDocUnique <- STM.swapTVar activeDocumentTVar ( Just newActiveDocUnique )
        if mbPrevActiveDocUnique == Just newActiveDocUnique
        then
          return $ return ()
        else do
          let change = ActiveDocChange { mbOldDocUnique = mbPrevActiveDocUnique }
          mbHist      <- Map.lookup newActiveDocUnique <$> STM.readTVar openDocumentsTVar
          mbActiveTab <- Map.lookup newActiveDocUnique <$> STM.readTVar fileBarTabsTVar
          mbPrevActiveTab <-
            case mbPrevActiveDocUnique of
              Nothing -> return Nothing
              Just prevActiveDocUnique ->
                Map.lookup prevActiveDocUnique <$> STM.readTVar fileBarTabsTVar
          uiUpdateAction <- updateUIAction change uiElts vars
          return $ do
            for_ mbActiveTab $ \ ( FileBarTab { fileBarTab = activeTab } ) -> do
              GTK.widgetAddCssClass activeTab "activeTab"
              GTK.widgetQueueDraw activeTab

            for_ mbPrevActiveTab \ ( FileBarTab { fileBarTab = prevActiveTab } ) -> do
              GTK.widgetRemoveCssClass prevActiveTab "activeTab"
              GTK.widgetQueueDraw prevActiveTab
            uiUpdateAction
            updateHistoryState uiElts mbHist
      uiAction

--------------
-- Quitting --
--------------

data Quit = Quit
  deriving stock Show

instance HandleAction Quit where
  handleAction ( UIElements { application, window } ) _ _ =
    quitEverything application window

quitEverything :: GTK.IsWindow window => GTK.Application -> window -> IO ()
quitEverything = GTK.applicationRemoveWindow

----------------
-- Undo & Redo --
----------------

data Undo = Undo
  deriving stock Show

instance HandleAction Undo where
  handleAction uiElts vars _ = updateHistory back uiElts vars


data Redo = Redo
  deriving stock Show

instance HandleAction Redo where
  handleAction uiElts vars _ = updateHistory fwd uiElts vars

updateHistory :: ( DocumentHistory -> Maybe ( DocumentHistory, ( Do, HistoryDiff ) ) ) -> UIElements -> Variables -> IO ()
updateHistory f uiElts vars@( Variables {..} ) = do
  uiUpdateAction <- STM.atomically do
    mbUnique <- STM.readTVar activeDocumentTVar
    case mbUnique of
      Nothing -> pure ( pure () )
      Just docUnique -> do
        mbDocHistory <- Map.lookup docUnique <$> STM.readTVar openDocumentsTVar
        case mbDocHistory of
          Nothing -> pure ( pure () )
          Just docHistory -> do
            let mbNewHist = f docHistory
            case mbNewHist of
              Nothing -> pure ( pure () )
              Just ( newDocHistory, ( doOrUndo, diff ) ) -> do
                STM.modifyTVar' openDocumentsTVar ( Map.insert docUnique newDocHistory )
                uiUpdateAction <- updateUIAction NoActiveDocChange uiElts vars

                -- If we undo past creating a new stroke that we were
                -- currently drawing, then reset the 'PartialPath' variable
                -- to 'Nothing'. This avoids attempting to continue a path
                -- that doesn't exist anymore.
                if
                  | History.Undo <- doOrUndo
                  , HierarchyDiff ( NewLayer { newIsGroup = False, newUnique = strokeUnique1 } ) <- diff
                  -> do
                    mbPartialPath <- STM.readTVar partialPathTVar
                    case mbPartialPath of
                      Just ( PartialPath { partialPathAnchor = DrawAnchor { anchorStroke = strokeUnique2 } } )
                        | strokeUnique1 == strokeUnique2
                        -> STM.writeTVar partialPathTVar Nothing
                      _ -> pure ()
                  | otherwise
                  -> pure ()

                pure do
                  case diff of
                    DocumentDiff {} -> return ()
                    HierarchyDiff hDiff ->
                      applyDiffToListModel parStoresTVar docUnique ( doOrUndo, hDiff )
                    ContentDiff {} -> return ()
                  updateHistoryState uiElts ( Just newDocHistory )
                  uiUpdateAction
  uiUpdateAction

---------
-- Cut --
---------

data Cut = Cut
  deriving stock Show

-- TODO
instance HandleAction Cut where
  handleAction _ _ _ = pure ()

----------
-- Copy --
----------

data Copy = Copy
  deriving stock Show

-- TODO
instance HandleAction Copy where
  handleAction _ _ _ = pure ()

-----------
-- Paste --
-----------

data Paste = Paste
  deriving stock Show

-- TODO
instance HandleAction Paste where
  handleAction _ _ _ = pure ()

---------------
-- Duplicate --
---------------

data Duplicate = Duplicate
  deriving stock Show

-- TODO
instance HandleAction Duplicate where
  handleAction _ _ _ = pure ()

------------
-- Delete --
------------

data Delete = Delete
  deriving stock Show

instance HandleAction Delete where
  handleAction
    uiElts
    vars@( Variables { toolTVar, modeTVar } )
    _
    = do
        tool <- STM.readTVarIO toolTVar
        mode <- STM.readTVarIO modeTVar
        case tool of
          -- Delete selected points on pressing 'Delete' in path mode.
          Selection
            | PathMode <- mode
            -> modifyingCurrentDocument uiElts vars \ doc ->
                case deleteSelected doc of
                  Nothing ->
                    pure Don'tModifyDoc
                  Just ( doc', affectedPoints, delStrokes ) -> do
                    let delStrokeList = Map.toList delStrokes
                        diff = case NE.nonEmpty delStrokeList of
                          Nothing ->
                            HistoryDiff $ ContentDiff $ DeletePoints affectedPoints
                          Just delStrokesNE ->
                            let
                              -- TODO: here we are re-doing the deletion, but using 'applyChangeToLayerHierarchy'
                              -- as this gives us the child indices we need for the list model update.
                              -- It would be better to refactor this to avoid going around the houses.
                              ( _hierarchy', deletedLayers ) =
                                applyDeletionToStrokeHierarchy
                                ( strokeHierarchy $ documentContent doc )
                                    -- NB: original doc, not doc'!
                                    -- (we don't want to attempt to delete what has already been deleted)
                                ( fmap (uncurry $ flip WithinParent) delStrokesNE )
                            in
                              HistoryDiff $ HierarchyDiff $
                                DeleteLayers
                                   { deletedPoints = affectedPoints
                                   , deletedLayers
                                   }
                    pure $ UpdateDoc ( UpdateDocumentTo doc' diff )

  -- TODO: handle deletion of layers by checking the current focus.
          _ -> pure ()

------------------
-- Delete layer --
------------------

data DeleteLayer = DeleteLayer !ChildLayer
  deriving stock Show

instance HandleAction DeleteLayer where
  handleAction _ vars
    ( DeleteLayer l ) =
      updateLayerHierarchy vars $
        DoLayerChange $
          DeleteItems ( NE.singleton l )

---------------
-- New group --
---------------

data NewGroup = NewGroup !RelativePosition !ChildLayer
  deriving stock Show

instance HandleAction NewGroup where
  handleAction _ vars ( NewGroup pos lay ) = do
    u <- Reader.runReaderT freshUnique ( uniqueSupply vars )
    updateLayerHierarchy vars $
      DoLayerChange $
        NewItem
         { newUnique = u
         , newIsGroup = True
         , newPosition = ( lay, pos )
         }

-------------------
-- Toggle guides --
-------------------

data ToggleGuides = ToggleGuides
  deriving stock Show

instance HandleAction ToggleGuides where
  handleAction ( UIElements { viewport = Viewport {..} } ) ( Variables { redrawStrokesTVar, showGuidesTVar } ) _ = do
    _guidesWereShown <- STM.atomically do
      guidesWereShown <- STM.readTVar showGuidesTVar
      STM.writeTVar showGuidesTVar ( not guidesWereShown )
      pure guidesWereShown
    --let
    --  newText :: Text
    --  newText
    --    | guidesWereShown = "Show guides"
    --    | otherwise       = "Hide guides"
    --  toggleGuidesWidget :: GTK.Button
    --  toggleGuidesWidget = menuItemWidget . toggleGuides . sectionItems . viewMenu4 . menuItemSubmenu . view
    --                     $ menuObject
    --GTK.buttonSetLabel toggleGuidesWidget newText
    STM.atomically ( STM.writeTVar redrawStrokesTVar True )
    for_ [ rulerCornerDrawingArea, topRulerDrawingArea, leftRulerDrawingArea ] \ drawingArea -> do
      GTK.widgetQueueDraw drawingArea

------------
-- Confirm --
------------

data Confirm = Confirm
  deriving stock Show

instance HandleAction Confirm where
  handleAction
    _
    ( Variables {..} )
    _
    = STM.atomically do
        tool <- STM.readTVar toolTVar
        case tool of
          -- End ongoing drawing on pressing enter key.
          Pen -> do
            STM.writeTVar partialPathTVar Nothing
            STM.writeTVar redrawStrokesTVar True
          _ -> pure ()

----------------
-- About page --
----------------

data About = About
  deriving stock Show

-- TODO
instance HandleAction About where
  handleAction _ _ _ = pure ()

------------------------
-- Canvas size dialog --
------------------------

data Canvas = Canvas
  deriving stock Show

data DocSizeUpd
  = DocSizeUpd
  { wantSize :: !( Maybe Bool )
  , needUpdate :: !Bool
  , setPt1, setPt2 :: !( Maybe ( ℝ 2 ) )
  }
  deriving stock Show

noDocSizeUpd :: DocSizeUpd
noDocSizeUpd = DocSizeUpd Nothing False Nothing Nothing

instance HandleAction Canvas where
  handleAction uiElts@( UIElements { colours, window } ) vars _ = do

    -- The canvas properties window is attached to the given document.
    mbActiveDoc <- STM.atomically $ activeDocument vars
    for_ mbActiveDoc $ \ ( activeDocUnique, History { present = activeDoc } ) -> do

      let
        meta = documentMetadata activeDoc
        nm = documentName meta
        mbDocSize = documentSize meta

      canvasWin <- GTK.windowNew
      GTK.setWindowResizable canvasWin True
      GTK.setWindowDecorated canvasWin True
      GTK.setWindowTitle canvasWin $ "Canvas properties for '" <> nm <> "'"
      GTK.windowSetTransientFor canvasWin ( Just window )
      GTK.windowSetModal canvasWin False
      widgetAddClasses canvasWin [ "metabrush", "bg", "plain", "text", "dialog", "canvas" ]

      -- Add custom close button... TODO: factor this out.
      canvasTitleBar <- GTK.headerBarNew
      GTK.headerBarSetShowTitleButtons canvasTitleBar False
      GTK.headerBarSetDecorationLayout canvasTitleBar ( Just "close" )
      widgetAddClass canvasTitleBar "titleBar"
      windowIcons <- GTK.boxNew GTK.OrientationHorizontal 0
      widgetAddClasses windowIcons [ "windowIcons" ]
      GTK.headerBarPackEnd canvasTitleBar windowIcons
      closeButton <- GTK.buttonNew
      GTK.boxAppend windowIcons    closeButton
      closeArea   <- GTK.drawingAreaNew
      GTK.buttonSetChild    closeButton ( Just    closeArea )
      GTK.drawingAreaSetDrawFunc closeArea $ Just \ _ cairoContext _ _ ->
        void $ Cairo.renderWithContext ( drawClose colours ) cairoContext
      widgetAddClass closeButton "windowIcon"
      widgetAddClass closeButton "closeWindowIcon"
      _ <- GTK.onButtonClicked closeButton $ GTK.windowClose canvasWin
      GTK.windowSetTitlebar canvasWin ( Just canvasTitleBar )

      canvasBox <- GTK.boxNew GTK.OrientationVertical 0
      GTK.windowSetChild canvasWin ( Just canvasBox )

      boundaryButton <- GTK.checkButtonNewWithLabel ( Just "Canvas boundary" )
      corner1Entry <- GTK.entryNew
      corner2Entry <- GTK.entryNew
      for_ [ corner1Entry, corner2Entry ] $ \ e -> do
        widgetAddClass e "monospace"
        GTK.setEntryInputPurpose e GTK.InputPurposeNumber

      keepGoingTVar <- STM.newTVarIO True
      docSizeTVar <- STM.newTVarIO $
        case mbDocSize of
          Nothing -> noDocSizeUpd
          Just ( DocumentSize p1 p2 ) ->
            DocSizeUpd Nothing False ( Just p1 ) ( Just p2 )

      case mbDocSize of
        Nothing -> do
          GTK.checkButtonSetActive boundaryButton False
          GTK.editableSetText corner1Entry "(-100, -100)"
          GTK.editableSetText corner2Entry "( 100,  100)"
          for_ [ corner1Entry, corner2Entry ] $ \ e ->
            GTK.widgetSetSensitive e False
        Just ( DocumentSize { documentTopLeft = ℝ2 x0 y0, documentBottomRight = ℝ2 x1 y1 } ) -> do
          GTK.checkButtonSetActive boundaryButton True
          GTK.editableSetText corner1Entry $ Text.pack ( show ( x0, y0 ) )
          GTK.editableSetText corner2Entry $ Text.pack ( show ( x1, y1 ) )

      _ <- GTK.onCheckButtonToggled boundaryButton $ do
              wantBoundary <- GTK.checkButtonGetActive boundaryButton
              for_ [ corner1Entry, corner2Entry ] \ e -> do
                GTK.widgetSetSensitive e wantBoundary
              STM.atomically $ STM.modifyTVar' docSizeTVar $ \ upd -> upd { needUpdate = True, wantSize = Just wantBoundary }

      setValueOnEntryFinishEditing docSizeTVar corner1Entry $ \ p1 oldUpd ->
        oldUpd { needUpdate = True, setPt1 = Just p1 }
      setValueOnEntryFinishEditing docSizeTVar corner2Entry $ \ p2 oldUpd ->
        oldUpd { needUpdate = True, setPt2 = Just p2 }

      let
        updDocSizeLoop = do
          keepGoing <- STM.readTVarIO keepGoingTVar
          when keepGoing $ do
            DocSizeUpd { wantSize, setPt1, setPt2 }
              <- STM.atomically $ do
                  upd@( DocSizeUpd { needUpdate, wantSize, setPt1, setPt2 } ) <- STM.readTVar docSizeTVar
                  when ( not needUpdate || ( wantSize == Just True && ( isNothing setPt1 || isNothing setPt2 ) ) ) do
                    STM.retry
                  STM.modifyTVar' docSizeTVar ( \ u -> u { needUpdate = False } )
                  return upd
            mbUpd <- STM.atomically . runMaybeT $ modifyingDocument uiElts vars activeDocUnique \ oldDoc ->
              return $ UpdateDoc $ UpdateDocumentTo
                { documentDiff = TrivialDiff
                , newDocument =
                  over ( field' @"documentMetadata" . field' @"documentSize" )
                    ( \ oldDocSize ->
                      case wantSize of
                        Just False -> Nothing
                        Just True
                          | Just ( ℝ2 x0 y0 ) <- setPt1
                          , Just ( ℝ2 x1 y1 ) <- setPt2
                          -> Just $
                               DocumentSize
                                 ( ℝ2 ( min x0 x1 ) ( min y0 y1 ) )
                                 ( ℝ2 ( max x0 x1 ) ( max y0 y1 ) )
                        _ -> oldDocSize
                    )
                    oldDoc
                }
            sequenceA_ mbUpd
            updDocSizeLoop

      _ <- forkIO updDocSizeLoop

      GTK.boxAppend canvasBox boundaryButton
      GTK.boxAppend canvasBox corner1Entry
      GTK.boxAppend canvasBox corner2Entry
      void $ GTK.onWindowCloseRequest canvasWin $ do
        STM.atomically $ STM.writeTVar keepGoingTVar False
        return False
      void $ GTK.onWidgetDestroy canvasWin $ do
        STM.atomically $ STM.writeTVar keepGoingTVar False

      widgetShow canvasWin

setValueOnEntryFinishEditing :: STM.TVar DocSizeUpd
                             -> GTK.Entry
                             -> ( ℝ 2 -> DocSizeUpd -> DocSizeUpd )
                             -> IO ()
setValueOnEntryFinishEditing valTVar entry updDocSize = do
  void $ GTK.onEntryActivate entry updateValue
  void $ GI.on entry ( GI.PropertyNotify #hasFocus ) \ _ -> do
    hasFocus <- GTK.widgetHasFocus entry
    unless hasFocus updateValue
  where
    updateValue = do
      txt <- GTK.editableGetText entry
      case readMaybe ( Text.unpack txt ) of
        Just ( x, y ) -> do
          GTK.widgetRemoveCssClass entry "invalid"
          STM.atomically $
            STM.modifyTVar' valTVar ( updDocSize ( ℝ2 x y ) )
        Nothing ->
          widgetAddClass entry "invalid"

------------------------
-- Preferences dialog --
-----------------------

data OpenPrefs = OpenPrefs
  deriving stock Show

-- TODO
instance HandleAction OpenPrefs where
  handleAction ( UIElements { colours, window } ) _ _ = do
    prefsWin <- GTK.windowNew
    GTK.setWindowResizable prefsWin True
    GTK.setWindowDecorated prefsWin True
    GTK.setWindowTitle prefsWin "Preferences"
    GTK.windowSetTransientFor prefsWin ( Just window )
    GTK.windowSetModal prefsWin False
    widgetAddClasses prefsWin [ "metabrush", "bg", "plain", "text", "dialog" ]

    -- Add custom close button... TODO: factor this out.
    prefsTitleBar <- GTK.headerBarNew
    GTK.headerBarSetShowTitleButtons prefsTitleBar False
    GTK.headerBarSetDecorationLayout prefsTitleBar ( Just "close" )
    widgetAddClass prefsTitleBar "titleBar"
    windowIcons <- GTK.boxNew GTK.OrientationHorizontal 0
    widgetAddClasses windowIcons [ "windowIcons" ]
    GTK.headerBarPackEnd prefsTitleBar windowIcons
    closeButton <- GTK.buttonNew
    GTK.boxAppend windowIcons    closeButton
    closeArea   <- GTK.drawingAreaNew
    GTK.buttonSetChild    closeButton ( Just    closeArea )
    GTK.drawingAreaSetDrawFunc closeArea $ Just \ _ cairoContext _ _ ->
      void $ Cairo.renderWithContext ( drawClose colours ) cairoContext
    widgetAddClass closeButton "windowIcon"
    widgetAddClass closeButton "closeWindowIcon"
    _ <- GTK.onButtonClicked closeButton $ GTK.windowClose prefsWin
    GTK.windowSetTitlebar prefsWin ( Just prefsTitleBar )

{-
    -- TODO: actually implement the preferences chooser...
    prefsNotebook <- GTK.notebookNew
    algoPrefs <- GTK.boxNew GTK.OrientationVertical 20
    algoLabel <- GTK.labelNew $ Just "Algorithms"
    _ <- GTK.notebookAppendPage prefsNotebook algoPrefs (Just algoLabel)
    otherPrefs <- GTK.boxNew GTK.OrientationVertical 20
    otherLabel <- GTK.labelNew $ Just "Other"
    _ <- GTK.notebookAppendPage prefsNotebook otherPrefs (Just otherLabel)
    GTK.windowSetChild prefsWin ( Just prefsNotebook )
-}

    widgetShow prefsWin

--------------------------------------------------------------------------------
-- Input actions

--------------------
-- Mouse movement --
--------------------

data MouseMove = MouseMove !( ℝ 2 )
  deriving stock Show

instance HandleAction MouseMove where
  handleAction
    uiElts@( UIElements { viewport = Viewport {..}, infoBar } )
    vars@( Variables {..} )
    ( MouseMove ( ℝ2 x y ) )
    = do
      viewportWidth  <- fromIntegral @_ @Double <$> GTK.widgetGetWidth  viewportDrawingArea
      viewportHeight <- fromIntegral @_ @Double <$> GTK.widgetGetHeight viewportDrawingArea

      moveTime <- getMonotonicTime

      modifyingCurrentDocument uiElts vars \ doc0@( Document { documentMetadata = meta0 } ) -> do

        modifiers      <- STM.readTVar modifiersTVar
        mbDocDragStart <- STM.readTVar documentDragTVar

        let Metadata { documentZoom = zoom, viewportCenter = oldCenter } = meta0
        let
          updMouseHold :: ℝ 2 -> Maybe MouseHold -> Maybe MouseHold
          updMouseHold _ Nothing = Nothing
          updMouseHold pos ( Just oldHold ) =
            Just $ oldHold { holdNonTrivial = nonTrivialHold zoom pos moveTime oldHold }

        mbDoc1 <- case mbDocDragStart of

          -- Handle middle-mouse document drag
          Just dragStartPos -> do

            let pos0 =
                  toViewportCoordinates zoom ( viewportWidth, viewportHeight ) oldCenter
                    ( ℝ2 x y )
                diff :: T ( ℝ 2 )
                diff = pos0 --> dragStartPos

            STM.writeTVar mousePosTVar $ Just dragStartPos
            STM.modifyTVar' mouseHoldTVar ( updMouseHold dragStartPos )
            return $
              if pos0 == dragStartPos
              then Nothing
              else Just $
                over ( field' @"documentMetadata" . field' @"viewportCenter" )
                  ( diff • ) doc0

          -- Handle normal mouse move (not document drag)
          Nothing -> do
            let pos =
                  toViewportCoordinates zoom ( viewportWidth, viewportHeight ) oldCenter
                    ( ℝ2 x y )
            STM.writeTVar mousePosTVar ( Just pos )
            STM.modifyTVar' mouseHoldTVar ( updMouseHold pos )

            ----------------------------------------------------------
            -- With the pen tool, keeping control pressed while moving the mouse
            -- moves the partial control point (if one exists).
            tool <- STM.readTVar toolTVar
            mbPartialPath <- STM.readTVar partialPathTVar
            mode <- STM.readTVar modeTVar
            STM.writeTVar redrawStrokesTVar True -- need to keep updating for mouse hover updates
            if
              | Pen <- tool
              , Just pp <- mbPartialPath
              , any ( \ case { Control _ -> True; _ -> False } ) modifiers
              -> do STM.writeTVar partialPathTVar ( Just $ pp { partialControlPoint = Just pos } )
                    pure Nothing
              -- In brush mode: modify brush parameters through brush widget.
              | BrushMode <- mode
              -> do mbMouseHold <- STM.readTVar mouseHoldTVar
                    case mbMouseHold of
                      Just ( hold@( MouseHold { holdAction = BrushWidgetAction { brushWidgetAction = brushAction } } ) ) ->
                        let
                          ctrl = pressingControl modifiers
                          alt  = pressingAlt modifiers
                        in
                          case applyBrushWidgetAction ctrl alt pos ( Just brushAction ) doc0 of
                            Nothing ->
                              pure Nothing
                            Just ( newDocument, brushAction' ) -> do
                              STM.writeTVar mouseHoldTVar$
                                ( Just $ hold { holdAction = BrushWidgetAction brushAction' } )
                              pure $ Just newDocument
                      _ -> pure Nothing
              | otherwise
              -> pure Nothing

        let uiUpdateAction doc = do
              updateInfoBar viewportDrawingArea infoBar vars ( Just $ documentMetadata doc )
              for_ [ rulerCornerDrawingArea, topRulerDrawingArea, leftRulerDrawingArea ]
                GTK.widgetQueueDraw

        case mbDoc1 of
          Nothing ->
            -- NB: we still need to update the UI, e.g. to show that the mouse
            -- position has changed, even if no action has taken place.
            pure $ Don'tModifyDocAndThen $ uiUpdateAction doc0
          Just doc1 ->
            -- None of the actions here fundamentally affect the documentcontents,
            -- so we use 'TrivialDiff'.
            pure $
              UpdateDocAndThen ( UpdateDocumentTo doc1 TrivialDiff ) do
                -- UI update action.
                updateInfoBar viewportDrawingArea infoBar vars ( Just $ documentMetadata doc1 )
                for_ [ rulerCornerDrawingArea, topRulerDrawingArea, leftRulerDrawingArea ]
                  GTK.widgetQueueDraw


selectionMode :: Foldable f => f Modifier -> SelectionMode
selectionMode = foldMap \case
  Alt   {} -> Subtract
  Shift {} -> Add
  _        -> New

pressingAlt :: Foldable f => f Modifier -> Bool
pressingAlt = any \case
  Alt {} -> True
  _      -> False

pressingControl :: Foldable f => f Modifier -> Bool
pressingControl = any \case
  Control {} -> True
  _          -> False

-----------------
-- Mouse click --
-----------------

data ActionOrigin
  = ViewportOrigin
  | RulerOrigin !Ruler
  deriving stock Show

data MouseClickType
  = SingleClick
  | DoubleClick
  deriving stock Show

data MouseClick =
  MouseClick
    { clickOrigin :: !ActionOrigin
    , clickType   :: !MouseClickType
    , clickButton :: !Word32
    , clickCoords :: !( ℝ 2 )
    }
  deriving stock Show

instance HandleAction MouseClick where
  handleAction
    uiElts@( UIElements { viewport = Viewport {..} } )
    vars@( Variables {..} )
    ( MouseClick actionOrigin ty button mouseClickCoords ) =
    do

      clickTime <- getMonotonicTime

      viewportWidth  <- fromIntegral @_ @Double <$> GTK.widgetGetWidth  viewportDrawingArea
      viewportHeight <- fromIntegral @_ @Double <$> GTK.widgetGetHeight viewportDrawingArea
      modifyingCurrentDocument uiElts vars \ doc -> do

        let
          meta@( Metadata { documentZoom = zoom, viewportCenter } )
            = documentMetadata doc
          toViewport :: ℝ 2 -> ℝ 2
          toViewport = toViewportCoordinates zoom ( viewportWidth, viewportHeight ) viewportCenter
          pos :: ℝ 2
          pos = toViewport mouseClickCoords

        STM.writeTVar mousePosTVar ( Just pos )

        case button of
          -- Left mouse button.
          1 -> do
            mode <- STM.readTVar modeTVar
            case actionOrigin of
              ViewportOrigin -> case ty of
                SingleClick -> do
                  modifiers <- STM.readTVar modifiersTVar
                  tool <- STM.readTVar toolTVar
                  case tool of
                    Selection -> do
                      -- First, check if we can initiate a brush parameter
                      -- modification action through a brush widget.
                      mbBrushWidgetActionUpdDoc <-
                        case mode of
                          MetaMode -> return Nothing
                          PathMode -> return Nothing
                          BrushMode -> do
                            -- Brush mode: modify brush parameters through brush widget.
                            mbMouseHold <- STM.readTVar mouseHoldTVar
                            let mbPrevWidgetAction = case mbMouseHold of
                                  Just ( MouseHold { holdAction = BrushWidgetAction { brushWidgetAction } } )
                                    -> Just brushWidgetAction
                                  _ -> Nothing
                                ctrl = pressingControl modifiers
                                alt  = pressingAlt modifiers
                            case applyBrushWidgetAction ctrl alt pos mbPrevWidgetAction doc of
                              Just ( newDocument, anActionState@( ABrushWidgetActionState brushAction ) ) -> do
                                let newHoldAction =
                                      MouseHold
                                        { holdStartPos   = pos
                                        , holdStartTime  = clickTime
                                        , holdNonTrivial = False
                                        , holdAction     = BrushWidgetAction anActionState
                                        }
                                STM.writeTVar mouseHoldTVar ( Just newHoldAction )
                                let BrushWidgetActionState
                                      { brushWidgetAction = updAction
                                      , brushWidgetStrokeUnique = updStrokeUnique
                                      , brushWidgetPointIndex = updPointIndex
                                      } = brushAction
                                    diff = HistoryDiff $ ContentDiff
                                         $ UpdateBrushParameters
                                            { updateBrushStroke = updStrokeUnique
                                            , updateBrushPoint  = updPointIndex
                                            , updateBrushAction = updAction
                                            }
                                return ( Just $ UpdateDoc $ UpdateDocumentTo newDocument diff )
                              _ ->
                                return Nothing
                      -- If we are doing a brush widget action, don't attempt anything else.
                      -- Otherwise, move on to selection.
                      case mbBrushWidgetActionUpdDoc of
                        Just updDoc -> return updDoc
                        Nothing ->
                          -- Selection mode mouse hold:
                          --
                          --   - If holding shift or alt, mouse hold initiates a rectangular selection.
                          --   - If not holding shift or alt:
                          --      - if mouse click selected an object, initiate a drag move,
                          --      - otherwise, initiate a rectangular selection.
                          case selectionMode modifiers of
                            -- Drag move: not holding shift or alt, click has selected something.
                            New
                              | PathMode <- mode -- Only allow dragging points in PathMode
                              , Just dragMove <- dragMoveSelect pos doc
                              -> do
                                let newHoldAction =
                                      MouseHold
                                        { holdStartPos   = pos
                                        , holdStartTime  = clickTime
                                        , holdNonTrivial = False
                                        , holdAction     = DragMoveHold dragMove
                                        }
                                STM.writeTVar mouseHoldTVar ( Just newHoldAction )
                                case dragMove of
                                  ClickedOnPoint { dragPoint = (u, i), dragPointWasSelected } ->
                                    if dragPointWasSelected
                                    -- Clicked on a selected point: preserve selection.
                                    then pure Don'tModifyDoc
                                    else do
                                      -- Clicked on an unselected point: only select that point.
                                      let newDoc =
                                            set ( field' @"documentMetadata" . field' @"selectedPoints" )
                                              ( StrokePoints $ Map.singleton u ( Set.singleton i ) )
                                              doc
                                      pure ( UpdateDoc $ UpdateDocumentTo newDoc TrivialDiff )
                                  -- Clicked on curve: preserve old selection.
                                  ClickedOnCurve {} ->
                                    pure Don'tModifyDoc
                            -- Rectangular selection.
                            _ -> do
                                let newHoldAction =
                                      MouseHold
                                        { holdStartPos   = pos
                                        , holdStartTime  = clickTime
                                        , holdNonTrivial = False
                                        , holdAction     = SelectionHold
                                        }
                                STM.writeTVar mouseHoldTVar ( Just newHoldAction )
                                pure Don'tModifyDoc
                    Pen -> case mode of
                      BrushMode -> pure Don'tModifyDoc
                      MetaMode  -> pure Don'tModifyDoc
                      PathMode -> do
                        -- Pen tool in path mode: start or continue a drawing operation.
                        mbPartialPath <- STM.readTVar partialPathTVar
                        mbSelBrush    <- STM.readTVar selectedBrushTVar
                        let newHoldAction =
                              MouseHold
                                { holdStartPos   = pos
                                , holdStartTime  = clickTime
                                , holdNonTrivial = False
                                , holdAction     = DrawHold
                                }
                        STM.writeTVar mouseHoldTVar ( Just newHoldAction )
                        case mbPartialPath of
                          -- No path started yet: find anchor for drawing (existing stroke endpoint, or new stroke).
                          Nothing -> do
                            ( newDocument, drawAnchor ) <-
                              getOrCreateDrawAnchor uniqueSupply mbSelBrush pos doc
                            let firstPos = anchorPos drawAnchor newDocument
                            STM.writeTVar partialPathTVar
                              ( Just $ PartialPath
                                { partialPathAnchor   = drawAnchor
                                , partialControlPoint = Nothing
                                , firstPoint          = Just firstPos
                                }
                              )
                            if anchorIsNew drawAnchor
                            then do
                              let
                                diff :: Diff
                                diff = HistoryDiff $ HierarchyDiff
                                     $ NewLayer
                                        { newUnique   = anchorStroke drawAnchor
                                        , newPosition = WithinParent Root 0
                                           -- TODO: add the stroke above the selected layer
                                           -- or something of the sort.
                                        , newIsGroup = False
                                        }
                              pure ( UpdateDoc $ UpdateDocumentTo newDocument diff )
                            else
                              pure Don'tModifyDoc
                          -- Path already started: indicate that we are continuing a path.
                          Just pp@( PartialPath { firstPoint = mbFirst } ) -> do
                            let stillAtFirstPoint = case mbFirst of
                                   Nothing -> Nothing
                                   Just p ->
                                    if inSmallPointClickRange zoom p pos
                                    then Just p
                                    else Nothing
                            STM.writeTVar partialPathTVar
                              ( Just $ pp { firstPoint = stillAtFirstPoint } )
                            pure Don'tModifyDoc

                DoubleClick -> do
                  tool   <- STM.readTVar toolTVar
                  modifs <- STM.readTVar modifiersTVar
                  case tool of
                    Selection
                      | PathMode <- mode
                      , null modifs
                      -> do
                        STM.writeTVar mouseHoldTVar Nothing
                        case subdivide pos doc of
                          Nothing              ->
                            pure Don'tModifyDoc
                          Just ( newDocument, subdiv ) -> do
                            let
                              diff = HistoryDiff $ ContentDiff
                                   $ SubdivideStroke subdiv
                            pure ( UpdateDoc $ UpdateDocumentTo newDocument diff )

                    -- Ignore double click event otherwise.
                    _ -> pure Don'tModifyDoc

              RulerOrigin ruler -> do
                showGuides <- STM.readTVar showGuidesTVar
                when showGuides do
                  let
                    mbGuide :: Maybe ( Unique, Guide )
                    mbGuide = selectedGuide pos zoom ( documentGuides meta )
                    guideAction :: GuideAction
                    guideAction
                      | Just ( guideUnique, _guide ) <- mbGuide
                      = MoveGuide guideUnique
                      | otherwise
                      = CreateGuide ruler
                  STM.writeTVar mouseHoldTVar $ Just $
                    MouseHold
                      { holdStartPos   = pos
                      , holdNonTrivial = False
                      , holdStartTime  = clickTime
                      , holdAction     = GuideAction { guideAction }
                      }
                pure Don'tModifyDoc

        -- Right mouse button: end partial path.
          3 -> do
            STM.writeTVar partialPathTVar Nothing
            STM.writeTVar redrawStrokesTVar True
            pure Don'tModifyDoc

          -- Middle mouse button: start document drag.
          2 -> do
            STM.writeTVar documentDragTVar ( Just pos )
            pure Don'tModifyDoc

          -- Other mouse buttons: ignored (for the moment at least).
          _ ->
            pure Don'tModifyDoc

-------------------
-- Mouse release --
-------------------

data MouseRelease = MouseRelease !Word32 !( ℝ 2 )
  deriving stock Show

instance HandleAction MouseRelease where
  handleAction
    uiElts@( UIElements { viewport = Viewport {..} } )
    vars@( Variables {..} )
    ( MouseRelease button ( ℝ2 x y ) ) =
    do
      viewportWidth  <- fromIntegral @_ @Double <$> GTK.widgetGetWidth  viewportDrawingArea
      viewportHeight <- fromIntegral @_ @Double <$> GTK.widgetGetHeight viewportDrawingArea

      releaseTime <- getMonotonicTime

      modifyingCurrentDocument uiElts vars \ doc@( Document { documentMetadata } ) -> do

        modifiers <- STM.readTVar modifiersTVar

        -- Obtain the two "hold" actions: normal hold and middle-mouse document drag.
        ( mbMouseHold, _mbDocDrag ) <-
          case button of
            -- Middle mouse button
            2 ->
              ( , ) <$> STM.readTVar mouseHoldTVar <*> STM.swapTVar documentDragTVar Nothing
            _ ->
              ( , ) <$> STM.swapTVar mouseHoldTVar Nothing <*> STM.readTVar documentDragTVar
        let
          Metadata { documentZoom = zoom, viewportCenter } = documentMetadata
          toViewport :: ℝ 2 -> ℝ 2
          toViewport = toViewportCoordinates zoom ( viewportWidth, viewportHeight ) viewportCenter
          pos :: ℝ 2
          pos = toViewport ( ℝ2 x y )
        STM.writeTVar mousePosTVar ( Just pos )

        case button of
          -- Left mouse button.
          1 -> do
            case mbMouseHold of
              Just
                MouseHold
                  { holdStartPos = holdStartPos@( ℝ2 hx hy )
                  , holdAction   = GuideAction { guideAction }
                  } ->
                case guideAction of
                  CreateGuide ruler
                    | createGuide
                    -> do
                      newDocument <- addGuide uniqueSupply ruler pos doc
                      pure ( UpdateDoc $ UpdateDocumentTo newDocument TrivialDiff )
                    | otherwise
                    -> pure ( UpdateDoc $ UpdateDocumentTo doc TrivialDiff )
                    -- ^^ force an UI update when releasing a guide inside a ruler area
                    where
                      createGuide :: Bool
                      createGuide
                        =  x >= 0
                        && y >= 0
                        && x <= viewportWidth
                        && y <= viewportHeight
                  MoveGuide guideUnique
                    | keepGuide
                    -> let
                          newDocument :: Document
                          newDocument =
                            over
                              ( field' @"documentMetadata" . field' @"documentGuides" . ix guideUnique . field' @"guidePoint" )
                              ( ( holdStartPos --> pos :: T ( ℝ 2 ) ) • )
                              doc
                        in pure ( UpdateDoc $ UpdateDocumentTo newDocument TrivialDiff )
                    | otherwise
                    -> let
                          newDocument :: Document
                          newDocument =
                             set ( field' @"documentMetadata" . field' @"documentGuides" . at guideUnique )
                              Nothing
                              doc
                        in pure ( UpdateDoc $ UpdateDocumentTo newDocument TrivialDiff )
                    where
                      l, t :: Double
                      ℝ2 l t = toViewport ( ℝ2 0 0 )
                      keepGuide :: Bool
                      keepGuide
                        =  ( x >= 0 || hx < l ) -- mouse hold position (hx,hy) is in document coordinates,
                        && ( y >= 0 || hy < t ) -- so we must compare it to the point (l,t) instead of (0,0)
                        && x <= viewportWidth
                        && y <= viewportHeight

              _notGuideAction -> do
                tool <- STM.readTVar toolTVar
                mode <- STM.readTVar modeTVar
                case tool of

                  Selection -> do
                    let
                      selMode :: SelectionMode
                      selMode = selectionMode modifiers
                    case mbMouseHold of
                      Just ( hold@MouseHold { holdStartPos = pos0, holdAction } )
                        | nonTrivialHold zoom pos releaseTime hold
                        , PathMode <- mode
                        , DragMoveHold { dragAction } <- holdAction
                        ->
                          let
                            alternateMode :: Bool
                            alternateMode = any ( \case { Alt _ -> True; _ -> False } ) modifiers
                          in case dragUpdate pos0 pos dragAction alternateMode doc of
                            Just ( doc', affectedPts ) ->
                              let diff = HistoryDiff $ ContentDiff
                                       $ DragMove
                                           { dragMoveSelection = dragAction
                                           , dragVector        = pos0 --> pos
                                           , draggedPoints     = affectedPts
                                           }
                              in pure $ UpdateDoc ( UpdateDocumentTo doc' diff )
                            Nothing  -> pure Don'tModifyDoc
                        | SelectionHold {} <- holdAction
                        -> let mbDoc'
                                  | inLargePointClickRange zoom pos0 pos
                                  = fst <$> selectAt selMode pos doc
                                  | otherwise
                                  = fst <$> selectRectangle selMode pos0 pos doc
                            in pure ( UpdateDoc $ UpdateDocumentTo ( fromMaybe doc mbDoc' ) TrivialDiff )
                        | otherwise
                        -> pure Don'tModifyDoc
                      _noNontrivialHold -> do
                        let mbDoc' = fst <$> selectAt selMode pos doc
                        pure ( UpdateDoc $ UpdateDocumentTo ( fromMaybe doc mbDoc' ) TrivialDiff )

                  Pen -> case mode of
                    PathMode -> do
                      mbPartialPath <- STM.readTVar partialPathTVar
                      mbSelBrush    <- STM.readTVar selectedBrushTVar
                      case mbPartialPath of
                        -- Normal pen mode mouse click should have created an anchor.
                        -- If no anchor exists, then just ignore the mouse release event.
                        Nothing -> pure Don'tModifyDoc
                        -- Mouse click release possibilities:
                        --
                        --  - click was on complementary draw stroke draw anchor to close the path,
                        --  - release at same point as click: finish current segment,
                        --  - release at different point as click: finish current segment, adding a control point.
                        Just
                          ( PartialPath
                            { partialPathAnchor   = anchor
                            , partialControlPoint = mbCp2
                            , firstPoint
                            }
                          ) -> do
                          let
                            p1 :: ℝ 2
                            p1 = anchorPos anchor doc
                            pathPoint :: ℝ 2
                            mbControlPoint :: Maybe ( ℝ 2 )
                            partialControlPoint :: Maybe ( ℝ 2 )
                            ( pathPoint, mbControlPoint, partialControlPoint )
                              | Just
                                  hold@MouseHold
                                    { holdStartPos   = holdPos
                                    , holdAction     = DrawHold {}
                                    } <- mbMouseHold
                              , nonTrivialHold zoom pos releaseTime hold
                              = ( holdPos, Just $ ( pos --> holdPos :: T ( ℝ 2 ) ) • holdPos, Just pos )
                              | otherwise
                              = ( pos, Nothing, Nothing )
                          ( _, otherAnchor ) <- getOrCreateDrawAnchor uniqueSupply mbSelBrush pathPoint doc
                          if isNothing firstPoint && anchorsAreComplementary anchor otherAnchor
                          -- Close path.
                          then do
                            STM.writeTVar partialPathTVar Nothing
                            let
                              newSegment :: Spline Open () ( PointData () )
                              newSegment = catMaybesSpline ( inSmallPointClickRange zoom `on` coords ) ()
                                ( PointData p1 () )
                                ( do
                                    cp <- mbCp2
                                    pure ( PointData cp () )
                                )
                                ( do
                                    cp <- mbControlPoint
                                    pure ( PointData cp () )
                                )
                                ( PointData ( anchorPos otherAnchor doc ) () )
                              newDocument :: Document
                              newDocument = addToAnchor True anchor newSegment doc
                              diff = HistoryDiff $ ContentDiff
                                   $ CloseStroke { closedStroke = anchorStroke anchor }
                            pure ( UpdateDoc $ UpdateDocumentTo newDocument diff )
                          else
                            case firstPoint of
                            -- Continue current partial path.
                              Just {} -> do
                                STM.writeTVar partialPathTVar ( Just $ PartialPath anchor partialControlPoint firstPoint )
                                pure Don'tModifyDoc
                            -- Finish current partial path.
                              Nothing -> do
                                STM.writeTVar partialPathTVar ( Just $ PartialPath anchor partialControlPoint Nothing )
                                let
                                  newSegment :: Spline Open () ( PointData () )
                                  newSegment = catMaybesSpline ( inSmallPointClickRange zoom `on` coords ) ()
                                    ( PointData p1 () )
                                    ( do
                                        cp <- mbCp2
                                        pure ( PointData cp () )
                                    )
                                    ( do
                                        cp <- mbControlPoint
                                        pure ( PointData cp () )
                                    )
                                    ( PointData pathPoint () )
                                  newDocument :: Document
                                  newDocument = addToAnchor False anchor newSegment doc
                                  diff = HistoryDiff $ ContentDiff
                                       $ ContinueStroke
                                           { continuedStroke = anchorStroke anchor
                                           , newSegment
                                           }
                                pure ( UpdateDoc $ UpdateDocumentTo newDocument diff )
                    BrushMode ->
                      pure Don'tModifyDoc
                    MetaMode ->
                      pure Don'tModifyDoc

          -- Other mouse buttons: ignored (for the moment at least).
          _ ->
            pure Don'tModifyDoc

---------------
-- Scrolling --
---------------

data Scroll = Scroll !( Maybe ( ℝ 2 ) ) !( T ( ℝ 2 ) )
  deriving stock Show

instance HandleAction Scroll where
  handleAction
    uiElts
    vars@( Variables {..} )
    ( Scroll mbMousePos ( V2 dx dy ) ) = do

      unless ( dx == 0 && dy == 0 ) do
        modifyingCurrentDocument uiElts vars \ doc@( Document { documentMetadata = oldMetadata }) -> do
          let Metadata { viewportCenter = oldCenter, documentZoom = Zoom { zoomFactor = oldZoomFactor } } = oldMetadata
          mbDocDrag <- STM.readTVar documentDragTVar
          modifiers <- STM.readTVar modifiersTVar
          let
            zooming = any ( \ case { Control _ -> True; _ -> False } ) modifiers
            mousePos :: ℝ 2
            mousePos = fromMaybe oldCenter mbMousePos
            mbNewDocAndFinalMousePos :: Maybe ( Document, ℝ 2 )
            mbNewDocAndFinalMousePos
              -- Zooming using 'Control'.
              | zooming
              = let
                  newZoomFactor :: Double
                  newZoomFactor
                    | dy > 0
                    = max 0.0078125 ( oldZoomFactor / sqrt 2 )
                    | otherwise
                    = min 4096 ( oldZoomFactor * sqrt 2 )
                  newCenter :: ℝ 2
                  newCenter
                    = ( 1 - oldZoomFactor / newZoomFactor ) *^ ( oldCenter --> mousePos :: T ( ℝ 2 ) )
                    • oldCenter
                  newMetadata =
                    oldMetadata
                      { documentZoom = Zoom newZoomFactor
                      , viewportCenter = newCenter }

                in Just ( doc { documentMetadata = newMetadata }, mousePos )
              | Just {} <- mbDocDrag
              -- Don't allow scrolling when in the middle of a document drag operation.
              = Nothing
              -- Vertical scrolling turned into horizontal scrolling using 'Shift'.
              | dx == 0 && any ( \ case { Shift _ -> True; _ -> False } ) modifiers
              = let
                  newCenter :: ℝ 2
                  newCenter = ( ( 25 / oldZoomFactor ) *^ V2 dy 0 ) • oldCenter
                in
                  Just
                    ( set ( field' @"documentMetadata" . field' @"viewportCenter" ) newCenter doc
                    , ( oldCenter --> newCenter :: T ( ℝ 2 ) ) • mousePos
                    )
              -- Vertical scrolling.
              | otherwise
              = let
                  newCenter :: ℝ 2
                  newCenter = ( ( 25 / oldZoomFactor ) *^ V2 dx dy ) • oldCenter
                in
                  Just
                    ( set ( field' @"documentMetadata" . field' @"viewportCenter" ) newCenter doc
                    , ( oldCenter --> newCenter :: T ( ℝ 2 ) ) • mousePos
                    )
          case mbNewDocAndFinalMousePos of
            Nothing ->
              pure Don'tModifyDoc
            Just ( newDoc, finalMousePos ) -> do
              for_ mbMousePos \ _oldPos ->
                STM.writeTVar mousePosTVar ( Just finalMousePos )
              pure ( UpdateDoc $ UpdateDocumentTo newDoc TrivialDiff )

--------------------
-- Keyboard press --
--------------------

data KeyboardPress = KeyboardPress !Word32
  deriving stock Show

instance HandleAction KeyboardPress where
  handleAction
    uiElts
    vars@( Variables {..} )
    ( KeyboardPress keyCode ) = do

      for_ ( modifierKey keyCode )
        ( STM.atomically . STM.modifyTVar' modifiersTVar . Set.insert )

      case fromIntegral keyCode of

        GDK.KEY_Escape ->
          handleAction uiElts vars Quit

        confirm
          |  confirm == GDK.KEY_Return
          || confirm == GDK.KEY_space
          -> handleAction uiElts vars Confirm

        ctrl
          | ctrl == GDK.KEY_Control_L || ctrl == GDK.KEY_Control_R
          -> do
            ----------------------------------------------------------
            -- With the pen tool, pressing control moves
            -- the partial path control point to the mouse position.
            STM.atomically do
              tool          <- STM.readTVar toolTVar
              mbMousePos    <- STM.readTVar mousePosTVar
              mbPartialPath <- STM.readTVar partialPathTVar
              case tool of
                Pen
                  | Just mp <- mbMousePos
                  , Just pp <- mbPartialPath
                  -> do
                       STM.writeTVar partialPathTVar ( Just $ pp { partialControlPoint = Just mp } )
                       STM.writeTVar redrawStrokesTVar True
                _ -> pure ()

        _ -> pure ()

----------------------
-- Keyboard release --
----------------------

data KeyboardRelease = KeyboardRelease !Word32
  deriving stock Show

instance HandleAction KeyboardRelease where
  handleAction _ ( Variables { modifiersTVar } ) ( KeyboardRelease keyCode ) =
    for_ ( modifierKey keyCode )
      ( STM.atomically . STM.modifyTVar' modifiersTVar . Set.delete )
