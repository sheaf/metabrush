module MetaBrush.Application.Action where

-- base
import Data.Word
  ( Word32 )

-- gi-gtk
import qualified GI.Gtk as GTK

-- text
import Data.Text
  ( Text )

-- MetaBrush
import Math.Linear
  ( ℝ(..), T(..) )
import MetaBrush.Application.Context
import MetaBrush.Layer
  ( ChildLayer, RelativePosition )
import MetaBrush.UI.Viewport
  ( Ruler(..) )
import MetaBrush.Unique
  ( Unique )

--------------------------------------------------------------------------------

actionPrefix :: ActionName -> Text

class HandleAction action where
  handleAction :: UIElements -> Variables -> action -> IO ()

instance HandleAction ()

data NewFile = NewFile !TabLocation
instance HandleAction NewFile

data OpenFile = OpenFile !TabLocation
instance HandleAction OpenFile

data OpenFolder = OpenFolder !TabLocation
instance HandleAction OpenFolder

data Save = Save
instance HandleAction Save

data SaveAs = SaveAs
instance HandleAction SaveAs

data Export = Export
instance HandleAction Export

data Close
  = CloseActive
  | CloseThis
    { docToClose :: !Unique }
  | CloseAll
instance HandleAction Close

data SwitchTo =
  SwitchTo
    { newActiveDocUnique :: !Unique
    }
instance HandleAction SwitchTo

data Quit = Quit
instance HandleAction Quit
quitEverything :: GTK.IsWindow window => GTK.Application -> window -> IO ()

data Undo = Undo
instance HandleAction Undo

data Redo = Redo
instance HandleAction Redo

data Cut = Cut
instance HandleAction Cut

data Copy = Copy
instance HandleAction Copy

data Paste = Paste
instance HandleAction Paste

data Duplicate = Duplicate
instance HandleAction Duplicate

data Delete = Delete
instance HandleAction Delete

data DeleteLayer = DeleteLayer !ChildLayer
instance HandleAction DeleteLayer

data NewGroup = NewGroup !RelativePosition !ChildLayer
instance HandleAction NewGroup

data ToggleGuides = ToggleGuides
instance HandleAction ToggleGuides

data Confirm = Confirm
instance HandleAction Confirm

data About = About
instance HandleAction About

data Canvas = Canvas
instance HandleAction Canvas

data OpenPrefs = OpenPrefs
instance HandleAction OpenPrefs

data MouseMove = MouseMove !( ℝ 2 )
instance HandleAction MouseMove

data ActionOrigin
  = ViewportOrigin
  | RulerOrigin !Ruler
data MouseClickType
  = SingleClick
  | DoubleClick
data MouseClick =
  MouseClick
    { clickOrigin :: !ActionOrigin
    , clickType   :: !MouseClickType
    , clickButton :: !Word32
    , clickCoords :: !( ℝ 2 )
    }
instance HandleAction MouseClick

data MouseRelease = MouseRelease !Word32 !( ℝ 2 )
instance HandleAction MouseRelease

data Scroll = Scroll !( Maybe ( ℝ 2 ) ) !( T ( ℝ 2 ) )
instance HandleAction Scroll

data KeyboardPress = KeyboardPress !Word32
instance HandleAction KeyboardPress

data KeyboardRelease = KeyboardRelease !Word32
instance HandleAction KeyboardRelease
