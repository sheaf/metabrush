{-# LANGUAGE OverloadedStrings #-}

module MetaBrush.Application.UpdateDocument where

-- base
import Control.Arrow
  ( (&&&), second )
import Control.Monad
  ( join, void )
import Data.Coerce
  ( coerce )
import Data.Foldable
  ( for_, sequenceA_ )
import Data.Monoid
  ( Ap(..) )
import Data.Traversable
  ( for )

-- containers
import qualified Data.Map.Strict as Map

-- generic-lens
import Data.Generics.Product.Fields
  ( field' )

-- gi-glib
import qualified GI.GLib as GLib

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gtk
import qualified GI.Gtk as GTK

-- lens
import Control.Lens
  ( set )
import Control.Lens.Fold
  ( Fold, foldMapOf, forOf_, sequenceAOf_ )

-- stm
import Control.Concurrent.STM
  ( STM )
import qualified Control.Concurrent.STM as STM

-- text
import Data.Text
  ( Text )

-- transformers
import Control.Monad.Trans.Class
  ( lift )
import Control.Monad.Trans.Maybe
  ( MaybeT(..) )

-- unordered-containers
import qualified Data.HashMap.Lazy as HashMap
  ( lookup )

-- MetaBrush
import MetaBrush.Application.Context
import MetaBrush.Document
  ( Document(..), DocumentContent(..), DocumentMetadata(..)
  )
import MetaBrush.Document.Diff
import MetaBrush.Document.History
  ( DocumentHistory(..), Do(..)
  , atStart, atEnd
  , newFutureStep, affirmPresentSaved
  )
import MetaBrush.GTK.Util
  ( (>>?=) )
import {-# SOURCE #-} MetaBrush.UI.FileBar
  ( removeFileTab )
import MetaBrush.UI.InfoBar
  ( updateInfoBar )
import {-# SOURCE #-} MetaBrush.UI.StrokeTreeView
  ( applyDiffToListModel, switchStrokeView )
import MetaBrush.UI.Viewport
  ( Viewport(..) )
import MetaBrush.Unique
  ( Unique )

--------------------------------------------------------------------------------

-- | Read the currently active document from the stateful variables.
activeDocument :: Variables -> STM ( Maybe ( Unique, DocumentHistory ) )
activeDocument ( Variables { activeDocumentTVar, openDocumentsTVar } )
  =    STM.readTVar activeDocumentTVar
  >>?= ( \ unique -> fmap ( unique , ) . Map.lookup unique <$> STM.readTVar openDocumentsTVar )

-- | Do something with the currently active document.
--
-- Does nothing if no document is currently active.
withActiveDocument :: Variables -> ( Unique -> Document -> STM a ) -> STM ( Maybe a )
withActiveDocument vars f = traverse ( uncurry f ) =<< ( fmap ( second present ) <$> activeDocument vars )

-- TODO: not sure why we need this datatype.
data DocumentUpdate
  = CloseDocument
  | SaveDocument     !( Maybe FilePath )
  | UpdateDocumentTo
    { newDocument  :: !Document
    , documentDiff :: !Diff
    }

data PureDocModification
  = Don'tModifyDoc
  | UpdateDoc !DocumentUpdate

-- | Modify a document, and then perform some subsequent IO action.
--
-- It is assumed that the subsequent IO action does not affect the rest of the application,
-- i.e. it doesn't change any of the STM variables or update the UI.
-- For instance: saving a file to disk.
data DocModification
  = Don'tModifyDocAndThen
    { postModifAction :: IO () }
  | UpdateDocAndThen
    { modifDocument   :: !DocumentUpdate
    , postModifAction :: IO ()
    }

class DocumentModification modif where
  docFold    :: Fold modif DocumentUpdate
  actionFold :: Fold modif ( IO () )

instance DocumentModification PureDocModification where
  docFold _ Don'tModifyDoc            = pure Don'tModifyDoc
  docFold f ( UpdateDoc mbDocInstnt ) = UpdateDoc <$> ( f mbDocInstnt )
  actionFold _ a = pure a

instance DocumentModification DocModification where
  docFold _ don't@( Don'tModifyDocAndThen {} ) = pure don't
  docFold f ( UpdateDocAndThen mbDocInstnt action ) = ( \ new -> UpdateDocAndThen new action ) <$> f mbDocInstnt
  actionFold f modif = ( \ action' -> modif { postModifAction = action' } ) <$> f ( postModifAction modif )

-- | Modify the currently active document.
--
-- Does nothing if no document is currently active.
modifyingCurrentDocument :: DocumentModification modif => UIElements -> Variables -> ( Document -> STM modif ) -> IO ()
modifyingCurrentDocument uiElts vars@( Variables {..} ) f = do
  mbAction <- STM.atomically . runMaybeT $ do
    unique <- MaybeT ( STM.readTVar activeDocumentTVar )
    modifyingDocument uiElts vars unique f
  sequenceA_ mbAction

modifyingDocument :: DocumentModification modif => UIElements -> Variables -> Unique -> ( Document -> STM modif ) -> MaybeT STM ( IO () )
modifyingDocument uiElts@( UIElements { menuActions } ) vars@( Variables {..} ) unique f = do
  oldDoc      <- MaybeT ( fmap present . Map.lookup unique <$> STM.readTVar openDocumentsTVar )
  maxHistSize <- lift ( STM.readTVar maxHistorySizeTVar )
  modif       <- lift ( f oldDoc )
  Ap uiUpdateAction <- lift . getAp $ flip ( foldMapOf docFold ) modif $ Ap . \case
    CloseDocument -> do
      STM.modifyTVar' openDocumentsTVar ( Map.delete unique )
      let change = ActiveDocChange { mbOldDocUnique = Just unique }
      coerce ( updateUIAction change uiElts vars )
    SaveDocument Nothing -> do
      STM.modifyTVar' openDocumentsTVar ( Map.adjust affirmPresentSaved unique )
      coerce ( updateUIAction NoActiveDocChange uiElts vars )
    SaveDocument ( Just newFilePath ) -> do
      STM.modifyTVar' openDocumentsTVar
        ( Map.adjust
          ( affirmPresentSaved
          . set ( field' @"present" . field' @"documentMetadata" . field' @"documentFilePath" )
              ( Just newFilePath )
          )
          unique
        )
      coerce ( updateUIAction NoActiveDocChange uiElts vars )
    UpdateDocumentTo { newDocument, documentDiff = diff } ->
      case diff of
        TrivialDiff -> do
          -- Non-content change.
          STM.modifyTVar' openDocumentsTVar
            ( Map.adjust ( set ( field' @"present" ) newDocument ) unique )
          coerce ( updateUIAction NoActiveDocChange uiElts vars )
        HistoryDiff histDiff -> do
          -- Content change.
          STM.modifyTVar' openDocumentsTVar
            ( Map.adjust
              ( newFutureStep maxHistSize histDiff
              . set ( field' @"documentContent" . field' @"unsavedChanges" ) True
              $ newDocument
              )
              unique
            )
          uiUpdateAction <- updateUIAction NoActiveDocChange uiElts vars
          pure $ Ap do
            case histDiff of
              DocumentDiff {} -> return ()
              HierarchyDiff hDiff ->
                applyDiffToListModel parStoresTVar unique ( Do, hDiff )
              ContentDiff {} -> return ()
            uiUpdateAction
            for_ ( HashMap.lookup ( WinAction "undo" ) menuActions ) ( `GIO.setSimpleActionEnabled` True  )
            for_ ( HashMap.lookup ( WinAction "redo" ) menuActions ) ( `GIO.setSimpleActionEnabled` False )
  pure
    do
      forOf_ docFold modif \ mbNewDoc -> do
        case mbNewDoc of
          CloseDocument -> void $ removeFileTab uiElts vars unique
          _             -> pure ()
      uiUpdateAction
      sequenceAOf_ actionFold modif

-- | A change in which document is currently active.
data ActiveDocChange
  -- | Continue with the same document (or lack of document).
  = NoActiveDocChange
  -- | Change between documents, or open/close a document.
  | ActiveDocChange
    { mbOldDocUnique :: Maybe Unique
    }

updateUIAction :: ActiveDocChange -> UIElements -> Variables -> STM ( IO () )
updateUIAction docChange uiElts@( UIElements { viewport = Viewport {..}, .. } ) vars@( Variables {..} ) = do
  case docChange of
    NoActiveDocChange -> return ()
    ActiveDocChange {} -> do
      -- When changing document, invalidate any state that only made sense
      -- for the previous document.
      STM.writeTVar mousePosTVar      Nothing
      STM.writeTVar mouseHoldTVar     Nothing
      STM.writeTVar documentDragTVar  Nothing
      STM.writeTVar partialPathTVar   Nothing
  mbDocHist <- activeDocument vars
  let
    mbDoc :: Maybe ( Unique, Document )
    mbDoc = second present <$> mbDocHist
    mbTitleText :: Maybe ( Text, Bool )
    mbTitleText = fmap ( ( documentName . documentMetadata &&& unsavedChanges . documentContent ) . snd ) mbDoc
  mbActiveTabDoc <- fmap join $ for mbDoc \ ( docUnique, _doc ) -> do
    mbActiveTab <- Map.lookup docUnique <$> STM.readTVar fileBarTabsTVar
    pure ( (,) <$> mbActiveTab <*> mbDoc )
  pure do
    updateTitle window titleLabel mbTitleText
    updateInfoBar viewportDrawingArea infoBar vars ( fmap ( documentMetadata . snd ) mbDoc )
    _ <- GLib.idleAdd GLib.PRIORITY_DEFAULT_IDLE $ do
          switchStrokeView strokesListView vars ( fst <$> mbDoc )
          return False
    for_ mbActiveTabDoc \ ( FileBarTab { fileBarTabCloseArea }, ( _, _activeDoc ) ) -> do
      GTK.widgetQueueDraw fileBarTabCloseArea
      updateHistoryState uiElts ( fmap snd mbDocHist )
      STM.atomically ( STM.writeTVar redrawStrokesTVar True )

updateTitle :: GTK.IsWindow window => window -> GTK.Label -> Maybe ( Text, Bool ) -> IO ()
updateTitle window titleLabel mbTitleText = do
  GTK.labelSetText titleLabel titleText
  GTK.setWindowTitle window titleText
  where
    titleText :: Text
    titleText = case mbTitleText of
      Nothing -> "MetaBrush"
      Just ( name, hasUnsavedChanges )
        | hasUnsavedChanges
        -> "● " <> name <> " – MetaBrush"
        | otherwise
        -> name <> " – MetaBrush"

updateHistoryState :: UIElements -> Maybe DocumentHistory -> IO ()
updateHistoryState ( UIElements {..} ) mbHist =
  case mbHist of
    Nothing -> do
      for_ ( HashMap.lookup ( WinAction "undo" ) menuActions ) ( `GIO.setSimpleActionEnabled` False )
      for_ ( HashMap.lookup ( WinAction "redo" ) menuActions ) ( `GIO.setSimpleActionEnabled` False )
    Just hist -> do
      if atStart hist
      then for_ ( HashMap.lookup ( WinAction "undo" ) menuActions ) ( `GIO.setSimpleActionEnabled` False )
      else for_ ( HashMap.lookup ( WinAction "undo" ) menuActions ) ( `GIO.setSimpleActionEnabled` True  )
      if atEnd hist
      then for_ ( HashMap.lookup ( WinAction "redo" ) menuActions ) ( `GIO.setSimpleActionEnabled` False )
      else for_ ( HashMap.lookup ( WinAction "redo" ) menuActions ) ( `GIO.setSimpleActionEnabled` True  )
