module MetaBrush.Asset.StrokeIcons
  ( drawVisible, drawInvisible, drawLocked, drawUnlocked )
  where

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- MetaBrush
import MetaBrush.Asset.Colours
  ( ColourRecord(..), Colours )
import MetaBrush.GTK.Util
  ( withRGBA, withRGBAndAlpha )

--------------------------------------------------------------------------------

drawVisible :: Colours -> Cairo.Render Bool
drawVisible ( Colours { bg, highlight } ) = do

  withRGBA bg Cairo.setSourceRGBA

  Cairo.moveTo 1.5 8
  Cairo.curveTo 6 2 10 2 14.5 8
  Cairo.curveTo 10 14 6 14 1.5 8
  Cairo.closePath
  Cairo.fillPreserve

  withRGBA highlight Cairo.setSourceRGBA

  Cairo.newPath
  Cairo.arc 8 8 3.1 0 ( 2 * pi )
  Cairo.closePath
  Cairo.fillPreserve

  withRGBA bg Cairo.setSourceRGBA

  Cairo.newPath
  Cairo.arc 8 8 2.2 0 ( 2 * pi )
  Cairo.closePath
  Cairo.fillPreserve

  withRGBA highlight Cairo.setSourceRGBA

  Cairo.newPath
  Cairo.arc 6.2 6.2 1.8 ( 1.75 * pi ) ( 0.75 * pi )
  Cairo.closePath
  Cairo.fillPreserve

  pure True

drawInvisible :: Colours -> Cairo.Render Bool
drawInvisible ( Colours { bg } ) = do

  withRGBA bg Cairo.setSourceRGBA

  Cairo.moveTo 1.5 8
  Cairo.curveTo 6 2 10 2 14.5 8
  Cairo.curveTo 10 14 6 14 1.5 8
  Cairo.closePath
  Cairo.fillPreserve

  pure True

drawLocked :: Colours -> Cairo.Render Bool
drawLocked ( Colours { .. } ) = do

  withRGBA bg Cairo.setSourceRGBA

  Cairo.newPath
  Cairo.moveTo  3 6.5
  Cairo.lineTo  3 12
  Cairo.arcNegative 4 12 1 pi ( 0.5 * pi )
  Cairo.lineTo 12.5 13
  Cairo.arcNegative 12 12 1 ( 0.5 * pi ) 0
  Cairo.lineTo 13 6.5
  Cairo.closePath
  Cairo.fillPreserve

  Cairo.newPath
  Cairo.moveTo 5 6.5
  Cairo.lineTo 5 5
  Cairo.arc 8 5 3 pi 0
  Cairo.lineTo 11 6.5
  Cairo.setLineWidth 1.5
  Cairo.stroke

  pure True

drawUnlocked :: Colours -> Cairo.Render Bool
drawUnlocked ( Colours { .. } ) = do

  withRGBAndAlpha bg 0.5 Cairo.setSourceRGBA

  Cairo.newPath
  Cairo.moveTo  3 6.5
  Cairo.lineTo  3 12
  Cairo.arcNegative 4 12 1 pi ( 0.5 * pi )
  Cairo.lineTo 12.5 13
  Cairo.arcNegative 12 12 1 ( 0.5 * pi ) 0
  Cairo.lineTo 13 6.5
  Cairo.closePath
  Cairo.fillPreserve

  Cairo.newPath
  Cairo.moveTo 5 6.5
  Cairo.lineTo 5 5
  Cairo.arc 8 5 3 pi ( 1.8 * pi )
  Cairo.setLineWidth 1.5
  Cairo.stroke

  pure True