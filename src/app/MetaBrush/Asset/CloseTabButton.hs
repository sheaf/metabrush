module MetaBrush.Asset.CloseTabButton
  ( drawCloseTabButton )
  where

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- gi-gtk
import qualified GI.Gtk as GTK

-- MetaBrush
import MetaBrush.Asset.Colours
  ( ColourRecord(..), Colours )
import MetaBrush.GTK.Util
  ( withRGBA )

--------------------------------------------------------------------------------

-- | "Close tab" button.
drawCloseTabButton :: Colours -> Bool -> [ GTK.StateFlags ] -> Cairo.Render Bool
drawCloseTabButton ( Colours {..} ) unsavedChanges flags = do


  if unsavedChanges
  then do
    if clicked
    then withRGBA close Cairo.setSourceRGBA
    else withRGBA plain Cairo.setSourceRGBA
    Cairo.arc 5 11 5 0 ( 2 * pi )
    Cairo.fill
    if clicked
    then withRGBA bg    Cairo.setSourceRGBA
    else
      if hover
      then withRGBA close Cairo.setSourceRGBA
      else withRGBA bg    Cairo.setSourceRGBA
  else
    if clicked
    then do
      Cairo.setLineWidth 5
      Cairo.setLineCap  Cairo.LineCapRound
      withRGBA close Cairo.setSourceRGBA
      drawCross

      withRGBA bg    Cairo.setSourceRGBA
    else
      if hover
      then withRGBA close Cairo.setSourceRGBA
      else withRGBA plain Cairo.setSourceRGBA

  Cairo.setLineWidth 2
  Cairo.setLineCap  Cairo.LineCapButt
  drawCross

  pure True
    where
      hover, clicked :: Bool
      hover   = GTK.StateFlagsPrelight `elem` flags
      clicked = GTK.StateFlagsActive   `elem` flags

drawCross :: Cairo.Render ()
drawCross = do
  Cairo.newPath
  Cairo.moveTo 2.5  8.5
  Cairo.lineTo 7.5 13.5
  Cairo.stroke

  Cairo.moveTo 2.5 13.5
  Cairo.lineTo 7.5  8.5
  Cairo.stroke
