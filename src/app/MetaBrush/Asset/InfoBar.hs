module MetaBrush.Asset.InfoBar
  ( drawMagnifier, drawTopLeftCornerRect )
  where

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- MetaBrush
import MetaBrush.Asset.Colours
  ( ColourRecord(..), Colours )
import MetaBrush.GTK.Util
  ( withRGBA )

--------------------------------------------------------------------------------

-- | Magnifying glass icon. Width = 19, height = 25.
drawMagnifier :: Colours -> Cairo.Render Bool
drawMagnifier ( Colours { magnifier, glass } ) = do

  -- Magnifying glass.
  withRGBA glass Cairo.setSourceRGBA
  Cairo.newPath
  Cairo.moveTo  18.191406  6.144531
  Cairo.curveTo 19.238281 10.191406 16.804688 14.324219 12.753906 15.371094
  Cairo.curveTo  8.703125 16.417969  4.574219 13.984375  3.527344  9.933594
  Cairo.curveTo  2.480469  5.882813  4.914063  1.753906  8.964844  0.707031
  Cairo.curveTo 13.011719 -0.339844 17.144531  2.09375  18.191406  6.144531
  Cairo.closePath
  Cairo.fill

  -- Magnifier.
  withRGBA magnifier Cairo.setSourceRGBA
  Cairo.newPath
  Cairo.moveTo   11.144531  0.00390625
  Cairo.curveTo  8.296875  -0.09375   5.484375   1.332031  3.933594  3.960938
  Cairo.curveTo  1.871094   7.464844  2.980469  12.0625    6.089844 14.515625
  Cairo.curveTo  5.878906  14.90625   5.226563  16.132813  4.554688 15.75
  Cairo.curveTo  4.300781  15.609375  3.878906  15.882813  3.605469 16.363281
  Cairo.lineTo   0.128906  22.34375
  Cairo.curveTo -0.140625  22.828125  0.0273438 23.4375    0.503906 23.707031
  Cairo.lineTo   2.320313  24.738281
  Cairo.curveTo  2.800781  25.011719  3.410156  24.839844  3.683594 24.363281
  Cairo.lineTo   7.148438  18.210938
  Cairo.curveTo  7.421875  17.730469  7.34375   17.394531  7.09375  17.246094
  Cairo.curveTo  6.445313  16.855469  7.1875    15.789063  7.449219 15.332031
  Cairo.curveTo  11.136719 16.988281 15.683594  15.683594  17.78125 12.117188
  Cairo.curveTo  20.035156  8.292969 18.761719   3.367188  14.9375   1.113281
  Cairo.curveTo  13.742188  0.410156 12.441406   0.0507813 11.144531 0.00390625
  Cairo.closePath
  Cairo.moveTo   10.855469  1.375
  Cairo.curveTo  13.894531  1.371094 16.550781  3.429688 17.3125    6.371094
  Cairo.curveTo  18.234375  9.9375   16.089844 13.570313 12.527344 14.492188
  Cairo.curveTo  8.960938  15.414063  5.328125 13.269531  4.40625   9.707031
  Cairo.curveTo  3.484375   6.144531  5.625     2.507813  9.191406  1.585938
  Cairo.curveTo  9.734375   1.445313 10.292969  1.375    10.855469  1.375
  Cairo.closePath
  Cairo.setFillRule Cairo.FillRuleWinding
  Cairo.fill

  pure True

drawTopLeftCornerRect :: Colours -> Cairo.Render Bool
drawTopLeftCornerRect ( Colours { bg, viewport } ) = do

  Cairo.newPath
  Cairo.moveTo  8 10
  Cairo.lineTo 32 10
  Cairo.lineTo 32 30
  Cairo.lineTo  8 30
  Cairo.closePath

  withRGBA viewport Cairo.setSourceRGBA
  Cairo.fill

  Cairo.newPath
  Cairo.moveTo 12 24
  Cairo.lineTo 12 14
  Cairo.lineTo 24 14

  Cairo.setLineWidth 4
  withRGBA bg Cairo.setSourceRGBA
  Cairo.stroke

  pure True
