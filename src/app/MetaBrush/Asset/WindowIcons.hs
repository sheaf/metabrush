module MetaBrush.Asset.WindowIcons
  ( drawMinimise, drawRestoreDown, drawMaximise, drawClose )
  where

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- MetaBrush
import MetaBrush.Asset.Colours
  ( ColourRecord(..), Colours )
import MetaBrush.GTK.Util
  ( withRGBA )

--------------------------------------------------------------------------------

-- | Minimise window icon.
drawMinimise :: Colours -> Cairo.Render Bool
drawMinimise ( Colours { plain } ) = do

  withRGBA plain Cairo.setSourceRGBA

  Cairo.newPath
  Cairo.moveTo  6 13
  Cairo.lineTo  6 17
  Cairo.lineTo 18 17
  Cairo.lineTo 18 13
  Cairo.closePath
  Cairo.fillPreserve

  pure True

-- | Restore down window icon.
drawRestoreDown :: Colours -> Cairo.Render Bool
drawRestoreDown ( Colours { plain } ) = do

  withRGBA plain Cairo.setSourceRGBA

  Cairo.newPath
  Cairo.moveTo  8  4
  Cairo.lineTo  8  9
  Cairo.lineTo  4  9
  Cairo.lineTo  4 19
  Cairo.lineTo 15 19
  Cairo.lineTo 15 14
  Cairo.lineTo 19 14
  Cairo.lineTo 19  4
  Cairo.closePath
  Cairo.moveTo  9  7
  Cairo.lineTo 18  7
  Cairo.lineTo 18 13
  Cairo.lineTo 15 13
  Cairo.lineTo 15  9
  Cairo.lineTo  9  9
  Cairo.closePath
  Cairo.moveTo  5 12
  Cairo.lineTo 14 12
  Cairo.lineTo 14 18
  Cairo.lineTo  5 18
  Cairo.closePath
  Cairo.fillPreserve

  pure True

-- | Maximise window icon.
drawMaximise :: Colours -> Cairo.Render Bool
drawMaximise ( Colours { plain } ) = do

  withRGBA plain Cairo.setSourceRGBA

  Cairo.newPath  
  Cairo.moveTo  6  5
  Cairo.lineTo  6 18
  Cairo.lineTo 19 18
  Cairo.lineTo 19  5
  Cairo.closePath
  Cairo.moveTo  7  9
  Cairo.lineTo 18  9
  Cairo.lineTo 18 17
  Cairo.lineTo  7 17
  Cairo.closePath
  Cairo.fillPreserve

  pure True

-- | Close window icon.
drawClose :: Colours -> Cairo.Render Bool
drawClose ( Colours { plain } ) = do

  Cairo.setLineWidth 2
  withRGBA plain Cairo.setSourceRGBA

  Cairo.setLineCap  Cairo.LineCapSquare
  Cairo.setLineJoin Cairo.LineJoinMiter

  Cairo.newPath 
  Cairo.moveTo  6.5  6.5
  Cairo.lineTo 17.5 17.5
  Cairo.strokePreserve

  Cairo.newPath
  Cairo.moveTo  6.5 17.5
  Cairo.lineTo 17.5  6.5
  Cairo.strokePreserve

  pure True
