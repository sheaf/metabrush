module MetaBrush.Asset.Cursor
  ( drawCursor, drawCursorIcon )
  where

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- MetaBrush
import MetaBrush.Asset.Colours
  ( ColourRecord(..), Colours )
import MetaBrush.GTK.Util
  ( withRGBA )

--------------------------------------------------------------------------------

-- | "Selection" mouse cursor. 18 x 18.
drawCursor :: Colours -> Cairo.Render Bool
drawCursor ( Colours { cursor, cursorOutline } ) = do

  Cairo.newPath
  Cairo.moveTo  12.5 10.5
  Cairo.curveTo  5.5 11.5 4.5 12.5 0.5 17.5
  Cairo.lineTo   0.5  0
  Cairo.closePath

  withRGBA cursor Cairo.setSourceRGBA
  Cairo.fillPreserve

  Cairo.setLineWidth 1
  withRGBA cursorOutline Cairo.setSourceRGBA
  Cairo.strokePreserve

  pure True

-- | "Selection" tool icon. 40 x 40.
drawCursorIcon :: Colours -> Cairo.Render Bool
drawCursorIcon ( Colours { cursor } )  = do

  withRGBA cursor Cairo.setSourceRGBA
  Cairo.newPath
  Cairo.moveTo  29 22
  Cairo.curveTo 20 22 18 24 12 31
  Cairo.lineTo  12 8.5
  Cairo.closePath
  Cairo.fillPreserve

  pure True
