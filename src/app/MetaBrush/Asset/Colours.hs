{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -fno-warn-deprecations #-}

module MetaBrush.Asset.Colours
  ( ColourRecord(..)
  , Colours, getColours
  , renderColoursFromColours
  )
  where

-- base
import Data.Traversable
  ( for )

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-gtk
import qualified GI.Gtk as GTK

-- text
import Data.Text
  ( Text )

-- MetaBrush
import MetaBrush.Render.Stroke
  ( RenderColours(..), RGBA(..) )
import MetaBrush.GTK.Util
  ( withRGBA )

--------------------------------------------------------------------------------

data ColourRecord a
  = Colours
  { bg, active, close, highlight
  , cursor, cursorOutline, cursorIndicator
  , plain, empty, emptyOutline
  , base, splash
  , pathPoint, pathPointOutline
  , controlPoint, controlPointLine, controlPointOutline
  , path, brush, brushStroke, brushCenter
  , pointHover, pointSelected
  , viewport, viewportScrollbar, tabScrollbar
  , guide, rulerBg, rulerTick, magnifier, glass
  , selected, selectedOutline
  , brushWidget, brushWidgetHover :: !a
  }
  -- NB: when adding a new colour to this record, don't forget to update:
  --
  --  - colours.css, to define a value for the colour,
  --  - theme.css, which is where GTK parses the colours in 'getColours'.
  deriving stock ( Show, Functor, Foldable, Traversable )

newtype ColourName
  = ColourName { colourName :: Text }
  deriving stock Show

colourNames :: ColourRecord ColourName
colourNames = Colours
  { bg                  = ColourName "bg"
  , active              = ColourName "active"
  , close               = ColourName "close"
  , highlight           = ColourName "highlight"
  , cursor              = ColourName "cursor"
  , cursorOutline       = ColourName "cursorStroke"
  , cursorIndicator     = ColourName "cursorIndicator"
  , plain               = ColourName "plain"
  , base                = ColourName "base"
  , empty               = ColourName "empty"
  , emptyOutline        = ColourName "emptyOutline"
  , splash              = ColourName "splash"
  , pathPoint           = ColourName "pathPoint"
  , pathPointOutline    = ColourName "pathPointStroke"
  , controlPoint        = ColourName "controlPoint"
  , controlPointLine    = ColourName "controlPointLine"
  , controlPointOutline = ColourName "controlPointStroke"
  , path                = ColourName "path"
  , brush               = ColourName "brush"
  , brushStroke         = ColourName "brushStroke"
  , brushCenter         = ColourName "brushCenter"
  , pointHover          = ColourName "pointHover"
  , pointSelected       = ColourName "pointSelected"
  , viewport            = ColourName "viewport"
  , viewportScrollbar   = ColourName "viewportScrollbar"
  , tabScrollbar        = ColourName "tabScrollbar"
  , rulerBg             = ColourName "ruler"
  , rulerTick           = ColourName "rulerTick"
  , guide               = ColourName "guide"
  , magnifier           = ColourName "magnifier"
  , glass               = ColourName "glass"
  , selected            = ColourName "selected"
  , selectedOutline     = ColourName "selectedOutline"
  , brushWidget         = ColourName "brushWidget"
  , brushWidgetHover    = ColourName "brushWidgetHover"
  }

type Colours = ColourRecord GDK.RGBA

getColours
  :: ( GTK.IsStyleProvider styleProvider )
  => styleProvider -> IO Colours
getColours _provider =
  for colourNames \ ( ColourName { colourName } ) -> do
    widget <- GTK.fixedNew
    GTK.widgetAddCssClass widget colourName
    GTK.widgetGetColor widget

renderColoursFromColours
  :: Colours -> IO ( RenderColours RGBA )
renderColoursFromColours ( Colours { .. } ) = do
  let
    renderColours :: RenderColours GDK.RGBA
    renderColours = RenderColours { .. }
    getOne :: GDK.RGBA -> IO RGBA
    getOne rgba = withRGBA rgba ( \ r g b a -> return $ RGBA { r, g, b, a } )
  traverse getOne renderColours
