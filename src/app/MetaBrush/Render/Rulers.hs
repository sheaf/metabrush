{-# LANGUAGE ScopedTypeVariables #-}

module MetaBrush.Render.Rulers
  ( renderRuler )
  where

-- base
import Control.Arrow
  ( first )
import Control.Monad
  ( when )
import Data.Fixed
  ( mod' )
import Data.Foldable
  ( for_, traverse_, toList )
import Data.Int
  ( Int32 )
import Data.List.NonEmpty
  ( NonEmpty(..) )
import qualified Data.List.NonEmpty as NonEmpty
  ( reverse )

-- acts
import Data.Act
  ( Act
    ( (•) )
  , Torsor
    ( (-->) )
  )

-- containers
import Data.Map.Strict
  ( Map )
import qualified Data.Map.Strict as Map

-- generic-lens
import Data.Generics.Product.Fields
  ( field' )

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- lens
import Control.Lens
  ( over )

-- MetaBrush
import Math.Linear
  ( ℝ(..), T(..) )
import MetaBrush.Application.Action
  ( ActionOrigin(..) )
import MetaBrush.Asset.Colours
  ( Colours, ColourRecord(..) )
import MetaBrush.Application.Context
  ( MouseHold(..), HoldAction(..), GuideAction(..) )
import MetaBrush.Document
import MetaBrush.Hover
import MetaBrush.UI.Coordinates
  ( toViewportCoordinates )
import MetaBrush.UI.Viewport
  ( Ruler(..) )
import MetaBrush.Unique
  ( Unique )
import MetaBrush.GTK.Util
  ( withRGBA )

--------------------------------------------------------------------------------

renderRuler
  :: Colours -> ( Int32, Int32 ) -> ActionOrigin -> ( Int32, Int32 )
  -> Maybe ( ℝ 2 ) -> Maybe MouseHold -> Bool
  -> Document
  -> Cairo.Render ()
renderRuler
  cols@( Colours {..} ) ( viewportWidth, viewportHeight ) actionOrigin ( width, height )
  mbMousePos mbMouseHold showGuides
  ( Document
    { documentMetadata =
      Metadata
        { viewportCenter = center@( ℝ2 cx cy )
        , documentZoom   = zoom@( Zoom { zoomFactor } )
        , documentGuides = guides0
        }
    } ) = do

    let
      guides1 :: Map Unique ( Guide, Bool )
      guides1 = fmap ( , False ) guides0

      modifiedGuides :: [ ( Guide, Bool ) ]
      modifiedGuides
        | Just
            MouseHold
              { holdStartPos = mousePos0
              , holdAction   = GuideAction act
              } <- mbMouseHold
        , Just mousePos <- mbMousePos
        = case act of
            MoveGuide guideUnique
              ->
                let
                  translate :: ℝ 2 -> ℝ 2
                  translate = ( ( mousePos0 --> mousePos :: T ( ℝ 2 ) ) • )
                in toList
                 $ Map.adjust
                      ( \ ( g, _ ) -> ( over ( field' @"guidePoint" ) translate g, True ) )
                      guideUnique
                      guides1
            CreateGuide ruler
              -> let
                  addNewGuides :: [ ( Guide, Bool ) ] -> [ ( Guide, Bool ) ]
                  addNewGuides gs = case ruler of
                    RulerCorner
                      -> ( Guide { guidePoint = mousePos, guideNormal = V2 0 1 }, True )
                       : ( Guide { guidePoint = mousePos, guideNormal = V2 1 0 }, True )
                       : gs
                    LeftRuler
                      -> ( Guide { guidePoint = mousePos, guideNormal = V2 1 0 }, True )
                       : gs
                    TopRuler
                      -> ( Guide { guidePoint = mousePos, guideNormal = V2 0 1 }, True )
                       : gs
                 in addNewGuides ( toList guides1 )
        | otherwise
        = toList guides1

      mbHoverContext :: Maybe HoverContext
      mbHoverContext
        | Just mp@( ℝ2 x y ) <- mbMousePos
        , x <= left || y <= top -- only hover guides from within ruler area
        = Just ( MouseHover mp )
        | otherwise
        = Nothing

    Cairo.save
    Cairo.translate ( 0.5 + 0.5 * fromIntegral viewportWidth ) ( 0.5 + 0.5 * fromIntegral viewportHeight )
    additionalAdjustment
    Cairo.scale zoomFactor zoomFactor
    Cairo.translate ( -cx ) ( -cy )

    -- Render tick marks.
    renderTicks
    -- Render guides.
    when showGuides ( for_ modifiedGuides ( renderGuide cols mbHoverContext zoom ) )
    -- Render mouse cursor indicator.
    for_ mbMousePos \ ( ℝ2 mx my ) ->
      case actionOrigin of
        RulerOrigin TopRuler  -> do
          Cairo.save
          Cairo.translate mx top
          Cairo.scale ( 1 / zoomFactor ) ( 1 / zoomFactor )
          Cairo.moveTo 0 0
          Cairo.lineTo -3 -6
          Cairo.lineTo  3 -6
          Cairo.closePath
          withRGBA cursorIndicator Cairo.setSourceRGBA
          Cairo.fill
          Cairo.restore
        RulerOrigin LeftRuler -> do
          Cairo.save
          Cairo.translate left my
          Cairo.moveTo 0 0
          Cairo.scale ( 1 / zoomFactor ) ( 1 / zoomFactor )
          Cairo.lineTo -6 -3
          Cairo.lineTo -6  3
          Cairo.closePath
          withRGBA cursorIndicator Cairo.setSourceRGBA
          Cairo.fill
          Cairo.restore

        _ -> pure ()

    Cairo.restore
    pure ()

  where
    dx, dy :: Double
    dx = fromIntegral width
    dy = fromIntegral height
    left, right, top, bottom :: Double
    ℝ2 left  top    = toViewport ( ℝ2 0 0 )
    ℝ2 right bottom = toViewport ( ℝ2 ( fromIntegral viewportWidth ) ( fromIntegral viewportHeight ) )
    additionalAdjustment :: Cairo.Render ()
    additionalAdjustment = case actionOrigin of
      ViewportOrigin -> pure ()
      RulerOrigin ruler -> case ruler of
        RulerCorner -> do
          Cairo.translate dx dy
        LeftRuler -> do
          Cairo.translate dx 0
        TopRuler -> do
          Cairo.translate 0 dy
    toViewport :: ℝ 2 -> ℝ 2
    toViewport = toViewportCoordinates zoom ( fromIntegral viewportWidth, fromIntegral viewportHeight ) center

    setTickRenderContext :: Cairo.Render ()
    setTickRenderContext = do
      Cairo.setLineWidth 1
      Cairo.setLineCap Cairo.LineCapButt
      withRGBA rulerTick Cairo.setSourceRGBA
      Cairo.selectFontFace "Fira Code" Cairo.FontSlantNormal Cairo.FontWeightNormal
      Cairo.setFontSize 8

    renderTicks :: Cairo.Render ()
    renderTicks = case actionOrigin of
      ViewportOrigin -> pure ()
      RulerOrigin ruler ->
        let
          smallSpacing, bigSpacing :: Double
          subdivs, displayedSubdivs :: [ Int ]
          ( bigSpacing, subdivs ) = tickSpacing ( 60 / zoomFactor )
          smallSpacing = bigSpacing / fromIntegral ( product displayedSubdivs )
          displayedSubdivs = dropTightTicks subdivs
          dropTightTicks :: [ Int ] -> [ Int ]
          dropTightTicks = go 1
            where
              go :: Double -> [Int] -> [Int]
              go _ [] = []
              go s ( x : xs )
                | s' > threshold
                = []
                | otherwise
                = x : go s' xs
                where
                  s', threshold :: Double
                  s' = s * fromIntegral x
                  threshold = 0.25 * bigSpacing * zoomFactor
          increasingSubdivs :: NonEmpty ( Int, ( Double, Bool ) )
          increasingSubdivs = NonEmpty.reverse $ ( 1, ( 15, True ) ) :| zip displayedSubdivs [ ( 8, False ), ( 6, False ), ( 4, False ) ]
        in case ruler of
          RulerCorner -> pure ()
          TopRuler    -> do
            let
              start :: Double
              start = truncateWith bigSpacing left
            setTickRenderContext
            traverse_ renderTickV $
              [ Tick {..}
              | ( i :: Int ) <- [ 0, 1 .. ceiling ( ( right - start ) / smallSpacing ) ]
              , let
                  ( tickSize, tickHasLabel ) = subdivAt i increasingSubdivs
                  tickPosition = start + fromIntegral i * smallSpacing
              ]
          LeftRuler   -> do
            let
              start :: Double
              start = truncateWith bigSpacing top
            setTickRenderContext
            traverse_ renderTickH $
              [ Tick {..}
              | ( i :: Int ) <- [ 0, 1 .. ceiling ( ( bottom - start ) / smallSpacing ) ]
              , let
                  ( tickSize, tickHasLabel ) = subdivAt i increasingSubdivs
                  tickPosition = start + fromIntegral i * smallSpacing
              ]

    renderTickV, renderTickH :: Tick -> Cairo.Render ()
    renderTickV ( Tick {..} ) = do
      Cairo.save
      Cairo.translate tickPosition top
      Cairo.scale ( 1 / zoomFactor ) ( 1 / zoomFactor )
      Cairo.moveTo 0 0
      Cairo.lineTo 0 -tickSize
      Cairo.stroke
      when tickHasLabel do
        Cairo.translate 2 -8.5
        Cairo.moveTo 0 0
        Cairo.showText ( show $ round @_ @Int tickPosition )
      Cairo.restore
    renderTickH ( Tick {..} ) = do
      Cairo.save
      Cairo.translate left tickPosition
      Cairo.scale ( 1 / zoomFactor ) ( 1 / zoomFactor )
      Cairo.moveTo 0           0
      Cairo.lineTo (-tickSize) 0
      Cairo.stroke
      when tickHasLabel do
        if tickPosition < 0
        then Cairo.translate -14.5  6.5
        else Cairo.translate -14.5 10.5
        for_ ( show $ round @_ @Int tickPosition ) \ char -> do
          Cairo.moveTo 0 0
          Cairo.showText [char]
          Cairo.translate 0 8
      Cairo.restore

data Tick
  = Tick
  { tickPosition :: Double
  , tickSize     :: Double
  , tickHasLabel :: Bool
  }
  deriving stock Show

renderGuide :: Colours -> Maybe HoverContext -> Zoom -> ( Guide, Bool ) -> Cairo.Render ()
renderGuide ( Colours {..} ) mbHover zoom@( Zoom { zoomFactor } )
  ( gd@( Guide { guidePoint = ℝ2 x y, guideNormal = V2 nx ny } ), guideSelected )
  = do

    Cairo.save
    Cairo.translate x y
    Cairo.scale ( 1 / zoomFactor ) ( 1 / zoomFactor )

    Cairo.setLineWidth 1
    let isHovered
          | Just hov <- mbHover
          = hovered hov zoom gd
          | otherwise
          = False
    if guideSelected || isHovered
    then withRGBA pointHover Cairo.setSourceRGBA
    else withRGBA guide      Cairo.setSourceRGBA

    Cairo.moveTo (  1e5 * ny ) ( -1e5 * nx )
    Cairo.lineTo ( -1e5 * ny ) (  1e5 * nx )
    Cairo.stroke

    Cairo.restore

tickSpacing :: Double -> ( Double, [ Int ] )
tickSpacing d
  | d <= 1 = ( 1, cycle [ 2, 5 ] )
  | d <= 2 = ( 2, 2 : cycle [ 2, 5 ] )
  | d <= 5 = ( 5, cycle [ 5, 2 ] )
  | otherwise
  = first ( 10 * ) $ tickSpacing ( 0.1 * d )

subdivAt :: Int -> NonEmpty ( Int, d ) -> d
subdivAt _ ( ( _, d ) :| [] )  = d
subdivAt i ( ( t, d ) :| ( nxt : nxts ) )
  | r == 0
  = subdivAt q ( nxt :| nxts )
  | otherwise
  = d
  where
    ( q, r ) = i `divMod` t

truncateWith :: Double -> Double -> Double
truncateWith m x = x - ( x `mod'` m )
