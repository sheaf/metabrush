{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MetaBrush.Render.Document
  ( getDocumentRender, blankRender )
  where

-- base
import Control.Monad
  ( when )
import Control.Monad.ST
  ( RealWorld, ST )
import Data.Foldable
  ( for_ )
import Data.Function
  ( on )
import Data.Functor.Compose
  ( Compose(..) )
import Data.Int
  ( Int32 )

-- acts
import Data.Act
  ( Act
    ( (•) )
  , Torsor
    ( (-->) )
  )

-- containers
import Data.Set
  ( Set )

-- deepseq
import Control.DeepSeq
  ( deepseq )

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- transformers
import Control.Monad.Trans.Writer.CPS as Writer

-- brush-strokes
import Math.Bezier.Cubic.Fit
  ( FitParameters )
import Math.Bezier.Spline
import Math.Bezier.Stroke
  ( RootSolvingAlgorithm )
import Math.Linear
  ( ℝ(..), T(..) )
import Math.Root.Isolation
  ( RootIsolationOptions )

-- MetaBrush
import MetaBrush.Action
  ( dragUpdate )
import MetaBrush.Asset.Colours
  ( Colours )
import MetaBrush.Application.Context
  ( Modifier(..)
  , MouseHold(..), HoldAction(..), PartialPath(..)
  )
import MetaBrush.Document
import MetaBrush.Document.Serialise
  ( ) -- 'Serialisable' instances
import MetaBrush.Draw
import MetaBrush.Hover
  ( HoverContext(..)
  , inSmallPointClickRange, mkAABB
  )
import MetaBrush.Layer
  ( WithinParent(..) )
import MetaBrush.Render.Stroke
import MetaBrush.Stroke
import MetaBrush.UI.ToolBar
  ( Mode(..) )
import MetaBrush.Unique
  ( Unique )

--------------------------------------------------------------------------------

blankRender :: Colours -> Cairo.Render ()
blankRender _ = pure ()

getDocumentRender
  :: RenderColours RGBA
  -> RootSolvingAlgorithm
  -> Maybe ( RootIsolationOptions 1 1, RootIsolationOptions 1 2, RootIsolationOptions 2 3 )
  -> FitParameters
  -> Mode -> Bool
  -> Set Modifier -> Maybe ( ℝ 2 ) -> Maybe MouseHold -> Maybe PartialPath
  -> Document
  -> ST RealWorld ( ( Int32, Int32 ) -> Cairo.Render () )
getDocumentRender
  cols
  rootAlgo mbCuspOptions fitParams
  mode debug
  modifiers mbMousePos mbHoldEvent mbPartialPath
  doc@( Document
        { documentMetadata =
            Metadata
              { viewportCenter = ℝ2 cx cy
              , documentZoom   = zoom@( Zoom { zoomFactor } )
              , documentSize   = mbDocSize
              , selectedPoints = selPts
              }
        } )
    = do

      let
        -- Get any modifications from in-flight user actions (e.g. in the middle of dragging something).
        modifiedStrokes :: [ ( Maybe Unique, Bool, Stroke ) ]
        modifiedStrokes = case mode of
          PathMode
            | Just
                MouseHold
                  { holdStartPos   = p0
                  , holdNonTrivial = True
                  , holdAction     = DragMoveHold { dragAction }
                  } <- mbHoldEvent
            , Just p1 <- mbMousePos
            , p0 /= p1
            -- Non-trivial drag in progress.
            , let
                alternateMode :: Bool
                alternateMode = any ( \case { Alt _ -> True; _ -> False } ) modifiers
            , Just ( doc', _ ) <- dragUpdate p0 p1 dragAction alternateMode doc
            -> getVisibleStrokes doc'
            | Just ( PartialPath anchor cp0 firstPoint ) <- mbPartialPath
            , let
                p0 = anchorPos anchor doc
                mbFinalPoint :: Maybe ( ℝ 2 )
                mbControlPoint :: Maybe ( ℝ 2 )
                ( mbFinalPoint, mbControlPoint )
                  | Just
                      MouseHold
                        { holdStartPos   = holdPos
                        , holdNonTrivial = True
                        , holdAction     = DrawHold
                        } <- mbHoldEvent
                  = case firstPoint of
                      Just {} -> ( mbMousePos  , Nothing )
                      Nothing -> ( Just holdPos
                                 , do { cp <- mbMousePos
                                      ; return $ ( cp --> holdPos :: T ( ℝ 2 ) ) • holdPos
                                      }
                                 )
                  | otherwise
                  = ( mbMousePos, Nothing )
            , Just finalPoint <- mbFinalPoint
            -- Path drawing operation in progress.
            , let
                previewSpline :: Spline Open () ( PointData () )
                previewSpline = catMaybesSpline ( inSmallPointClickRange zoom `on` coords ) ()
                  ( PointData p0 () )
                  ( do
                      cp <- cp0
                      pure ( PointData cp () )
                  )
                  ( do
                      cp <- mbControlPoint
                      pure ( PointData cp () )
                  )
                  ( PointData finalPoint () )
                doc' = addToAnchor False anchor previewSpline doc
            -> getVisibleStrokes doc'
          _ -> getVisibleStrokes doc

      strokesRenderData <-
        traverse
         ( \ ( mbUnique, isLocked, stroke ) ->
             ( mbUnique, isLocked, ) <$>
               strokeRenderData rootAlgo mbCuspOptions fitParams stroke
         )
         modifiedStrokes

      let
        wantedElements isLocked =
          -- When locked, only draw the stroke and not
          -- the individual path points or brushes.
          if isLocked
          then
            WantedElements
              { wantPoints  = False
              , wantPaths   = False
              , wantBrushes = False
              }
          else
            WantedElements
              { wantPoints  = not ( mode == MetaMode )
              , wantPaths   = not ( mode == MetaMode )
              , wantBrushes = mode == BrushMode
              }

        renderSelectionRect :: Cairo.Render ()
        mbHoverContext :: Maybe HoverContext
        ( renderSelectionRect, mbHoverContext )
          | Just
              MouseHold
              { holdStartPos   = p0
              , holdNonTrivial = True
              , holdAction     = SelectionHold
              } <- mbHoldEvent
          , Just p1 <- mbMousePos
          = ( drawSelectionRectangle cols zoomFactor p0 p1, Just $ RectangleHover ( mkAABB p0 p1 ) )
          | otherwise
          = ( pure (), MouseHover <$> mbMousePos )

        drawingInstructions :: ( Int32, Int32 ) -> Cairo.Render ()
        drawingInstructions ( viewportWidth, viewportHeight ) = do
          Cairo.save
          Cairo.translate ( 0.5 + 0.5 * fromIntegral viewportWidth ) ( 0.5 + 0.5 * fromIntegral viewportHeight )
          Cairo.scale zoomFactor zoomFactor
          Cairo.translate -cx -cy

          -- Give the document area the base colour.
          case mbDocSize of
            Nothing -> do
              -- No document size: fill the viewport with the base colour.
              withRGBA ( base cols ) Cairo.setSourceRGBA
              Cairo.paint
            Just docSize ->
              -- Document size: fill within the document boundary.
              drawRectangle ( Just $ base cols, Nothing ) zoomFactor
                ( documentTopLeft docSize )
                ( documentBottomRight docSize )

          -- Draw the main content (the strokes).
          for_ strokesRenderData $ \ ( mbStrokeUnique, isLocked, stroke ) -> do
            compositeRenders $ getCompose $
              renderStroke cols ( wantedElements isLocked ) selPts mbHoverContext debug zoom
                ( mbStrokeUnique, stroke )

          case mbDocSize of
            Nothing ->
              return ()
            Just docSize ->
              -- Then draw the document boundary over the top of the strokes.
              drawRectangle ( Nothing, Just $ emptyOutline cols ) zoomFactor
                ( documentTopLeft docSize )
                ( documentBottomRight docSize )

          -- Finally, draw the selection rectangle (if any).
          renderSelectionRect

          Cairo.restore

      strokesRenderData `deepseq` pure drawingInstructions

getVisibleStrokes :: Document -> [ ( Maybe Unique, Bool, Stroke ) ]
getVisibleStrokes ( Document { documentMetadata, documentContent } ) =
  Writer.execWriter $
    forStrokeHierarchy
       ( layerMetadata documentMetadata )
       ( strokeHierarchy documentContent )
       ( \ ( WithinParent _ uniq ) stroke ( StrokeMetadata { strokeVisible, strokeLocked } ) -> do
          when strokeVisible $
            Writer.tell [ ( Just uniq, strokeLocked, stroke ) ]
          return PreserveStroke
       )
