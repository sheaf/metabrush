{-# LANGUAGE OverloadedStrings #-}

module MetaBrush.UI.InfoBar
  ( InfoBar(..), createInfoBar, updateInfoBar )
  where

-- base
import Control.Arrow
  ( second )
import Control.Monad
  ( void )
import Data.Foldable
  ( for_ )
import Data.String
  ( IsString )
import Numeric
  ( showFFloat )

-- gi-cairo-connector
import qualified GI.Cairo.Render.Connector as Cairo
  ( renderWithContext )

-- gi-cairo-render
import qualified GI.Cairo.Render as Cairo

-- gi-gtk
import qualified GI.Gtk as GTK

-- stm
import qualified Control.Concurrent.STM.TVar as STM
  ( readTVarIO )

-- text
import qualified Data.Text as Text
  ( pack )

-- brush-strokes
import Math.Linear
  ( ℝ(..) )

-- MetaBrush
import MetaBrush.Application.Context
import MetaBrush.Asset.Colours
  ( Colours )
import MetaBrush.Asset.Cursor
  ( drawCursorIcon )
import MetaBrush.Asset.InfoBar
  ( drawMagnifier, drawTopLeftCornerRect )
import MetaBrush.Document
  ( DocumentMetadata(..), Zoom(..) )
import MetaBrush.UI.Coordinates
  ( toViewportCoordinates )
import MetaBrush.GTK.Util
  ( widgetAddClass, widgetAddClasses )

--------------------------------------------------------------------------------

-- | Add the UI elements for the info bar:
--
--   * current zoom level,
--   * current cursor position,
--   * current viewport extent (top left and bottom right corner coordinates).
--
-- Returns the GTK label widgets, so that the information can be updated.
createInfoBar :: Colours -> IO InfoBar
createInfoBar colours = do
  infoBarArea       <- GTK.boxNew GTK.OrientationHorizontal 0
  widgetAddClasses infoBarArea [ "infoBar", "monospace", "contrast" ]
  GTK.widgetSetHalign infoBarArea GTK.AlignEnd

  zoomBox        <- GTK.boxNew GTK.OrientationHorizontal 0
  cursorPosBox   <- GTK.boxNew GTK.OrientationHorizontal 0
  topLeftPosBox  <- GTK.boxNew GTK.OrientationHorizontal 0
  botRightPosBox <- GTK.boxNew GTK.OrientationHorizontal 0

  for_ [ botRightPosBox, topLeftPosBox, cursorPosBox, zoomBox ] \ box -> do
    GTK.boxPrepend infoBarArea box
    widgetAddClass box "infoBarBox"

  -------------
  -- Magnifier

  magnifierArea <- GTK.drawingAreaNew
  zoomText <- GTK.labelNew ( Just na )

  GTK.boxAppend zoomBox magnifierArea
  GTK.boxAppend zoomBox zoomText

  GTK.drawingAreaSetDrawFunc magnifierArea $ Just \ _ cairoContext _ _ ->
    void $ ( `Cairo.renderWithContext` cairoContext ) do
      Cairo.scale 0.9 0.9
      Cairo.translate 14 10
      drawMagnifier colours

  -------------------
  -- Cursor position

  cursorPosArea <- GTK.drawingAreaNew
  cursorPosText <- GTK.labelNew ( Just $ "x: " <> na <> "\ny: " <> na )

  GTK.boxAppend cursorPosBox cursorPosArea
  GTK.boxAppend cursorPosBox cursorPosText

  GTK.drawingAreaSetDrawFunc cursorPosArea $ Just \ _ cairoContext _ _ ->
    void $ ( `Cairo.renderWithContext` cairoContext ) do
      Cairo.scale 0.75 0.75
      Cairo.translate 9.5 7
      drawCursorIcon colours

  ---------------------
  -- Top left position

  topLeftPosArea <- GTK.drawingAreaNew
  topLeftPosText <- GTK.labelNew ( Just $ "x: " <> na <> "\ny: " <> na )

  GTK.boxAppend topLeftPosBox topLeftPosArea
  GTK.boxAppend topLeftPosBox topLeftPosText

  GTK.drawingAreaSetDrawFunc topLeftPosArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawTopLeftCornerRect colours ) cairoContext

  -------------------------
  -- Bottom right position

  botRightPosArea <- GTK.drawingAreaNew
  botRightPosText <- GTK.labelNew ( Just $ "x: " <> na <> "\ny: " <> na )

  GTK.boxAppend botRightPosBox botRightPosArea
  GTK.boxAppend botRightPosBox botRightPosText

  GTK.drawingAreaSetDrawFunc botRightPosArea $ Just \ _ cairoContext _ _ ->
    void $ ( `Cairo.renderWithContext` cairoContext ) do
      Cairo.scale -1 -1
      Cairo.translate -40 -40
      drawTopLeftCornerRect colours

  -------------------------

  for_ [ magnifierArea, cursorPosArea, topLeftPosArea, botRightPosArea ] \ area -> do
    widgetAddClass area "infoBarIcon"
    GTK.widgetSetSizeRequest area 40 40 -- not sure why this is needed...?

  for_ [ zoomText, cursorPosText, topLeftPosText, botRightPosText ] \ info -> do
    widgetAddClass info "infoBarInfo"

  pure ( InfoBar {..} )

updateInfoBar :: GTK.DrawingArea -> InfoBar -> Variables -> Maybe DocumentMetadata -> IO ()
updateInfoBar viewportDrawingArea ( InfoBar {..} ) ( Variables { mousePosTVar } ) mbDoc
  = do
    viewportWidth  <- fromIntegral @_ @Double <$> GTK.widgetGetWidth  viewportDrawingArea
    viewportHeight <- fromIntegral @_ @Double <$> GTK.widgetGetHeight viewportDrawingArea
    case mbDoc of
      Nothing -> do
        GTK.labelSetText        zoomText $ na
        GTK.labelSetText   cursorPosText $ Text.pack ( "x: " <> na <> "\ny: " <> na )
        GTK.labelSetText  topLeftPosText $ Text.pack ( "x: " <> na <> "\ny: " <> na )
        GTK.labelSetText botRightPosText $ Text.pack ( "x: " <> na <> "\ny: " <> na )
      Just ( Metadata { documentZoom = zoom@( Zoom { zoomFactor } ), viewportCenter } ) -> do
        let
          toViewport :: ℝ 2 -> ℝ 2
          toViewport = toViewportCoordinates zoom ( viewportWidth, viewportHeight ) viewportCenter
          ℝ2 l t = toViewport ( ℝ2 0 0 )
          ℝ2 r b = toViewport ( ℝ2 viewportWidth viewportHeight )
        mbMousePos <- STM.readTVarIO mousePosTVar
        GTK.labelSetText zoomText $ Text.pack ( fixed 5 2 ( 100 * zoomFactor ) <> "%" )
        case mbMousePos of
          Just ( ℝ2 mx my ) ->
            GTK.labelSetText cursorPosText $ Text.pack ( "x: " <> fixed 6 4 mx <> "\ny: " <> fixed 6 4 my )
          Nothing ->
            GTK.labelSetText cursorPosText $ Text.pack ( "x: " <> na <> "\ny: " <> na )
        GTK.labelSetText  topLeftPosText $ Text.pack ( "x: " <> fixed 6 2  l <> "\ny: " <> fixed 6 2  t )
        GTK.labelSetText botRightPosText $ Text.pack ( "x: " <> fixed 6 2  r <> "\ny: " <> fixed 6 2  b )

fixed :: Int -> Int -> Double -> String
fixed digitsBefore digitsAfter x =
  case second (drop 1) . break ( == '.' ) $ showFFloat ( Just digitsAfter ) x "" of
    ( as, bs ) ->
      let
        l, r :: Int
        l = length as
        r = length bs
      in
        replicate ( digitsBefore - l ) ' ' <> as <> "." <> bs <> replicate ( digitsAfter - r ) '0'

na :: IsString a => a
na = "      n/a"
