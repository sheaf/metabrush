{-# LANGUAGE OverloadedStrings #-}

module MetaBrush.UI.Panels
  ( PanelsBar(..)
  , createPanelBar
  )
  where

-- base
import Control.Monad
  ( void )
import Data.Foldable
  ( for_ )

-- gi-gtk
import qualified GI.Gtk as GTK

-- MetaBrush
import MetaBrush.GTK.Util
  ( widgetAddClass, widgetAddClasses )
--import MetaBrush.UI.StrokeTreeView
--  ( newStrokesView )

--------------------------------------------------------------------------------

data PanelsBar
  = PanelsBar
  { layersScrolledWindow, brushesScrolledWindow
      :: !GTK.ScrolledWindow
  , transformPanelBox, historyPanelBox
      :: !GTK.Box
  }

-- | Creates the right hand side panel UI.
createPanelBar :: GTK.Box -> IO PanelsBar
createPanelBar panelBox = do

  widgetAddClass panelBox "panels"

  pane1 <- GTK.panedNew GTK.OrientationVertical
  GTK.panedSetWideHandle pane1 True
  GTK.widgetSetVexpand pane1 True
  GTK.boxAppend panelBox pane1

  panels1 <- GTK.notebookNew
  panels2 <- GTK.notebookNew

  GTK.notebookSetGroupName panels1 ( Just "Top panel" )
  GTK.notebookSetGroupName panels2 ( Just "Bottom panel" )

  GTK.panedSetStartChild pane1 ( Just panels1 )
  GTK.panedSetEndChild   pane1 ( Just panels2 )

  layersScrolledWindow <- GTK.scrolledWindowNew
  GTK.scrolledWindowSetPolicy layersScrolledWindow GTK.PolicyTypeNever GTK.PolicyTypeAutomatic
  brushesScrolledWindow <- GTK.scrolledWindowNew
  GTK.scrolledWindowSetPolicy brushesScrolledWindow GTK.PolicyTypeNever GTK.PolicyTypeAutomatic

  transformPanelBox <- GTK.boxNew GTK.OrientationVertical 0
  historyPanelBox   <- GTK.boxNew GTK.OrientationVertical 0

  strokesTab   <- GTK.labelNew ( Just "Strokes"   )
  brushesTab   <- GTK.labelNew ( Just "Brushes"   )
  transformTab <- GTK.labelNew ( Just "Transform" )
  historyTab   <- GTK.labelNew ( Just "History"   )

  for_ [ strokesTab, brushesTab, transformTab, historyTab ] \ tab -> do
    widgetAddClasses tab [ "plain", "text", "panelTab" ]

  for_ [ layersScrolledWindow, brushesScrolledWindow ] \ w -> widgetAddClasses w [ "panel", "listViewPane" ]
  for_ [ transformPanelBox, historyPanelBox ] \ panel -> do
    widgetAddClass panel "panel"

  void $ GTK.notebookAppendPage panels1 brushesScrolledWindow ( Just brushesTab )
  void $ GTK.notebookAppendPage panels1 historyPanelBox       ( Just historyTab )

  void $ GTK.notebookAppendPage panels2 layersScrolledWindow  ( Just strokesTab )
  void $ GTK.notebookAppendPage panels2 transformPanelBox     ( Just transformTab )

  GTK.notebookSetTabReorderable panels1 brushesScrolledWindow True
  GTK.notebookSetTabDetachable  panels1 brushesScrolledWindow True
  GTK.notebookSetTabReorderable panels1 historyPanelBox       True
  GTK.notebookSetTabDetachable  panels1 historyPanelBox       True

  GTK.notebookSetTabReorderable panels2 layersScrolledWindow True
  GTK.notebookSetTabDetachable  panels2 layersScrolledWindow True
  GTK.notebookSetTabReorderable panels2 transformPanelBox    True
  GTK.notebookSetTabDetachable  panels2 transformPanelBox    True

  transformContent <- GTK.labelNew ( Just "Transform tab content..." )
  historyContent   <- GTK.labelNew ( Just "History tab content..." )

  GTK.boxAppend transformPanelBox transformContent
  GTK.boxAppend   historyPanelBox   historyContent

  return $
    PanelsBar { layersScrolledWindow, brushesScrolledWindow
              , transformPanelBox, historyPanelBox
              }
