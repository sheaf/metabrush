{-# LANGUAGE OverloadedStrings #-}

module MetaBrush.UI.Menu where

-- base
import Control.Monad
  ( void )
import Data.Foldable
  ( for_, traverse_ )
import Data.IORef
  ( IORef, newIORef, atomicModifyIORef' )
import qualified Data.List.NonEmpty as NE
import System.IO.Unsafe
  ( unsafePerformIO )

-- haskell-gi-base
import qualified Data.GI.Base.Signals as GI

-- gi-cairo-connector
import qualified GI.Cairo.Render.Connector as Cairo
  ( renderWithContext )

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gobject
import qualified GI.GObject as GObject

-- gi-gtk
import qualified GI.Gtk as GTK

-- text
import Data.Text
  ( Text )
import qualified Data.Text as Text
  ( unpack )

-- transformers
import Control.Monad.IO.Class
  ( MonadIO(..) )

-- unordered-containers
import Data.HashMap.Strict
  ( HashMap )
import qualified Data.HashMap.Strict as HashMap
  ( insert, lookup, traverseWithKey, empty )
import Data.HashSet
  ( HashSet )
import qualified Data.HashSet as HashSet
  ( fromList, toMap )

-- MetaBrush
import {-# SOURCE #-} MetaBrush.Application.Action
import MetaBrush.Application.Context
import MetaBrush.Asset.Colours
  ( Colours )
import MetaBrush.Asset.WindowIcons
  ( drawMinimise, drawRestoreDown, drawMaximise, drawClose )
import MetaBrush.GTK.Util
  ( widgetAddClass, widgetAddClasses )
import MetaBrush.Layer
  ( Layer(..), RelativePosition(..), WithinParent(.. ))

--------------------------------------------------------------------------------
-- Types for describing menu items.

data MenuItem where
  SubmenuDescription ::
    { submenuLabel   :: !( Maybe Text )
    , submenuItems   :: ![ MenuItem ]
    } -> MenuItem
  MenuItemDescription :: HandleAction action =>
    { menuItemLabel  :: !Text
    , menuItemAction :: !( Maybe ActionName, action )
    , menuItemAccel  :: !( Maybe Text )
    } -> MenuItem
  Section ::
    { sectionName  :: !( Maybe Text )
    , sectionItems :: [ MenuItem ]
    } -> MenuItem

menuActionNames :: HashSet ActionName
menuActionNames = HashSet.fromList
  -- file menu
  [ WinAction "newFile"
  , WinAction "openFile"
  , WinAction "openFolder"
  , WinAction "save"
  , WinAction "saveAs"
  , WinAction "export"
  , WinAction "closeActive"
  , WinAction "closeAll"
  , WinAction "quit"
  -- edit menu
  , WinAction "undo"
  , WinAction "redo"
  , WinAction "cut"
  , WinAction "copy"
  , WinAction "paste"
  , WinAction "duplicate"
  , WinAction "delete"
  -- view menu
  , WinAction "toggleGuides"
  -- about menu
  , WinAction "about"
  -- settings
  , WinAction "canvas"
  , WinAction "prefs"
  ]

strokeMenuActionNames :: HashSet ActionName
strokeMenuActionNames = HashSet.fromList
  [ WinAction "deleteLayer"
  , WinAction "newGroupAbove"
  , WinAction "newGroupBelow"
  ]

createMenuActions :: IO ( HashMap ActionName GIO.SimpleAction )
createMenuActions =
  HashMap.traverseWithKey
    ( \ actionName _ -> GIO.simpleActionNew ( actionSimpleName actionName ) Nothing )
    ( HashSet.toMap menuActionNames <> HashSet.toMap strokeMenuActionNames )

--------------------------------------------------------------------------------
-- Menu used in MetaBrush.

menuDescription :: [ MenuItem ]
menuDescription =
  [ SubmenuDescription ( Just "File" ) fileMenuDescription
  , SubmenuDescription ( Just "Edit" ) editMenuDescription
  , SubmenuDescription ( Just "View" ) viewMenuDescription
  , SubmenuDescription ( Just "Help" ) helpMenuDescription
  ]

fileMenuDescription :: [ MenuItem ]
fileMenuDescription =
  [ MenuItemDescription "New"         ( Just $ WinAction "newFile"    , ( NewFile    AfterCurrentTab ) ) ( Just "<Control>n"        )
  , MenuItemDescription "Open file"   ( Just $ WinAction "openFile"   , ( OpenFile   AfterCurrentTab ) ) ( Just "<Control>o"        )
  , MenuItemDescription "Open folder" ( Just $ WinAction "openFolder" , ( OpenFolder AfterCurrentTab ) ) ( Just "<Control><Shift>o" )
  , MenuItemDescription "Save"        ( Just $ WinAction "save"       , Save                           ) ( Just "<Control>s"        )
  , MenuItemDescription "Save as"     ( Just $ WinAction "saveAs"     , SaveAs                         ) ( Just "<Control><Shift>s" )
  , MenuItemDescription "Export"      ( Just $ WinAction "export"     , Export                         ) ( Just "<Control>e"        )
  , MenuItemDescription "Close"       ( Just $ WinAction "closeActive", CloseActive                    ) ( Just "<Control>w"        )
  , MenuItemDescription "Close all"   ( Just $ WinAction "closeAll"   , CloseAll                       ) ( Just "<Control><Shift>w" )
  , MenuItemDescription "Quit"        ( Just $ WinAction "quit"       , Quit                           ) ( Just "<Control>q"        )
  ]

editMenuDescription :: [ MenuItem ]
editMenuDescription =
  [ Section Nothing
    [ MenuItemDescription "Undo"        ( Just $ WinAction "undo", Undo )      ( Just "<Control>z" )
    , MenuItemDescription "Redo"        ( Just $ WinAction "redo", Redo )      ( Just "<Control>y" )
    ]
  , Section Nothing
    [ MenuItemDescription "Cut"         ( Just $ WinAction "cut"      , Cut       ) ( Just "<Control>x" )
    , MenuItemDescription "Copy"        ( Just $ WinAction "copy"     , Copy      ) ( Just "<Control>c" )
    , MenuItemDescription "Paste"       ( Just $ WinAction "paste"    , Paste     ) ( Just "<Control>v" )
    , MenuItemDescription "Duplicate"   ( Just $ WinAction "duplicate", Duplicate ) ( Just "<Control>d" )
    , MenuItemDescription "Delete"      ( Just $ WinAction "delete"   , Delete    ) ( Just "Delete"  )
    ]
  , Section Nothing
    [ MenuItemDescription "Canvas"      ( Just $ WinAction "canvas", Canvas    ) ( Just "<Alt><Control>c" )
    , MenuItemDescription "Preferences" ( Just $ WinAction "prefs" , OpenPrefs ) ( Just "<Control><Shift>p" )
    ]
  ]

viewMenuDescription :: [ MenuItem ]
viewMenuDescription =
  [ Section Nothing
    [ MenuItemDescription "Navigator"      ( Nothing, () ) Nothing
    , MenuItemDescription "History"        ( Nothing, () ) ( Just "<Control>h" )
    ]
  , Section Nothing
    [ MenuItemDescription "Strokes"        ( Nothing, () ) Nothing
    , MenuItemDescription "Brushes"        ( Nothing, () ) Nothing
    , MenuItemDescription "Metaparameters" ( Nothing, () ) Nothing
    ]
  , Section Nothing
    [ MenuItemDescription "Transform"      ( Nothing, () ) Nothing
    ]
  , Section Nothing
    [ MenuItemDescription "Toggle guides"  ( Just $ WinAction "toggleGuides", ToggleGuides ) ( Just "g" )
    ]
  ]

helpMenuDescription :: [ MenuItem ]
helpMenuDescription =
  [ MenuItemDescription "About MetaBrush" ( Just $ WinAction "about", About ) ( Just "<Ctrl>question" )
  ]

strokeMenuDescription :: WithinParent Layer -> [ MenuItem ]
strokeMenuDescription item@( WithinParent _ lay ) =
  [ case lay of
      StrokeLayer {} -> MenuItemDescription "Delete stroke" ( Just $ WinAction "deleteLayer", DeleteLayer child ) Nothing
      GroupLayer  {} -> MenuItemDescription "Delete group"  ( Just $ WinAction "deleteLayer", DeleteLayer child ) Nothing
  , Section ( Just "New group" ) $
    [ MenuItemDescription "...above" ( Just $ WinAction "newGroupAbove", NewGroup Above child ) Nothing
    , MenuItemDescription "...below" ( Just $ WinAction "newGroupBelow", NewGroup Below child ) Nothing
    ]
  ]
  where
    child = fmap layerUnique item

--------------------------------------------------------------------------------
-- Creating a GTK popover menu bar from a menu description.

{-# NOINLINE previousActions #-}
previousActions :: IORef ( HashMap Text ( NE.NonEmpty GI.SignalHandlerId ) )
previousActions = unsafePerformIO $ newIORef HashMap.empty

makeMenu :: MonadIO m => UIElements -> Variables -> GIO.Menu -> [ MenuItem ] -> m ()
makeMenu uiElts@( UIElements { application, window, menuActions } ) vars menu = traverse_ \case
  SubmenuDescription
    { submenuLabel
    , submenuItems
    } -> do
      submenu <- GIO.menuNew
      makeMenu uiElts vars submenu submenuItems
      GIO.menuAppendSubmenu menu submenuLabel submenu
  MenuItemDescription
    { menuItemLabel
    , menuItemAction = ( mbActionName, actionData )
    , menuItemAccel
    } -> do
      for_ mbActionName $ \ actionName -> do
          let
            simpleName :: Text
            simpleName = actionSimpleName actionName
          case HashMap.lookup actionName menuActions of
            Nothing ->
              error
                ( "Could not create menu item labelled " <> Text.unpack menuItemLabel <>
                  ": missing action " <> show actionName
                )
            Just menuItemAction -> do
              -- Un-register any previously registered actions.
              -- (For stroke menu actions in which we create a new set of actions
              -- each time the popover menu pop ups.)
              signalId
                <- GIO.onSimpleActionActivate menuItemAction
                  ( \ _ -> handleAction uiElts vars actionData )
              prevActions <- liftIO $ atomicModifyIORef' previousActions $ \ oldActionsMap ->
                let old = HashMap.lookup simpleName oldActionsMap
                in ( HashMap.insert simpleName ( NE.singleton signalId ) oldActionsMap, maybe [] NE.toList old )
              for_ prevActions $ GObject.signalHandlerDisconnect menuItemAction
              GIO.actionMapRemoveAction window simpleName
              GIO.actionMapAddAction window menuItemAction
          for_ menuItemAccel \ accelText -> do
            actionDetailedName <- GIO.actionPrintDetailedName simpleName Nothing
            GTK.applicationSetAccelsForAction application ( actionPrefix actionName <> actionDetailedName ) [accelText]
      menuItem <- GIO.menuItemNew ( Just menuItemLabel ) ( fmap ( \ name -> actionPrefix name <> actionSimpleName name ) mbActionName )
      GIO.menuAppendItem menu menuItem
  Section
    { sectionName
    , sectionItems
    } -> do
      section <- GIO.menuNew
      makeMenu uiElts vars section sectionItems
      GIO.menuAppendSection menu sectionName section

--------------------------------------------------------------------------------
-- Creating the menu bar from its declarative specification.

createMenu :: MonadIO m => UIElements -> Variables -> m GIO.Menu
createMenu uiElts vars = do
  menu <- GIO.menuNew
  makeMenu uiElts vars menu menuDescription
  pure menu

-- | Add the menu bar to the given box (title bar box).
createMenuBar :: MonadIO m => UIElements -> Variables -> Colours -> m GTK.PopoverMenuBar
createMenuBar uiElts@( UIElements { application, window, titleBar } ) vars colours = do
  --accelGroup <- GTK.accelGroupNew
  --GTK.windowAddAccelGroup window accelGroup
  menu <- createMenu uiElts vars
  menuBar <- GTK.popoverMenuBarNewFromModel ( Just menu )
  widgetAddClasses menuBar [ "headerMenu", "text", "plain" ]
  GTK.headerBarPackStart titleBar menuBar

  windowIcons <- GTK.boxNew GTK.OrientationHorizontal 0
  widgetAddClasses windowIcons [ "windowIcons" ]
  GTK.headerBarPackEnd titleBar windowIcons

  minimiseButton <- GTK.buttonNew
  maximiseButton <- GTK.buttonNew
  closeButton    <- GTK.buttonNew

  GTK.boxAppend windowIcons minimiseButton
  GTK.boxAppend windowIcons maximiseButton
  GTK.boxAppend windowIcons    closeButton

  minimiseArea <- GTK.drawingAreaNew
  maximiseArea <- GTK.drawingAreaNew
  closeArea    <- GTK.drawingAreaNew

  GTK.buttonSetChild minimiseButton ( Just minimiseArea )
  GTK.buttonSetChild maximiseButton ( Just maximiseArea )
  GTK.buttonSetChild    closeButton ( Just    closeArea )

  GTK.drawingAreaSetDrawFunc minimiseArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawMinimise colours ) cairoContext

  GTK.drawingAreaSetDrawFunc maximiseArea $ Just \ _ cairoContext _ _ -> do
    isMaximised <- GTK.getWindowMaximized window
    if isMaximised
    then void $ Cairo.renderWithContext ( drawRestoreDown colours ) cairoContext
    else void $ Cairo.renderWithContext ( drawMaximise    colours ) cairoContext

  GTK.drawingAreaSetDrawFunc closeArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawClose colours ) cairoContext

  for_ [ minimiseButton, maximiseButton, closeButton ] \ button -> do
    widgetAddClass button "windowIcon"

  widgetAddClass closeButton "closeWindowIcon"

  ---------------------------------------------------------
  -- Actions

  _ <- GTK.onButtonClicked closeButton    ( quitEverything application window )
  _ <- GTK.onButtonClicked minimiseButton ( GTK.windowMinimize window )
  _ <- GTK.onButtonClicked maximiseButton do
    isMaximised <- GTK.getWindowMaximized window
    if isMaximised
    then GTK.windowUnmaximize window
    else GTK.windowMaximize   window
    GTK.widgetQueueDraw maximiseArea

  ---------------------------------------------------------

  GTK.applicationSetMenubar application ( Just menu )

  pure menuBar
