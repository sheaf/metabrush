module MetaBrush.UI.StrokeTreeView where

-- base
import GHC.Stack

-- containers
import Data.Map.Strict
  ( Map )

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gtk
import qualified GI.Gtk as GTK

-- stm
import qualified Control.Concurrent.STM as STM

-- MetaBrush
import MetaBrush.Application.Context
import MetaBrush.Document.Diff
import MetaBrush.Document.History
import MetaBrush.Layer
import MetaBrush.Unique

--------------------------------------------------------------------------------

newLayersListModel :: Variables -> Unique -> IO GTK.MultiSelection

switchStrokeView :: GTK.ListView -> Variables -> Maybe Unique -> IO ()

newLayerView :: UIElements -> Variables -> IO GTK.ListView

applyDiffToListModel :: HasCallStack
                     => STM.TVar ( Map Unique ( Map ( Parent Unique ) GIO.ListStore ) )
                     -> Unique
                     -> ( Do, HierarchyDiff )
                     -> IO ()
