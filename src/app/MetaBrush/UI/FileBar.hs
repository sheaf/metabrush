{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module MetaBrush.UI.FileBar
  ( FileBar(..), FileBarTab(..)
  , createFileBar, newFileTab, removeFileTab
  , TabLocation(..)
  )
  where

-- base
import Control.Monad
  ( join, void, when )
import Data.Foldable
  ( traverse_ )
import Data.Traversable
  ( for )

-- containers
import qualified Data.Map.Strict as Map

-- generic-lens
import Data.Generics.Product.Fields
  ( field' )

-- gi-cairo-connector
import qualified GI.Cairo.Render.Connector as Cairo
  ( renderWithContext )

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gtk
import qualified GI.Gtk as GTK

-- haskell-gi-base
import qualified Data.GI.Base as GI

-- lens
import Control.Lens
  ( set )
import Control.Lens.At
  ( ix )

-- stm
import qualified Control.Concurrent.STM      as STM
  ( atomically )
import qualified Control.Concurrent.STM.TVar as STM
  ( readTVar, readTVarIO, modifyTVar' )

-- transformers
import Control.Monad.Trans.Reader
  ( runReaderT )

-- unordered-containers
import Data.HashMap.Lazy
  ( HashMap )

-- MetaBrush
import {-# SOURCE #-} MetaBrush.Application.Action
  ( SwitchTo(..), Close(..), handleAction )
import MetaBrush.Application.Context
import MetaBrush.Application.UpdateDocument
  ( ActiveDocChange (..), updateUIAction )
import MetaBrush.Asset.CloseTabButton
  ( drawCloseTabButton )
import MetaBrush.Asset.Colours
  ( Colours )
import MetaBrush.Document
import MetaBrush.Document.History
  ( DocumentHistory(..), newHistory )
import MetaBrush.UI.Panels
  ( PanelsBar )
import MetaBrush.UI.Viewport
  ( Viewport(..) )
import MetaBrush.Unique
  ( Unique, freshUnique, uniqueText )
import MetaBrush.GTK.Util
  ( editableLabelNew, widgetAddClass, widgetAddClasses )

--------------------------------------------------------------------------------

newFileTab
  :: UIElements
  -> Variables
  -> Maybe ( Unique, DocumentHistory )
  -> TabLocation
  -> IO ()
newFileTab
  uiElts@( UIElements { fileBar = FileBar {..}, .. } )
  vars@( Variables {..} )
  mbDocHist
  newTabLoc
  = do

    ( thisTabDocUnique, thisTabDocHist ) <-
      case mbDocHist of
        -- Use the provided document (e.g. document read from a file).
        Just docHist -> pure docHist
        -- Create a new empty document.
        Nothing -> do
          newDocUniq <- STM.atomically $ runReaderT freshUnique uniqueSupply
          pure ( newDocUniq, newHistory $ emptyDocument ( "Untitled " <> uniqueText newDocUniq ) )

    pgLabel <- editableLabelNew ( documentName $ documentMetadata $ present thisTabDocHist )

    closeFileArea <- GTK.drawingAreaNew

    GTK.drawingAreaSetDrawFunc closeFileArea $ Just \ _ cairoContext _ _ -> void do
      mbTabDoc <- fmap present . Map.lookup thisTabDocUnique <$> STM.readTVarIO openDocumentsTVar
      let
        unsaved :: Bool
        unsaved = maybe False ( unsavedChanges . documentContent ) mbTabDoc
      flags <- GTK.widgetGetStateFlags closeFileArea
      Cairo.renderWithContext ( drawCloseTabButton colours unsaved flags ) cairoContext

    -- Create box for file tab elements.
    tab <- GTK.boxNew GTK.OrientationHorizontal 0
    widgetAddClasses tab [ "fileBarTab" ]
    GTK.boxAppend tab pgLabel
    GTK.boxAppend tab closeFileArea
    widgetAddClasses closeFileArea [ "fileBarCloseButton" ]

    -- Place the new tab in the correct position within the file bar.
    case newTabLoc of
      LastTab ->
        GTK.boxAppend fileTabsBox tab
      AfterCurrentTab -> do
        mbActiveTab <- fmap join $ STM.atomically do
          mbUnique <- STM.readTVar activeDocumentTVar
          for mbUnique \ docUnique -> do
            Map.lookup docUnique <$> STM.readTVar fileBarTabsTVar
        case mbActiveTab of
          Just ( FileBarTab { fileBarTab = activeTab } )
            -> GTK.boxInsertChildAfter fileTabsBox tab ( Just activeTab )
          _ -> GTK.boxAppend fileTabsBox tab

    let
      fileBarTab :: FileBarTab
      fileBarTab =
        FileBarTab
          { fileBarTab          = tab
          , fileBarTabLabel     = pgLabel
          , fileBarTabCloseArea = closeFileArea
          }
    -- Add this document to the set of existing documents.
    STM.atomically do
      STM.modifyTVar' openDocumentsTVar ( Map.insert thisTabDocUnique thisTabDocHist )
      STM.modifyTVar' fileBarTabsTVar   ( Map.insert thisTabDocUnique fileBarTab )

    ----------------------------------------
    -- Page label editing

    -- Connect a signal for editing the document name.
    void $ GI.on pgLabel ( GI.PropertyNotify #editing ) $ \ _ -> do
      editing <- GTK.editableLabelGetEditing pgLabel

      -- Only perform an update once we have stopped editing.
      when ( not editing ) do
        newDocName <- GTK.editableGetText pgLabel
        GTK.editableSetEditable pgLabel False
        -- Re-enable keyboard shortcuts that were disabled.
        traverse_ ( `GIO.setSimpleActionEnabled` True ) menuActions
          -- NB: 'updateUIAction' below will properly re-disable 'undo'
          -- and 'redo' if there is nothing to undo or redo.
        uiUpdate <-
          STM.atomically $ do
            STM.modifyTVar' openDocumentsTVar
              ( set ( ix thisTabDocUnique
                    . field' @"present"
                    . field' @"documentMetadata"
                    . field' @"documentName"
                    )
              newDocName
              )
            updateUIAction NoActiveDocChange uiElts vars
        uiUpdate

    -- Make it so that we require a double click to start editing
    -- a file bar tab label.
    pgClicks <- GTK.gestureClickNew
    void $ GTK.onGestureClickReleased pgClicks $ \ nbClicks _x _y -> do
      button <- GTK.gestureSingleGetCurrentButton ?self
      case button of
        1 -> do
          handleAction uiElts vars ( SwitchTo thisTabDocUnique )
          void $ GTK.widgetGrabFocus pgLabel
          when ( nbClicks > 1 ) do
            -- Disable all accelerators when doing text entry.
            traverse_ ( `GIO.setSimpleActionEnabled` False ) menuActions
            GTK.editableSetEditable pgLabel True
            GTK.editableLabelStartEditing pgLabel
        _ -> return ()

    --
    ----------------------------------------

    closeButtonClick <- GTK.gestureClickNew
    void $ GTK.onGestureClickPressed closeButtonClick $ \ _ _ _ -> do
      void $ GTK.gestureSetState ?self GTK.EventSequenceStateClaimed
    void $ GTK.onGestureClickReleased closeButtonClick $ \ _ _ _ -> do
      button <- GTK.gestureSingleGetCurrentButton ?self
      case button of
        1 -> do
          GTK.widgetQueueDraw closeFileArea
          handleAction uiElts vars ( CloseThis thisTabDocUnique )
        _ -> return ()
      void $ GTK.gestureSetState ?self GTK.EventSequenceStateClaimed
    GTK.widgetAddController closeFileArea closeButtonClick
    GTK.widgetAddController tab pgClicks

    -- Switch to this tab.
    handleAction uiElts vars ( SwitchTo thisTabDocUnique )

-- | Create a file bar: tabs allowing selection of the active document.
--
-- Updates the active document when buttons are clicked.
createFileBar
  :: Colours -> Variables
  -> GTK.Application -> GTK.ApplicationWindow -> GTK.EventControllerKey
  -> GTK.HeaderBar -> GTK.Label -> Viewport -> InfoBar
  -> GTK.PopoverMenuBar -> HashMap ActionName GIO.SimpleAction
  -> PanelsBar
  -> GTK.ListView
  -> IO FileBar
createFileBar
  colours
  vars@( Variables { openDocumentsTVar } )
  application window windowKeys titleBar titleLabel viewport infoBar menuBar menuActions
  panelsBar strokesListView
  = do

    -- Create file bar: box containing scrollable tabs, and a "+" button after it.
    fileBarBox <- GTK.boxNew GTK.OrientationHorizontal 0
    widgetAddClass fileBarBox "fileBar"

    fileTabsScroll <- GTK.scrolledWindowNew
    GTK.scrolledWindowSetPolicy           fileTabsScroll GTK.PolicyTypeExternal GTK.PolicyTypeNever
      -- For some reason, "External" policy means to enable scrolling without showing the scrollbar
    GTK.scrolledWindowSetOverlayScrolling fileTabsScroll True
    GTK.widgetSetHexpand fileTabsScroll True

    newFileButton <- GTK.buttonNewWithLabel "+"
    widgetAddClasses newFileButton [ "newFileButton" ]

    GTK.boxAppend fileBarBox fileTabsScroll
    GTK.boxAppend fileBarBox newFileButton
    GTK.widgetSetHalign newFileButton GTK.AlignEnd

    fileTabsBox <- GTK.boxNew GTK.OrientationHorizontal 0
    GTK.scrolledWindowSetChild fileTabsScroll ( Just fileTabsBox )
    widgetAddClasses fileTabsBox [ "fileBar", "plain", "text" ]

    let
      fileBar :: FileBar
      fileBar = FileBar {..}
      uiElements :: UIElements
      uiElements = UIElements {..}

    documents <- STM.readTVarIO openDocumentsTVar
    ( `Map.foldMapWithKey` documents ) \ docUnique doc ->
      newFileTab
        uiElements vars
        ( Just ( docUnique, doc ) )
        LastTab

    void $ GTK.onButtonClicked newFileButton do
      newFileTab
        uiElements vars
        Nothing
        LastTab

    pure fileBar

-- | Close a document: remove the corresponding file tab from the file bar.
--
-- Returns previous/next tabs, if any.
removeFileTab :: UIElements -> Variables -> Unique -> IO ( Maybe GTK.Box, Maybe GTK.Box )
removeFileTab
  ( UIElements { fileBar = FileBar { fileTabsBox } } )
  ( Variables {..} )
  docUnique = do

    mbCleanupAction <- STM.atomically do
      -- Remove the tab.
      mbTab <- Map.lookup docUnique <$> STM.readTVar fileBarTabsTVar
      for mbTab \ ( FileBarTab { fileBarTab = tab } ) -> do
        STM.modifyTVar' openDocumentsTVar ( Map.delete docUnique )
        STM.modifyTVar' fileBarTabsTVar   ( Map.delete docUnique )

        pure do
          mbPrev <- traverse ( GTK.unsafeCastTo GTK.Box ) =<< GTK.widgetGetPrevSibling tab
          mbNext <- traverse ( GTK.unsafeCastTo GTK.Box ) =<< GTK.widgetGetNextSibling tab
          GTK.boxRemove fileTabsBox tab
          return ( mbPrev, mbNext )

    case mbCleanupAction of
      Nothing -> return ( Nothing, Nothing )
      Just doIt -> doIt
