module MetaBrush.UI.FileBar
  ( FileBar(..), FileBarTab(..), TabLocation(..)
  , newFileTab, removeFileTab
  )
  where

-- gi-gtk
import qualified GI.Gtk as GTK

-- MetaBrush
import MetaBrush.Application.Context
import MetaBrush.Document.History
  ( DocumentHistory )
import MetaBrush.Unique
  ( Unique )

--------------------------------------------------------------------------------

newFileTab :: UIElements -> Variables -> Maybe ( Unique, DocumentHistory ) -> TabLocation -> IO ()
removeFileTab :: UIElements -> Variables -> Unique -> IO ( Maybe GTK.Box, Maybe GTK.Box )
