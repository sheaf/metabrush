module MetaBrush.UI.Coordinates
  ( toViewportCoordinates, closestPoint )
  where

-- base
import Data.Coerce
  ( coerce )
import Data.Functor.Identity
  ( Identity(..) )
import Data.Semigroup
  ( ArgMin, Arg(..), Min(..) )

-- acts
import Data.Act
  ( Act
    ( (•) )
  , Torsor
    ( (-->) )
  )

-- brush-strokes
import qualified Math.Bezier.Cubic     as Cubic
  ( Bezier(..), closestPoint )
import qualified Math.Bezier.Quadratic as Quadratic
  ( Bezier(..), closestPoint )
import Math.Bezier.Spline
  ( Curve(..), Spline(..), SplineTypeI, KnownSplineType(bifoldSpline)
  , fromNextPoint
  )
import Math.Module
  ( (*^), squaredNorm, closestPointOnSegment )
import Math.Linear
  ( ℝ(..), T(..), Segment(..) )

-- MetaBrush
import MetaBrush.Document
  ( Zoom(..) )
import MetaBrush.Stroke
  ( Stroke(..), PointData, coords )

--------------------------------------------------------------------------------

-- | Convert a position relative to the drawing area into viewport coordinates.
toViewportCoordinates :: Zoom -> ( Double, Double ) -> ℝ 2 -> ℝ 2 -> ℝ 2
toViewportCoordinates ( Zoom { zoomFactor } ) ( viewportWidth, viewportHeight ) viewportCenter ( ℝ2 x y )
  = ( recip zoomFactor *^ ( ℝ2 ( 0.5 * viewportWidth ) ( 0.5 * viewportHeight ) --> ℝ2 x y :: T ( ℝ 2 ) ) )
  • viewportCenter

-- | Find the closest point on a stroke.
closestPoint :: ℝ 2 -> Stroke -> ArgMin Double ( Maybe ( ℝ 2 ) )
closestPoint c ( Stroke { strokeSpline } ) =
  coerce $
    bifoldSpline @_ @Identity
      ( closestPointToCurve ( splineStart strokeSpline ) )
      ( res . coords )
      strokeSpline
  where
    res :: ℝ 2 -> Identity ( ArgMin BoundedDouble ( Maybe ( ℝ 2 ) ) )
    res p = coerce $ Arg ( squaredNorm ( c --> p :: T ( ℝ 2 ) ) ) ( Just p )

    closestPointToCurve
      :: forall clo crvData brushParams
      .  SplineTypeI clo
      => PointData brushParams
      -> PointData brushParams
      -> Curve clo crvData ( PointData brushParams )
      -> Identity ( ArgMin BoundedDouble ( Maybe ( ℝ 2 ) ) )
    closestPointToCurve start p0 ( LineTo p1 _ ) =
      res ( snd $ closestPointOnSegment @( T ( ℝ 2 ) ) c ( Segment (coords p0 ) ( coords $ fromNextPoint start p1 ) ) )
    closestPointToCurve start p0 ( Bezier2To p1 p2 _ ) = coerce $
      fmap ( fmap ( Just . snd ) )
        ( Quadratic.closestPoint @( T ( ℝ 2 ) ) ( Quadratic.Bezier ( coords p0 ) ( coords p1 ) ( coords $ fromNextPoint start p2 ) ) c )
    closestPointToCurve start p0 ( Bezier3To p1 p2 p3 _ ) = coerce $
      fmap ( fmap ( Just . snd ) )
        ( Cubic.closestPoint @( T ( ℝ 2 ) ) ( Cubic.Bezier ( coords p0 ) ( coords p1 ) ( coords p2 ) ( coords $ fromNextPoint start p3 ) ) c )

-- Messing around to emulate a `Monoid` instance for `ArgMin Double ( Maybe ( ℝ 2 ) )`
newtype BoundedDouble = BoundedDouble Double
  deriving stock   Show
  deriving newtype ( Eq, Ord )
instance Bounded BoundedDouble where
  minBound = BoundedDouble ( -1 / 0 )
  maxBound = BoundedDouble (  1 / 0 )
instance Bounded ( Arg BoundedDouble ( Maybe b ) ) where
  minBound = Arg minBound Nothing
  maxBound = Arg maxBound Nothing
