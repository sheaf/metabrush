{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MetaBrush.UI.StrokeTreeView
  ( newLayersListModel
  , newLayerView
  , switchStrokeView
  , updateLayerHierarchy
  , applyDeletionToStrokeHierarchy
  , applyChangeToLayerHierarchy
  , applyDiffToListModel
  , getSelectedItem
  , DragSourceData(..)
  , DoLayerChange(..), LayerChange(..)
  )
  where

-- base
import Control.Arrow
  ( second )
import Control.Monad
  ( void, when )
import Data.Foldable
  ( for_, traverse_ )
import Data.List
  ( elemIndex )
import qualified Data.List.NonEmpty as NE
import Data.Maybe
  ( fromJust, isJust, isNothing )
import Data.Traversable
  ( for )
import Data.Word
  ( Word32 )
import GHC.Stack

-- containers
import Data.Map.Strict
  ( Map )
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Data.Sequence as Seq

-- generic-lens
import Data.Generics.Product.Fields
  ( field' )

-- gi-cairo-connector
import qualified GI.Cairo.Render.Connector as Cairo
  ( renderWithContext )

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-glib
import qualified GI.GLib as GLib

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gobject
import qualified GI.GObject as GObject

-- gi-gtk
import qualified GI.Gtk as GTK

-- haskell-gi-base
import qualified Data.GI.Base as GI
import qualified Data.GI.Base.GObject as GI
import qualified Data.GI.Base.GType as GI
import qualified Data.GI.Base.GValue as GI
import qualified Data.GI.Base.Overloading as GI

-- lens
import Control.Lens
  ( over )

-- stm
import qualified Control.Concurrent.STM as STM

-- text
import Data.Text
  ( Text )

-- MetaBrush
import Debug.Utils
  ( trace )
import MetaBrush.Application.Context
import MetaBrush.Application.UpdateDocument
import MetaBrush.Asset.Brushes
  ( lookupBrush )
import MetaBrush.Asset.StrokeIcons
  ( drawVisible, drawInvisible, drawLocked, drawUnlocked )
import MetaBrush.Brush
import MetaBrush.Document
import MetaBrush.Document.Diff
import MetaBrush.Document.History
import MetaBrush.GTK.Util
import MetaBrush.Layer
import MetaBrush.Stroke hiding ( Layer(..) )
import MetaBrush.Unique
import MetaBrush.UI.Menu
  ( strokeMenuDescription, makeMenu )

--------------------------------------------------------------------------------

-- | Custom GTK object used to hold layer data.
--
-- These are the items that will get stored in the ListModel used by GTK
-- to store the layer hierarchy data.
newtype LayerItem = LayerItem ( GTK.ManagedPtr LayerItem )

instance GI.TypedObject LayerItem  where
  glibType = GI.registerGType LayerItem

instance GI.GObject LayerItem

instance GI.HasParentTypes LayerItem
type instance GI.ParentTypes LayerItem = '[ GObject.Object ]


instance GI.DerivedGObject LayerItem where
  type GObjectParentType  LayerItem = GObject.Object
  type GObjectPrivateData LayerItem = Maybe Layer
  objectTypeName = "MetaBrush-LayerItem"
  objectClassInit _ = return ()
  objectInstanceInit _ _ = return Nothing
  objectInterfaces = [ ]

--------------------------------------------------------------------------------
-- Drag and drop data --
------------------------

-- | Data passed in a drag-and-drop operation associated with the drag source.
data DragSourceData
  = DragBrush     { draggedBrushName :: !( Maybe Text ) }
  | DragLayerItem { draggedLayerItem :: !ChildLayer }
  deriving stock ( Show, Eq )


--------------------------------------------------------------------------------
-- GTK TreeListModel --
-----------------------

-- | Switch to a different stroke view, creating it on the fly if it
-- doesn't exist already.
switchStrokeView :: GTK.ListView -> Variables -> Maybe Unique -> IO ()
switchStrokeView layersListView vars@( Variables { .. } ) mbDocUnique = do
  mbDocListModel <- for mbDocUnique $ \ docUnique -> do
    models <- STM.readTVarIO strokeListModelsTVar
    case Map.lookup docUnique models of
      Just lm -> return lm
      Nothing -> do
        lm <- newLayersListModel vars docUnique
        STM.atomically $
          STM.modifyTVar' strokeListModelsTVar ( Map.insert docUnique lm )
        return lm
  GTK.listViewSetModel layersListView mbDocListModel

-- | Create a new 'GTK.TreeListModel' with 'GTK.MultiSelection'
-- from the document with the given 'Unique'.
newLayersListModel :: Variables -> Unique -> IO GTK.MultiSelection
newLayersListModel ( Variables { .. } ) docUnique = do

  itemType <- GI.glibType @LayerItem

  store0 <- GIO.listStoreNew itemType
  ( store, mbDocHist ) <- STM.atomically do
    mbDocHist <- Map.lookup docUnique <$> STM.readTVar openDocumentsTVar
    mbDocStore <- Map.lookup docUnique <$> STM.readTVar parStoresTVar
    store <- case mbDocStore of
      Just store
        | Just rootStore <- Map.lookup Root store
        -> return rootStore
      _ -> do
        STM.modifyTVar' parStoresTVar ( Map.insert docUnique ( Map.singleton Root store0 ) )
        return store0
    return ( store, mbDocHist )

  for_ mbDocHist $ \ activeDocHist -> do

    let layerHierarchy = strokeHierarchy $ documentContent $ present activeDocHist
    for_ ( topLevel layerHierarchy ) $ \ layerUniq -> do
      let mbChildren = lookupChildren_maybe ( Parent layerUniq ) layerHierarchy
          layer = case mbChildren of
            Nothing -> StrokeLayer { layerUnique = layerUniq }
            Just {} -> GroupLayer  { layerUnique = layerUniq }
      item <- GI.unsafeCastTo LayerItem =<< GI.new LayerItem []
      GI.gobjectSetPrivateData item ( Just layer )
      GIO.listStoreAppend store item

  rootModel <- GIO.toListModel store
  let passthrough = False -- Must not use passthrough to use TreeExpander widgets.
      autoExpand  = True  -- Autoexpand on creation; we later set this to False.


  -- Pass a copy of the (reference to the) root GIO.ListStore to the
  -- 'treeListModelNew' function to ensure we retain ownership of it.
  model <- GI.withManagedPtr rootModel $ \ rmPtr ->
           GI.withNewObject rmPtr $ \ rmCopy ->
             GTK.treeListModelNew rmCopy passthrough autoExpand
               createChildModel

  -- After the initial model has been created, we set "autoexpand" to False,
  -- so that if we move a non-expanded group, it doesn't get automatically
  -- expanded after it is moved.
  GTK.treeListModelSetAutoexpand model False

  selectionModel <- GI.withManagedPtr model $ \ lmPtr ->
                    GI.withNewObject lmPtr $ \ lmCopy ->
                    GTK.multiSelectionNew ( Just lmCopy )

  return selectionModel

    where
      createChildModel :: GObject.Object -> IO ( Maybe GIO.ListModel )
      createChildModel parent = do
        dat <- getLayerData =<< GTK.unsafeCastTo LayerItem parent
        case dat of
          StrokeLayer {} -> return Nothing
          GroupLayer { layerUnique = groupUnique } -> do
            mbDocHist <- Map.lookup docUnique <$> STM.atomically ( STM.readTVar openDocumentsTVar )
            case mbDocHist of
              Nothing -> return Nothing
              Just docHist -> do
                let layerHierarchy = strokeHierarchy $ documentContent $ present docHist
                    children = lookupChildren ( Parent groupUnique ) layerHierarchy

                -- Try to re-use an existing list store, if there is one.
                mbOldChildStore <-
                  STM.atomically $ do
                    parStores <- STM.readTVar parStoresTVar
                    return $
                      Map.lookup ( Parent groupUnique )
                        =<< Map.lookup docUnique parStores

                newChildStore <-
                  case mbOldChildStore of
                    Just oldStore -> do
                      return oldStore
                    Nothing -> do
                      -- Otherwise, create a new child ListModel.
                      -- NB: create a simple GIO.ListStore, not a nested GTK.TreeListModel,
                      -- as that would cause e.g. grand-children models to be created twice.
                      itemType <- GI.glibType @LayerItem
                      childStore <- GIO.listStoreNew itemType

                      for_ children $ \ childUniq -> do
                        let mbChildChildren = lookupChildren_maybe ( Parent childUniq ) layerHierarchy
                            childLayer = case mbChildChildren of
                              Nothing -> StrokeLayer{ layerUnique = childUniq }
                              Just {} -> GroupLayer { layerUnique = childUniq }
                        item <- GI.unsafeCastTo LayerItem =<< GI.new LayerItem []
                        GI.gobjectSetPrivateData item ( Just childLayer )
                        GIO.listStoreAppend childStore item

                      -- Store the child store in our mapping from parent unique to
                      -- ListStore, so that we know where to insert children.
                      STM.atomically $ do
                        STM.modifyTVar parStoresTVar
                          ( Map.insertWith Map.union docUnique ( Map.singleton ( Parent groupUnique ) childStore ) )
                      return childStore

                -- Pass a copy of the (reference to the) child store
                -- to ensure we retain ownership of it.
                childModelCopy <- GI.withManagedPtr newChildStore $ \ childStorePtr ->
                                  GI.withNewObject childStorePtr $ \ childStoreCopy ->
                                  GIO.toListModel childStoreCopy

                return $ Just childModelCopy

-- | Gets the 'LayerItem' for a row in a 'GTK.TreeListModel'
-- with @passthrough = False@.
treeListItemLayerItem :: HasCallStack => GTK.ListItem -> IO ( Maybe LayerItem )
treeListItemLayerItem listItem = do
  mbListRow <- GTK.listItemGetItem listItem
  case mbListRow of
    Nothing -> return Nothing
    Just listRow -> do
      layerItem <-
        treeListRowLayerItem =<< GTK.unsafeCastTo GTK.TreeListRow listRow
      return $ Just layerItem

-- | Retrieve the 'LayerItem' underlying a 'GTK.TreeListRow'.
treeListRowLayerItem :: HasCallStack => GTK.TreeListRow -> IO LayerItem
treeListRowLayerItem listRow = do
  mbListRowItem <- GTK.treeListRowGetItem listRow
  case mbListRowItem of
    Nothing   -> error "treeListRowLayerItem: TreeListRow has no item"
    Just item -> GTK.unsafeCastTo LayerItem item
      -- NB: if you made the mistake of passing a 'createChildModel' function
      -- which recursively creates a TreeListModel, you would have to recurse
      -- into the row using 'treeListRowGetItem' to eventually get at the
      -- underlying data.

-- | Class for objects which wrap a 'LayerItem'.
class HasLayerData a where
  -- | Get the layer data associated to a 'LayerItem' or object
  -- containing a 'LayerItem'.
  getLayerData :: HasCallStack => a -> IO Layer
instance HasLayerData LayerItem where

  getLayerData item = do
    mbDat <- GI.gobjectGetPrivateData item
    case mbDat of
      Nothing -> error "getLayerData: no private data"
      Just dat -> return dat
instance HasLayerData GTK.TreeListRow where
  getLayerData row = do
    parLayerItem <- treeListRowLayerItem row
    getLayerData parLayerItem

-- | Get the Layer data for this 'ListItem', or 'Nothing' if the 'ListItem'
-- is currently unbound.
listItemLayerData :: GTK.ListItem -> IO ( Maybe Layer )
listItemLayerData listItem = do
  mbLayerItem <- treeListItemLayerItem listItem
  traverse getLayerData mbLayerItem

-----------------------
-- GTK layer widgets --
-----------------------

-- | The generic widget used to display a list item.
--
-- Structure:
--
--  - ListItem
--    - TreeExpander
--      - ContentBox
--        - CheckButton
--        - Label
data LayerViewWidget =
  LayerViewWidget
    { layerViewContentBox    :: GTK.Box
    , layerViewVisibleButton :: ( GTK.Button, GTK.DrawingArea )
    , layerViewLockedButton  :: ( GTK.Button, GTK.DrawingArea )
    , layerViewLabel         :: GTK.EditableLabel
    }

newLayerViewWidget :: IO GTK.TreeExpander
newLayerViewWidget = do

  expander <- GTK.treeExpanderNew

  GTK.treeExpanderSetIndentForIcon  expander True
  GTK.treeExpanderSetIndentForDepth expander True
  GTK.treeExpanderSetHideExpander   expander False

  contentBox <- GTK.boxNew GTK.OrientationHorizontal 0
  GTK.treeExpanderSetChild expander ( Just contentBox )

  itemLabel <- editableLabelNew " "
  GTK.boxAppend contentBox itemLabel
  GTK.widgetSetHalign  itemLabel GTK.AlignStart
  GTK.widgetSetHexpand itemLabel True

  visibleButton <- GTK.buttonNew
  lockedButton  <- GTK.buttonNew
  GTK.boxAppend contentBox visibleButton
  GTK.boxAppend contentBox lockedButton
  GTK.widgetSetHalign visibleButton GTK.AlignEnd
  GTK.widgetSetHalign lockedButton  GTK.AlignEnd

  widgetAddClass visibleButton "strokeButton"
  widgetAddClass  lockedButton "strokeButton"

  visibleButtonArea <- GTK.drawingAreaNew
  lockedButtonArea  <- GTK.drawingAreaNew

  GTK.buttonSetChild visibleButton ( Just visibleButtonArea )
  GTK.buttonSetChild  lockedButton ( Just  lockedButtonArea )

  return expander

-- | Get the widget hierarchy for a list item, so that we can modify
-- the wdigets to display the appropriate content.
getLayerViewWidget :: GTK.TreeExpander -> IO LayerViewWidget
getLayerViewWidget expander = do
  mbContentBox <- GTK.treeExpanderGetChild expander
  case mbContentBox of
    Nothing -> error "getLayerViewWidget: expected ListItem->Expander->Box"
    Just contentBox0 -> do
      contentBox <- GTK.unsafeCastTo GTK.Box contentBox0
      mbLayerLabel <- traverse ( GTK.unsafeCastTo GTK.EditableLabel ) =<< GTK.widgetGetFirstChild contentBox
      case mbLayerLabel of
        Nothing -> error "getLayerViewWidget: expected ListItem->Expander->Box->LayerLabel"
        Just layerLabel -> do
          mbVisibleButton <- traverse ( GTK.unsafeCastTo GTK.Button ) =<< GTK.widgetGetNextSibling layerLabel
          case mbVisibleButton of
            Nothing -> error "getLayerViewWidget: expected ListItem->Expander->Box->{LayerLabel,Button}"
            Just visibleButton -> do
              mbVisibleArea <- traverse ( GTK.unsafeCastTo GTK.DrawingArea ) =<< GTK.widgetGetFirstChild visibleButton
              case mbVisibleArea of
                Nothing -> error "getLayerViewWidget: expected ListItem->Expander->Box->{LayerLabel,Button->DrawingArea}"
                Just visibleArea -> do
                  mbLockedButton <- traverse ( GTK.unsafeCastTo GTK.Button ) =<< GTK.widgetGetNextSibling visibleButton
                  case mbLockedButton of
                    Nothing -> error "getLayerViewWidget: expected ListItem->Expander->Box->{LayerLabel,Button,Button}"
                    Just lockedButton -> do
                      mbLockedArea <- traverse ( GTK.unsafeCastTo GTK.DrawingArea ) =<< GTK.widgetGetFirstChild lockedButton
                      case mbLockedArea of
                        Nothing -> error "getLayerViewWidget: expected ListItem->Expander->Box->{LayerLabel,Button,Button->DrawingArea}"
                        Just lockedArea -> do
                          return $
                            LayerViewWidget
                              { layerViewContentBox = contentBox
                              , layerViewVisibleButton = ( visibleButton, visibleArea )
                              , layerViewLockedButton = ( lockedButton, lockedArea )
                              , layerViewLabel = layerLabel
                              }

------------------
-- GTK ListView --
------------------

-- | Create a new 'GTK.ListView' that displays 'LayerItem's.
newLayerView :: UIElements -> Variables -> IO GTK.ListView
newLayerView uiElts@( UIElements { window, colours } ) vars = mdo

  layersListFactory <- GTK.signalListItemFactoryNew
  layerMenu <- GIO.menuNew
  layerPopover <- GTK.popoverMenuNewFromModel ( Nothing @GIO.Menu )
  widgetAddClasses layerPopover [ "strokeMenu", "text", "plain" ]

  -- Connect to "setup" signal to create generic widgets for viewing the tree.
  --
  -- We attach a collection of signals to each widget,
  -- to handle e.g. drag-and-drop operations.
  --
  -- We attach the signals in the "setup" phase, because we don't want
  -- to have to keep attaching/removing event controllers to the widgets
  -- (in the "bind" and "unbind" stages).
  --
  -- However, in the "setup" phase, we don't yet know which underlying ListModel
  -- item we are displaying (this is only set on "bind").
  -- So: how can we set signal handlers in the "setup" phase? The answer is that
  -- each signal handler will read the private data associated to the widget;
  -- this data gets set when binding the widget to its ListModel item.
  _ <- GTK.onSignalListItemFactorySetup layersListFactory $ \ listItem0 -> do

        listItem <- GTK.unsafeCastTo GTK.ListItem listItem0
        GTK.listItemSetFocusable listItem False

        expander <- newLayerViewWidget
        GTK.listItemSetChild listItem ( Just expander )
        widgetAddClass expander "layer-item"

        LayerViewWidget
          { layerViewLabel         = label
          , layerViewVisibleButton = ( visibleButton, visibleDrawingArea )
          , layerViewLockedButton  = (  lockedButton,  lockedDrawingArea )
          }
            <- getLayerViewWidget expander

        ------------------
        -- GestureClick --
        ------------------

        click <- GTK.gestureClickNew
        GTK.gestureSingleSetButton click 0 -- listen to both left and right clicks

        void $ GTK.onGestureClickPressed click $ \ _nbClicks x y -> do
          button <- GTK.gestureSingleGetCurrentButton ?self
          if -- | button <= 1 && nbClicks > 1
             -- -> do GTK.editableSetEditable label True
             --       GTK.editableLabelStartEditing label
             --       void $ GTK.widgetGrabFocus label
             | button == 3
             -> do
              mbLayer <- listItemLayerData listItem
              for_ mbLayer $ \ layer -> do
                mbTreeListRow <- traverse ( GTK.unsafeCastTo GTK.TreeListRow ) =<< GTK.listItemGetItem listItem
                treeListRow <- case mbTreeListRow of
                    Nothing -> error "newLayerView ListItem onSetup: no TreeListRow"
                    Just r -> return r
                srcPar <- fmap snd <$> getParent treeListRow
                GTK.widgetUnparent layerPopover
                GTK.widgetSetParent layerPopover expander

                GIO.menuRemoveAll layerMenu
                makeMenu uiElts vars layerMenu ( strokeMenuDescription ( WithinParent srcPar layer ) )
                GTK.popoverMenuSetMenuModel layerPopover ( Just layerMenu )
                rect <- GDK.newZeroRectangle
                GDK.setRectangleX rect ( round x )
                GDK.setRectangleY rect ( round y )
                GTK.popoverSetPointingTo layerPopover ( Just rect )
                GTK.popoverPopup layerPopover
             | otherwise
             -> return ()

        ----------------------------
        -- Visible/locked buttons --
        ----------------------------

        void $ GTK.onButtonClicked visibleButton $ do
          mbLayer <- listItemLayerData listItem
          for_ mbLayer $ \ layer -> do
            let uniq = layerUnique layer
            modifyingCurrentDocument uiElts vars \ doc -> do
              let wasInvisible = Set.member uniq ( invisibleLayers $ layerMetadata $ documentMetadata doc )
                  doc' =
                    over ( field' @"documentMetadata" . field' @"layerMetadata"
                         . field' @"invisibleLayers"
                         )
                      ( if wasInvisible then Set.delete uniq else Set.insert uniq )
                      doc
              return $
                UpdateDocAndThen ( UpdateDocumentTo doc' TrivialDiff ) $
                  GTK.widgetQueueDraw visibleDrawingArea

        void $ GTK.onButtonClicked lockedButton $ do
          mbLayer <- listItemLayerData listItem
          for_ mbLayer $ \ layer -> do
            let uniq = layerUnique layer
            modifyingCurrentDocument uiElts vars \ doc -> do
              let wasLocked = Set.member uniq ( lockedLayers $ layerMetadata $ documentMetadata doc )
                  doc' =
                    over ( field' @"documentMetadata" . field' @"layerMetadata"
                         . field' @"lockedLayers"
                         )
                      ( if wasLocked then Set.delete uniq else Set.insert uniq )
                      doc
              return $
                UpdateDocAndThen ( UpdateDocumentTo doc' TrivialDiff ) $
                  GTK.widgetQueueDraw lockedDrawingArea

        GTK.drawingAreaSetDrawFunc visibleDrawingArea $ Just \ _ cairoContext _ _ -> do
          mbLayer <- listItemLayerData listItem
          for_ mbLayer $ \ layer -> do
            let uniq = layerUnique layer
            mbActiveDoc <- STM.atomically $ activeDocument vars
            for_ mbActiveDoc $ \ ( _activeDocUnique, activeDocHist ) -> do
              let meta = layerMetadata $ documentMetadata $ present activeDocHist
                  invis = Set.member uniq ( invisibleLayers meta )
              if invis
              then void $ Cairo.renderWithContext ( drawInvisible colours ) cairoContext
              else void $ Cairo.renderWithContext ( drawVisible   colours ) cairoContext

        GTK.drawingAreaSetDrawFunc lockedDrawingArea $ Just \ _ cairoContext _ _ -> do
          mbLayer <- listItemLayerData listItem
          for_ mbLayer $ \ layer -> do
            let uniq = layerUnique layer
            mbActiveDoc <- STM.atomically $ activeDocument vars
            for_ mbActiveDoc $ \ ( _activeDocUnique, activeDocHist ) -> do
              let meta = layerMetadata $ documentMetadata $ present activeDocHist
                  locked = Set.member uniq ( lockedLayers meta )
              if locked
              then void $ Cairo.renderWithContext ( drawLocked   colours ) cairoContext
              else void $ Cairo.renderWithContext ( drawUnlocked colours ) cairoContext

        -------------------
        -- EditableLabel --
        -------------------

        -- Connect a signal for editing the layer name.
        void $ GI.on label ( GI.PropertyNotify #editing ) $ \ _ -> do
          editing <- GTK.editableLabelGetEditing label

          -- Only perform an update once we have stopped editing.
          when ( not editing ) do
            newText <- GTK.editableGetText label
            mbLayer <- listItemLayerData listItem
            for_ mbLayer $ \ layer -> do
              -- The layer might not be bound currently, e.g. because it is
              -- being moved. I think it's OK to just ignore anything
              -- in such a situation.
              GTK.editableSetEditable label False
              traverse_ ( `GIO.setSimpleActionEnabled` True ) ( menuActions uiElts )
                -- NB: 'modifyingCurrentDocument' below will properly re-disable
                -- 'undo' and 'redo' if there is nothing to undo or redo.
              modifyingCurrentDocument uiElts vars \ doc -> do
                let doc' =
                     over ( field' @"documentMetadata" . field' @"layerMetadata"
                          . field' @"layerNames"
                          )
                        ( Map.insert ( layerUnique layer ) newText )
                        doc
                return $
                  UpdateDoc ( UpdateDocumentTo doc' TrivialDiff )

        -- Make it so that we need a second click on the selected row to
        -- trigger editing of the layer name.
        labelClicks <- GTK.gestureClickNew
        void $ GTK.onGestureClickReleased labelClicks $ \ _nbClicks _x _y -> do
          button <- GTK.gestureSingleGetCurrentButton ?self
          isSingleSelRow <- isSingleSelected listView listItem
          case button of
            1 -> do
              when isSingleSelRow do
                -- Disable all accelerators when doing text entry.
                traverse_ ( `GIO.setSimpleActionEnabled` False ) ( menuActions uiElts )
                GTK.editableSetEditable label True
                GTK.editableLabelStartEditing label
            _ -> return ()

        ----------------
        -- DragSource --
        ----------------

        -- Connect signals for starting a drag from this widget.
        dragSource <- GTK.dragSourceNew
        GTK.dragSourceSetActions dragSource [ GDK.DragActionCopy ]

        void $ GTK.onDragSourcePrepare dragSource $ \ _x _y -> do
          mbLayer <- listItemLayerData listItem
          case mbLayer of
            Nothing -> return Nothing
            Just layer -> do
              let srcUniq = layerUnique layer

              mbTreeListRow <- traverse ( GTK.unsafeCastTo GTK.TreeListRow ) =<< GTK.listItemGetItem listItem
              treeListRow <- case mbTreeListRow of
                  Nothing -> error "newLayerView ListItem onSetup: no TreeListRow"
                  Just r -> return r
              srcPar <- getParent treeListRow

              mbSelModel <- GTK.listViewGetModel listView
              for_ mbSelModel \ selModel0 -> do
                selModel <- GTK.unsafeCastTo GTK.MultiSelection selModel0
                rowPos <- GTK.treeListRowGetPosition treeListRow
                GTK.selectionModelSelectItem selModel rowPos True -- True <=> unselect rest

              let dnd_sourceItem =
                    DragLayerItem $
                      WithinParent
                        { parent = fmap snd srcPar
                        , item = srcUniq
                        }

              val <- GDK.contentProviderNewForValue =<< GIO.toGValue ( GI.HValue dnd_sourceItem )
              widgetAddClass window "dragging-item"
              return $ Just val

        void $ GTK.onDragSourceDragBegin dragSource $ \ _drag -> do

{- To set a cursor icon for the drag, write the x/y coordinates in the
   'prepare' signal handler to an IORef, and then use the following:
          ( x, y ) <- readIORef dragPosRef
          paintable <- GTK.widgetPaintableNew ( Just expander )
          GTK.dragSourceSetIcon ?self ( Just paintable ) ( round x ) ( round y )
-}
          noPaintable <- GDK.paintableNewEmpty 0 0
          GTK.dragSourceSetIcon ?self ( Just noPaintable ) 0 0
          widgetAddClass expander "dragged"
          -- TODO: add "dragged" class for all descendants as well.
        void $ GTK.onDragSourceDragCancel dragSource $ \ _drag _reason -> do
          GTK.widgetRemoveCssClass window "dragging-item"
          GTK.widgetRemoveCssClass expander "dragged"
          return True
              -- ^^^^ Important. Setting this to 'False' stops GDK
              -- from properly clearing the drag cursor.
        void $ GTK.onDragSourceDragEnd dragSource $ \ _drag _deleteData -> do
          GTK.widgetRemoveCssClass window "dragging-item"
          GTK.widgetRemoveCssClass expander "dragged"

        ----------------
        -- DropTarget --
        ----------------

        -- Connect signals for receiving a drop on this widget.
        dropTarget <- GTK.dropTargetNew GI.gtypeHValue [ GDK.DragActionCopy ]

        let dropTargetCleanup = do
              GTK.widgetRemoveCssClass expander "drag-over"
              GTK.widgetRemoveCssClass expander "drag-top"
              GTK.widgetRemoveCssClass expander "drag-bot"
              mbNextItem <- getNextItem_maybe expander
              for_ mbNextItem $ \ nextItem -> do
                GTK.widgetRemoveCssClass nextItem "drag-top"
        void $ GTK.onDropTargetAccept dropTarget $ \ dropObj -> do
          dragFormats <- GDK.dropGetFormats dropObj
          hasHValueGType <- GDK.contentFormatsContainGtype dragFormats GI.gtypeHValue
          return hasHValueGType
        void $ GTK.onDropTargetDrop dropTarget $ \ val _x y -> do
          mbDropTgt <- listItemLayerData listItem
          case mbDropTgt of
            Nothing -> return False
            Just dropTgt -> do
              dropTargetCleanup
              GI.HValue dnd_data <- GIO.fromGValue @( GI.HValue DragSourceData ) val

              mbTreeListRow <- traverse ( GTK.unsafeCastTo GTK.TreeListRow ) =<< GTK.listItemGetItem listItem
              treeListRow <- case mbTreeListRow of
                  Nothing -> error "newLayerView ListItem onSetup: no TreeListRow"
                  Just r -> return r
              dstPar <- getParent treeListRow

              case dnd_data of
                DragBrush brushName -> do
                  case dropTgt of
                    GroupLayer {} ->
                      return False
                    StrokeLayer tgtStrokeUnique -> do
                      updateLayerHierarchy vars $
                        DoLayerChange $
                          SetBrush
                            { setBrushStroke = WithinParent ( fmap snd dstPar ) tgtStrokeUnique
                            , setBrushName   = brushName
                            }
                      return True

                DragLayerItem dragSrc@( WithinParent { item = dragSrcUniq }) -> do
                  let dropTgtUniq = layerUnique dropTgt
                  dstFlatIndex <- GTK.treeListRowGetPosition treeListRow
                  h <- GTK.widgetGetHeight expander
                  let dropRelPos = if y < 0.5 * fromIntegral h
                                   then Above
                                   else Below
                  expanded <- GTK.treeListRowGetExpanded treeListRow

                  isDescendant <- isDescendantOf dragSrcUniq listItem

                  let mbDropIntoGroup
                        | expanded
                        , Below <- dropRelPos
                        , not isDescendant
                        = Just treeListRow
                        | otherwise
                        = Nothing
                      mbDropOutsideGroup
                        | dragSrcUniq == dropTgtUniq
                        , Parent par <- dstPar
                        , Below <- dropRelPos
                        = Just par
                        | otherwise
                        = Nothing

                  if isDescendant && isNothing mbDropOutsideGroup
                  then do
                    return False
                  else do
                    -- Compute the destination parent.
                    -- Usually, the destination parent is the parent of the drop target.
                    -- BUT:
                    --  1. when dropping an item into the first position of an
                    --     expanded group, the destination parent is the drop target itself,
                    --     not the parent of the drop target.
                    --  2. when an item is at the bottom of a group, dropping it on its
                    --     lower half moves the item out of the group, so the
                    --     destination parent is the grand-parent of the drop target.
                    ( dropDst, newPosInTree ) <-
                      if
                        -- (1)
                        | Just dstParRow <- mbDropIntoGroup
                        -> do
                            dstParFlatIndex <- GTK.treeListRowGetPosition dstParRow
                            return $
                              ( MoveToTopOfGroup dropTgtUniq
                              , dstParFlatIndex + 1
                              )
                        -- (2)
                        | Just ( dstParRow, dstParUniq ) <- mbDropOutsideGroup
                        -> do
                            grandPar <- getParent dstParRow
                            return $
                              ( MoveItemOutsideGroupIfLastItemInGroup
                                  { itemUnique        = dropTgtUniq
                                  , parentUnique      = dstParUniq
                                  , grandParentUnique = fmap snd grandPar
                                  , itemExpanded      = expanded
                                  }
                              , dstFlatIndex + 1
                              )
                        | otherwise
                        -> do
                          return $
                            ( MoveAboveOrBelow
                                { moveDstItem =
                                    WithinParent
                                      { parent = fmap snd dstPar
                                      , item = dropTgtUniq
                                      }
                                , moveRelPos = dropRelPos
                                }
                            , case dropRelPos of
                                Above -> dstFlatIndex
                                Below -> dstFlatIndex + 1
                            )

                    -- Compute the position that the item we are moving will have
                    -- at the end of the move.
                    --
                    -- First, we compute whether we moved up or down.
                    -- NB: we need to compute the source item position now (using 'treeListRowGetPosition'),
                    -- at the end of the drag-and-drop operation, because TreeExpander nodes
                    -- might have expanded/collapsed in the meantime.
                    mbSelModel <- GTK.listViewGetModel listView
                    case mbSelModel of
                      Nothing -> return False
                      Just selModel0 -> do
                        selModel <- GTK.unsafeCastTo GTK.MultiSelection selModel0
                        layersListModel
                          <- GTK.unsafeCastTo GTK.TreeListModel =<< fmap fromJust ( GTK.multiSelectionGetModel selModel )
                        mbSelIx <- singleSelection_maybe listView

                        -- Now compute the final destination position.
                        mbDstPosAfterShift <-
                          case mbSelIx of
                            Nothing ->
                              return Nothing
                            Just selIx
                              -- If we moved up, simply use the destination position.
                              | selIx >= newPosInTree
                              -> return $ Just newPosInTree
                              | otherwise
                              -> do
                              -- If we moved down, we need to substract the number of items
                              -- moved. Note that this depends on which TreeExpander nodes
                              -- are expanded.
                              mbSelRow <- GTK.treeListModelGetRow layersListModel selIx
                              case mbSelRow of
                                Nothing -> return Nothing
                                Just selRow0 -> do
                                  selRow <- GTK.unsafeCastTo GTK.TreeListRow selRow0
                                  nbDescendants <- getNbExpandedDescendants layersListModel selRow
                                  return $
                                    if newPosInTree < nbDescendants
                                    then Nothing
                                    else Just $ newPosInTree - nbDescendants

                        updateLayerHierarchy vars $
                          DoLayerChange $
                            Move
                              { moveSrc = dragSrc
                              , moveDst = dropDst
                              }

                        -- After moving, update the selected item to be the moved item.
                        case mbDstPosAfterShift of
                          Nothing -> return ()
                          Just dstPos ->
                            void $ GTK.selectionModelSelectItem selModel dstPos True
                        return True

        void $ GTK.onDropTargetEnter dropTarget $ \ _x y -> do
          widgetAddClass expander "drag-over"
          h <- GTK.widgetGetHeight expander
          if y < 0.5 * fromIntegral h
          then do
            widgetAddClass expander "drag-top"
          else do
            widgetAddClass expander "drag-bot"
            mbNextItem <- getNextItem_maybe expander
            for_ mbNextItem $ \ nextItem -> do
              widgetAddClass nextItem "drag-top"
          return [ GDK.DragActionCopy ]
        void $ GTK.onDropTargetMotion dropTarget $ \ _x y -> do
          h <- GTK.widgetGetHeight expander
          if y < 0.5 * fromIntegral h
          then do
            GTK.widgetRemoveCssClass expander "drag-bot"
            widgetAddClass expander "drag-top"
            mbNextItem <- getNextItem_maybe expander
            for_ mbNextItem $ \ nextItem -> do
              GTK.widgetRemoveCssClass nextItem "drag-top"
          else do
            GTK.widgetRemoveCssClass expander "drag-top"
            widgetAddClass expander "drag-bot"
            mbNextItem <- getNextItem_maybe expander
            for_ mbNextItem $ \ nextItem -> do
              widgetAddClass nextItem "drag-top"
          return [ GDK.DragActionCopy ]
        void $ GTK.onDropTargetLeave dropTarget $ do
          dropTargetCleanup

        GTK.widgetAddController label    labelClicks
        GTK.widgetAddController expander dragSource
        GTK.widgetAddController expander dropTarget
        GTK.widgetAddController expander click

  -- Connect to "bind" signal to modify the generic widget to display the data for this list item.
  _ <- GTK.onSignalListItemFactoryBind layersListFactory $ \ listItem0 -> do

        listItem <- GTK.unsafeCastTo GTK.ListItem listItem0
        mbExpander <- GTK.listItemGetChild listItem
        expander <-
          case mbExpander of
            Nothing -> error "layerView onBind: list item has no child"
            Just expander0 -> GTK.unsafeCastTo GTK.TreeExpander expander0

        LayerViewWidget
          { layerViewLabel = layerLabel
          , layerViewVisibleButton = ( _, visibleDrawingArea )
          , layerViewLockedButton  = ( _,  lockedDrawingArea )
          } <- getLayerViewWidget expander

        mbLayer <- listItemLayerData listItem
        layer <- case mbLayer of
          Nothing -> error "layer is unbound... but it has just been bound?"
          Just l -> return l
        mbActiveDoc <- STM.atomically $ activeDocument vars
        for_ mbActiveDoc $ \ ( _activeDocUnique, activeDocHist ) -> do
          let meta = layerMetadata $ documentMetadata $ present activeDocHist

          -- All we do is set the name of this layer/group,
          -- and re-render the visible/locked buttons.
          let mbLayerText  = Map.lookup ( layerUnique layer ) ( layerNames meta )
          mbTreeListRow <- traverse ( GTK.unsafeCastTo GTK.TreeListRow ) =<< GTK.listItemGetItem listItem
          treeListRow <- case mbTreeListRow of
            Nothing -> error "newLayerView ListItem onBind: no TreeListRow"
            Just r -> return r
          GTK.treeExpanderSetListRow expander ( Just treeListRow )
          for_ mbLayerText $ GTK.editableSetText layerLabel
          GTK.widgetQueueDraw visibleDrawingArea
          GTK.widgetQueueDraw  lockedDrawingArea

  listView <- GTK.listViewNew ( Nothing @GTK.MultiSelection ) ( Just layersListFactory )
  return listView

singleSelection_maybe :: GTK.ListView -> IO ( Maybe Word32 )
singleSelection_maybe listView = do
  mbSelModel <- GTK.listViewGetModel listView
  case mbSelModel of
    Nothing -> return Nothing
    Just selModel0 -> do
      selModel <- GTK.unsafeCastTo GTK.MultiSelection selModel0
      selItemsBitSet <- GTK.selectionModelGetSelection selModel
      sz <- GTK.bitsetGetSize selItemsBitSet
      if sz == 1
      then Just <$> GTK.bitsetGetNth selItemsBitSet 0
      else return Nothing

isSingleSelected :: GTK.ListView -> GTK.ListItem -> IO Bool
isSingleSelected listView listItem = do
  mbSel <- singleSelection_maybe listView
  case mbSel of
    Nothing -> return False
    Just sel -> do
      mbTreeListRow <- traverse ( GTK.unsafeCastTo GTK.TreeListRow ) =<< GTK.listItemGetItem listItem
      case mbTreeListRow of
        Nothing -> return False
        Just row -> do
          pos <- GTK.treeListRowGetPosition row
          return $ sel == pos

-- | Get the next item in the flattened tree, if any.
-- Ignores any notion of parent/child.
--
-- This is used to style the "item below" the current drop target,
-- to account for the fact that the item below is rendered on top of the item
-- above it, which overdraws any shadow/glow extending downwards on the latter.
getNextItem_maybe :: GTK.TreeExpander -> IO ( Maybe ( GTK.TreeExpander ) )
getNextItem_maybe expander = do
  mbParent <- GTK.widgetGetParent expander
  case mbParent of
    Nothing -> error "nextItem: item has no parent"
    Just parent -> do
      mbNextItemParent <- GTK.widgetGetNextSibling parent
      case mbNextItemParent of
        Nothing -> return Nothing
        Just nextItemParent -> do
          mbNextItem <- GTK.widgetGetFirstChild nextItemParent
          case mbNextItem of
            Nothing -> error "nextItem: next item has no child"
            Just nextItem0 -> do
              nextItem <- GTK.unsafeCastTo GTK.TreeExpander nextItem0
              return $ Just nextItem

-- | Is this list item a descendant of the item with the given unique?
isDescendantOf :: Unique -- ^ are we a descendant of this?
               -> GTK.ListItem -- ^ item we are querying
               -> IO Bool
isDescendantOf u listItem = do
  mbListRow <- GTK.listItemGetItem listItem
  case mbListRow of
    Nothing -> error "isDescendantOf: ListItem has no item"
    Just listRow0 -> do
      listRow <- GTK.unsafeCastTo GTK.TreeListRow listRow0
      go listRow
  where
    go :: GTK.TreeListRow -> IO Bool
    go listRow = do
      u' <- layerUnique <$> getLayerData listRow
      if u' == u
      then return True
      else do
        mbPar <- GTK.treeListRowGetParent listRow
        case mbPar of
          Nothing -> return False
          Just par -> go par

-- | Get the number of expanded descendants of the given 'GTK.TreeListRow',
-- including the row itself.
getNbExpandedDescendants :: GTK.TreeListModel -> GTK.TreeListRow -> IO Word32
getNbExpandedDescendants layersListModel = fmap fst . go
  where
    go :: GTK.TreeListRow -> IO ( Word32, Word32 )
    go row0 = do
      pos0 <- GTK.treeListRowGetPosition row0
      expanded <- GTK.treeListRowGetExpanded row0
      if not expanded
      then do
        return ( 1, pos0 )
      else do
        ( nbChildItems, lastPosChecked ) <- goChildren ( row0, pos0 )
        return ( 1 + nbChildItems, lastPosChecked )
    goChildren :: ( GTK.TreeListRow, Word32 ) -> IO ( Word32, Word32 )
    goChildren ( row, lastPosChecked ) = do
      mbRow' <- GTK.treeListModelGetRow layersListModel ( lastPosChecked + 1 )
      case mbRow' of
        Nothing -> do
          return ( 0, lastPosChecked + 1 )
        Just nextRow0 -> do
          nextRow <- GTK.unsafeCastTo GTK.TreeListRow nextRow0
          mbNextParent <- GTK.treeListRowGetParent nextRow
          case mbNextParent of
            Just nextParent
              | nextParent == row
              -> do
                ( nbChildren, lastChecked ) <- go nextRow
                ( n, lastChecked' ) <- goChildren ( row, lastChecked )
                return ( nbChildren + n, lastChecked' )
            _ -> return ( 0, lastPosChecked )


-- | Retrieve the parent of the current 'GTK.TreeListRow', if any.
getParent :: GTK.TreeListRow -> IO ( Parent ( GTK.TreeListRow, Unique ) )
getParent treeListRow = do
  mbPar <- GTK.treeListRowGetParent treeListRow
  case mbPar of
    Nothing ->
      return Root
    Just p -> do
      parUniq <- layerUnique <$> getLayerData p
      return ( Parent ( p, parUniq ) )

-- | Get the 'Unique' of the selected item in the 'ListView', if any.
getSelectedItem :: GTK.ListView -> IO ( Maybe ChildLayer )
getSelectedItem layersView = do
  mbSelectionModel <- GTK.listViewGetModel layersView
  case mbSelectionModel of
    Nothing -> error "getSelectedItem: no SelectionModel"
    Just selModel0 -> do
      selModel <- GTK.unsafeCastTo GTK.SingleSelection selModel0
      mbRow <- GTK.singleSelectionGetSelectedItem selModel
      for mbRow $ \ row0 -> do
        row <- GTK.unsafeCastTo GTK.TreeListRow row0
        mbItem <- GTK.treeListRowGetItem row
        case mbItem of
          Nothing -> error "getSelectedItem: row has no item"
          Just item0 -> do
            layer     <- GTK.unsafeCastTo LayerItem item0
            layerData <- layerUnique <$> getLayerData layer
            parent <- getParent row
            return $
              WithinParent
                { parent = fmap snd parent
                , item = layerData
                }

--------------------------------------------------------------------------------

-- | Do, undo or redo?
data DoLayerChange = DoLayerChange !LayerChange | RedoChange | UndoChange

-- | Description of a change to the layer hierarchy.
data LayerChange
  = Move
      { moveSrc :: !ChildLayer
      , moveDst :: !MoveDst
      }
  | NewItem
      { newUnique   :: !Unique
      , newIsGroup  :: !Bool
      , newPosition :: !( ChildLayer, RelativePosition )
      }
  | DeleteItems
      { deleteItems :: !( NE.NonEmpty ChildLayer )
      }
  | SetBrush
      { setBrushStroke :: !ChildLayer
      , setBrushName   :: !( Maybe Text )
      }

-- | Destination of a move operation.
data MoveDst
  -- | Move an item into the top of a group.
  = MoveToTopOfGroup
    { dstParUnique :: !Unique
      -- ^ The group to drop into.
    }
  -- | Move an item outside and below its current parent,
  -- but only if it is the last item in its parent.
  | MoveItemOutsideGroupIfLastItemInGroup
    { itemUnique        :: !Unique
    , itemExpanded      :: !Bool
    , parentUnique      :: !Unique
    , grandParentUnique :: !( Parent Unique )
    }
  -- | Move an item above or below another item.
  | MoveAboveOrBelow
    { moveDstItem :: !ChildLayer
    , moveRelPos  :: !RelativePosition
      -- ^ Whether to move above or below the destination.
    }

--------------------------------------------------------------------------------

-- | Update the layer hierarchy, keeping both the application state and
-- the GTK ListModel in sync.
updateLayerHierarchy :: Variables -> DoLayerChange -> IO ()
updateLayerHierarchy vars@( Variables { parStoresTVar } ) doOrUndo = do
  mbDiff <- STM.atomically $ do

    -- TODO: need to use 'modifyingCurrentDocument' in some form,
    -- but that function doesn't permit modifying history.

    mbActiveDoc <- activeDocument vars
    ( mbNewHist, mbDiff ) <-
      case mbActiveDoc of
        Nothing -> return ( Nothing, Nothing)
        Just ( activeDocUnique, History past ( Document oldPresent oldMeta ) future ) -> do
          let oldHierarchy = strokeHierarchy oldPresent
          case doOrUndo of
            DoLayerChange change -> do
              let !( !hierarchy', !newNames, mbDiff ) = applyChangeToLayerHierarchy change oldHierarchy
                  !content' = oldPresent { strokeHierarchy = hierarchy' }
                  !meta' = over ( field' @"layerMetadata" . field' @"layerNames" ) ( newNames <> ) oldMeta
                  !( !history', mbDoOrUndo ) =
                    case mbDiff of
                      Nothing ->
                        ( History
                            { past    = past
                            , present = Document content' meta'
                            , future  = future
                            }
                        , Nothing )
                      Just diff ->
                        (  History
                            { past    = past Seq.:|> ( oldPresent, HierarchyDiff diff )
                            , present = Document content' meta'
                            , future  = []
                            }
                        , Just ( Do, diff ) )
              return ( Just ( activeDocUnique, history' ), mbDoOrUndo )
            UndoChange -> case past of
              past' Seq.:|> ( present', diff ) -> do
                let !history' =
                      History
                        { past    = past'
                        , present = Document present' oldMeta
                        , future  = ( diff, oldPresent ) : future
                        }
                return
                  ( Just ( activeDocUnique, history' )
                  , case diff of
                      HierarchyDiff hDiff ->
                        Just ( Undo, hDiff )
                      _ -> Nothing
                  )
              Seq.Empty ->
                return ( Nothing, Nothing )
            RedoChange -> case future of
              ( diff, present' ) : future' -> do
                let history' =
                      History
                        { past    = past Seq.:|> ( oldPresent, diff )
                        , present = Document present' oldMeta
                        , future  = future'
                        }
                return
                  ( Just ( activeDocUnique, history' )
                  , case diff of
                      HierarchyDiff hDiff ->
                        Just ( Do, hDiff )
                      _ -> Nothing
                  )
              [] ->
                return ( Nothing, Nothing )

    for_ mbNewHist $ \ ( activeDoc, hist ) -> do
      STM.modifyTVar' ( openDocumentsTVar vars ) ( Map.insert activeDoc hist )
      STM.writeTVar ( redrawStrokesTVar vars ) True
    return $ ( , ) <$> ( fst <$> mbNewHist ) <*> mbDiff

  for_ mbDiff $ \ ( docUnique, diff ) ->
    GLib.idleAdd GLib.PRIORITY_HIGH_IDLE $ do
      applyDiffToListModel parStoresTVar docUnique diff
      return False



-- | Apply a change to the application layer hierarchy.
--
-- The change to the GTK ListModel is done in 'applyDiffToListModel'.
applyChangeToLayerHierarchy :: LayerChange -> StrokeHierarchy -> ( StrokeHierarchy, Map Unique Text, Maybe HierarchyDiff )
applyChangeToLayerHierarchy change hierarchy =
  case change of
    Move
      { moveSrc = WithinParent srcParUniq srcUniq
      , moveDst } ->
      let mbDst =
            case moveDst of
              MoveAboveOrBelow
                { moveRelPos
                , moveDstItem = WithinParent parUniq tgtUniq
                } ->
                  Just ( parUniq
                       , Just ( tgtUniq , moveRelPos )
                       )
              MoveToTopOfGroup
                { dstParUnique } ->
                  Just ( Parent dstParUnique, Nothing )
              MoveItemOutsideGroupIfLastItemInGroup
                { itemUnique, itemExpanded
                , parentUnique, grandParentUnique
                }
                  | Just siblings <- lookupChildren_maybe ( Parent parentUnique ) hierarchy
                  , last siblings == itemUnique
                  -- Only allow this for a group when the group is not expanded
                  -- or it has no children.
                  , let expandedGroupWithChildren
                          | itemExpanded
                          , Just cs <- lookupChildren_maybe ( Parent itemUnique ) hierarchy
                          , not ( null cs )
                          = True
                          | otherwise
                          = False
                  , not expandedGroupWithChildren
                  -> Just ( grandParentUnique, Just ( parentUnique, Below ) )
                  | otherwise
                  -> Nothing
      in case mbDst of
        Nothing ->
          ( hierarchy, Map.empty, Nothing )
        Just ( dstParUniq, mbDstUniq ) ->
          let
            !( !hierarchy', mbChildIxs ) =
              moveLayerUpdate
                ( srcParUniq, srcUniq )
                ( dstParUniq, mbDstUniq )
                hierarchy
          in ( hierarchy'
             , Map.empty
             , case mbChildIxs of
                Just ( srcChildIx, dstChildIx ) ->
                  Just $
                    MoveLayer
                      { moveUnique = srcUniq
                      , srcPos = WithinParent srcParUniq srcChildIx
                      , dstPos = WithinParent dstParUniq dstChildIx
                      }
                Nothing -> Nothing
             )
    NewItem { newUnique = u, newIsGroup, newPosition = ( WithinParent dstParUniq childUniq, relPos ) } ->
      let dropPos = Just ( childUniq, relPos )
          !( !hierarchy', dstChildIx ) = insertLayerIntoParent hierarchy ( dstParUniq, dropPos ) u
          !hierarchy'' =
            if newIsGroup
            then insertGroup ( Parent u ) [] hierarchy'
            else hierarchy'
      in
        ( hierarchy''
        , Map.singleton u ( if newIsGroup then "Group" else "Layer" )
        , Just $
            NewLayer
              { newPosition = WithinParent dstParUniq dstChildIx
              , newUnique = u
              , newIsGroup
              }
        )
    DeleteItems { deleteItems } ->
      let
        !( !hierarchy', delLayers ) = applyDeletionToStrokeHierarchy hierarchy deleteItems
      in
        ( hierarchy'
        , Map.empty
        , Just $ DeleteLayers mempty delLayers
        )
    SetBrush { setBrushStroke = WithinParent parUniq strokeUnique, setBrushName } ->
      case Map.lookup strokeUnique ( content hierarchy ) of
        Just oldStroke
          | Just oldPar_cs <- lookupChildren_maybe parUniq hierarchy
          , Just strokeIx  <- elemIndex strokeUnique oldPar_cs
          ->
          let
            newStroke :: Stroke
            newStroke =
              case setBrushName of
                Just brushNm
                  | Just ( SomeBrush brush ) <- lookupBrush brushNm
                  -> setStrokeBrush ( Just brush ) oldStroke
                _ -> setStrokeBrush Nothing oldStroke
            hierarchy' = hierarchy { content = Map.insert strokeUnique newStroke ( content hierarchy ) }
          in
            ( hierarchy'
            , Map.empty
            , Just $
                StrokeSetBrush
                  { changedBrushStroke = WithinParent parUniq ( fromIntegral strokeIx )
                  , newBrushName = setBrushName
                  }
            )
          | otherwise
          -> trace ( unlines [ "internal error in 'applyChangeToLayerHierarchy' SetBrush"
                             , "could not find index within parent of stroke " ++ show strokeUnique
                             , "parent: " ++ show parUniq ])
            ( hierarchy, Map.empty, Nothing )
        Nothing ->
          trace ( unlines [ "internal error in 'applyChangeToLayerHierarchy' SetBrush"
                          , "no content for stroke with unique " ++ show strokeUnique ])
            ( hierarchy, Map.empty, Nothing )


applyDeletionToStrokeHierarchy :: StrokeHierarchy -> NE.NonEmpty ChildLayer -> ( StrokeHierarchy, Map (Parent Unique) [DeleteLayer] )
applyDeletionToStrokeHierarchy hierarchy0 = go hierarchy0
  where
    go :: StrokeHierarchy -> NE.NonEmpty ChildLayer -> ( StrokeHierarchy, Map (Parent Unique) [DeleteLayer] )
    go h (WithinParent parUniq delUniq NE.:| items) =
      let !( !h', _ )      = removeLayerFromParent h ( parUniq, delUniq )
          !( _, !childIx ) = removeLayerFromParent hierarchy0 ( parUniq, delUniq )
          !( !h'', !mbChildren ) = deleteLayerKey delUniq h'
      in second ( Map.insertWith (++) parUniq
                  [DeleteLayer { delPosition = childIx
                               , delUnique   = delUniq
                               , delIsGroup  = isJust mbChildren
                               }] ) $ ( case items of { [] -> ( h'', Map.empty )
                                                      ; (i:is) -> ( go h'' (i NE.:| is) ) } )

-- | Apply a change to the 'ListModel' underlying the UI
-- representation of the layer hierarchy.
--
-- The change to the application 'StrokeHierarchy' is done beforehand,
-- in 'applyChangeToLayerHierarchy'.
applyDiffToListModel :: HasCallStack
                     => STM.TVar ( Map Unique ( Map ( Parent Unique ) GIO.ListStore ) )
                     -> Unique
                     -> ( Do, HierarchyDiff )
                     -> IO ()
applyDiffToListModel parStoreTVar docUnique ( doOrUndo, diff ) = do
  -- All modifications to the GTK.TreeListModel are done by editing
  -- an appropriate child GIO.ListModel.
  --
  -- We **do not** use the flattened indices returned by GTK.treeListRowGetPosition,
  -- as those are ephemeral (they change when tree expanders are expanded or collapsed).
  -- Instead, we keep track of a mapping
  --
  --   parent --> list store used to hold its children
  --
  -- and use that child list store to perform updates.

  mbParStoreFromUniq <- Map.lookup docUnique <$> STM.readTVarIO parStoreTVar
  case mbParStoreFromUniq of
    Nothing ->
      putStrLn $ unlines
        [ "internal error in 'applyDiffToListModel'"
        , "failed to look up list store map for document with unique " ++ show docUnique ]
    Just parStoreFromUniq -> do
      case diff of
        MoveLayer { srcPos = WithinParent srcPar srcIx
                  , dstPos = WithinParent dstPar dstIx } ->
          case Map.lookup srcPar parStoreFromUniq of
            Nothing ->
              putStrLn $ unlines
                [ "internal error in 'applyDiffToListModel' MoveLayer"
                , "failed to look up list store for source parent " ++ show srcPar ]
            Just srcStore ->
              case Map.lookup dstPar parStoreFromUniq of
                Nothing ->
                  putStrLn $ unlines
                    [ "internal error in 'applyDiffToListModel' MoveLayer"
                    , "failed to look up list store for destination parent " ++ show dstPar ]
                Just dstStore ->
                  case doOrUndo of
                    Do -> do
                      mbItem <- GIO.listModelGetItem srcStore srcIx
                      case mbItem of
                        Nothing ->
                          putStrLn $ unlines
                            [ "internal error in 'applyDiffToListModel' Do MoveLayer"
                            , "failed to get item at index " ++ show srcIx ]
                        Just item -> do
                          GIO.listStoreRemove srcStore srcIx
                          GIO.listStoreInsert dstStore dstIx item
                    Undo -> do
                      mbItem <- GIO.listModelGetItem dstStore dstIx
                      case mbItem of
                        Nothing ->
                          putStrLn $ unlines
                            [ "internal error in 'applyDiffToListModel' Undo MoveLayer"
                            , "failed to get item at index " ++ show dstIx ]
                        Just item -> do
                          GIO.listStoreRemove dstStore dstIx
                          GIO.listStoreInsert srcStore srcIx item
        NewLayer { newPosition = WithinParent dstPar dstIx, newUnique, newIsGroup } -> do
          case Map.lookup dstPar parStoreFromUniq of
            Nothing ->
              putStrLn $ unlines
                [ "internal error in 'applyDiffToListModel' NewLayer"
                , "failed to look up list store for parent " ++ show dstPar ]
            Just dstStore ->
              case doOrUndo of
                Do -> do
                  item <- GI.new LayerItem []
                  GI.gobjectSetPrivateData item ( Just $ if newIsGroup then GroupLayer newUnique else StrokeLayer newUnique )
                  GIO.listStoreInsert dstStore dstIx item
                Undo ->
                  GIO.listStoreRemove dstStore dstIx
        DeleteLayers _delPts delLayers ->
          Map.foldMapWithKey deleteLayersInParent delLayers
            where
              deleteLayersInParent _ [] = return ()
              deleteLayersInParent srcPar (l:ls) =
                case Map.lookup srcPar parStoreFromUniq of
                  Nothing ->
                    putStrLn $ unlines
                      [ "internal error in 'applyDiffToListModel' DeleteLayers"
                      , "failed to look up list store for parent " ++ show srcPar ]
                  Just srcStore ->
                    let sortedLays = NE.sortOn delPosition (l NE.:| ls)
                    in
                      if sequentialOn delPosition sortedLays
                      then do
                        (remLays, addLays) <-
                          case doOrUndo of
                            Do -> return ( fromIntegral ( length sortedLays ) , [] )
                            Undo -> do
                              newLays <- for sortedLays $ \ ( DeleteLayer { delUnique, delIsGroup } ) -> do
                                item <- GI.new LayerItem []
                                GI.gobjectSetPrivateData item
                                  ( Just $ if delIsGroup then GroupLayer delUnique else StrokeLayer delUnique )
                                GObject.toObject item
                              return ( 0, NE.toList newLays )
                        GIO.listStoreSplice srcStore ( delPosition $ NE.head sortedLays ) remLays addLays
                      else case doOrUndo of
                        Do ->
                          -- Delete later items first, otherwise indexing is wrong.
                          for_ ( NE.reverse sortedLays ) $ \ ( DeleteLayer { delPosition = srcIx } ) ->
                            GIO.listStoreRemove srcStore srcIx
                        Undo ->
                          -- Insert earlier items first, otherwise indexing is wrong.
                          for_ sortedLays $ \ ( DeleteLayer { delUnique, delIsGroup, delPosition = srcIx } ) -> do
                            item <- GI.new LayerItem []
                            GI.gobjectSetPrivateData item
                              ( Just $ if delIsGroup then GroupLayer delUnique else StrokeLayer delUnique )
                            GIO.listStoreInsert srcStore srcIx item
        StrokeSetBrush { changedBrushStroke = WithinParent changedPar changedIx } ->
          case Map.lookup changedPar parStoreFromUniq of
            Nothing ->
              putStrLn $ unlines
                [ "internal error in 'applyDiffToListModel' StrokeSetBrush"
                , "failed to look up list store for parent " ++ show changedPar ]
            Just changedStrokeStore ->
              GIO.listModelItemsChanged changedStrokeStore changedIx 0 0

sequentialOn :: forall a i. ( Num i, Ord i ) => ( a -> i ) -> NE.NonEmpty a -> Bool
sequentialOn f ( a NE.:| as ) = go ( f a ) as
  where
    go :: i -> [ a ] -> Bool
    go _ [] = True
    go i (b:bs)
      | let j = f b
      , j == i + 1
      = go j bs
      | otherwise
      = False

-- | Update the 'StrokeHierarchy' after a drag-and-drop operation,
-- moving one layer or group around.
--
-- This handles the application side logic.
-- The UI side is handled in 'dragAndDropListModelUpdate'.
--
-- Returns an updated 'StrokeHierarchy', together with the relative indices
-- of the source and destination items within their respective parents.
-- This information is then used by GTK to update the underlying 'ListModel'.
moveLayerUpdate
  :: ( Parent Unique, Unique )
    -- ^ source
  -> ( Parent Unique, Maybe ( Unique, RelativePosition ) )
    -- ^ destination
    --
    --  - @Nothing@: drop as first element of group
    --  - @Just (u, above)@ drop above/below u
  -> StrokeHierarchy
    -- ^ hierarchy to update
  -> ( StrokeHierarchy, Maybe ( Word32, Word32 ) )
moveLayerUpdate src@( srcPar, srcUniq ) dst@( dstPar, _ ) hierarchy =
  let
    -- Remove the child from its old parent.
    ( hierarchy' , oldChildPos ) = removeLayerFromParent hierarchy src
    -- Add the child to its new parent.
    ( hierarchy'', newChildPos ) = insertLayerIntoParent hierarchy' dst srcUniq
  in
    ( hierarchy''
      -- If the move is a no-op, then return 'Nothing' to avoid
      -- updating the 'GTK.TreeListModel'.
    , if srcPar == dstPar && oldChildPos == newChildPos
      then Nothing
      else Just ( oldChildPos, newChildPos )
    )

-- | Remove a layer or group from its parent in the 'StrokeHierarchy',
-- returning the updated 'StrokeHierarchy' together with the index the item
-- was found within its parent.
--
-- NB: does not delete the layer itself.
removeLayerFromParent :: HasCallStack
                      => StrokeHierarchy
                      -> ( Parent Unique, Unique )
                      -> ( StrokeHierarchy, Word32 )
removeLayerFromParent hierarchy ( parent, u ) =
  let oldPar_cs = lookupChildren parent hierarchy
      newChildren = filter ( /= u ) oldPar_cs
      oldChildPos = case elemIndex u oldPar_cs of
        Nothing -> error $ unlines
                     [ "internal error in 'removeLayerFromParent': could not find index within parent"
                     , "parent: " ++ show parent
                     , "child: " ++ show u
                     , "children of parent: " ++ show oldPar_cs
                     , "hierarchy: " ++ show hierarchy
                     ]
        Just i -> fromIntegral i
  in ( insertGroup parent newChildren hierarchy, oldChildPos )

-- | Add a layer to a parent in the 'StrokeHierarchy', returning the updated
-- 'StrokeHierarchy' together with the index the item was placed within its parent.
--
-- NB: does not add the layer itself.
insertLayerIntoParent :: StrokeHierarchy
                      -> ( Parent Unique, Maybe ( Unique, RelativePosition ) )
                          -- ^ destination
                          --
                          --  - @Nothing@: drop as first element of group
                          --  - @Just (u, above)@ drop above/below u
                      -> Unique
                      -> ( StrokeHierarchy, Word32 )
insertLayerIntoParent hierarchy ( newPar, mbTgtUniq ) srcUniq =
  let
    newPar_oldCs = lookupChildren newPar hierarchy
    ( newPar_newCs, newChildPos ) =
      case mbTgtUniq of
        -- Drop as first child of group.
        Nothing ->
          ( srcUniq : filter ( /= srcUniq ) newPar_oldCs
          , 0
          )
        -- Drop (before or after) given child.
        Just ( tgtUniq, dropRelPos ) ->
          let ( bef, aft ) = break ( == tgtUniq ) $ filter ( /= srcUniq ) newPar_oldCs
          in  ( case dropRelPos of
                  Above -> bef ++ [ srcUniq ] ++ aft
                  Below -> bef ++ take 1 aft ++ [ srcUniq ] ++ drop 1 aft
              , fromIntegral ( length ( takeWhile ( /= tgtUniq ) $ newPar_oldCs ) )
              + case dropRelPos of { Above -> 0; Below -> 1 }
              )
  in ( insertGroup newPar newPar_newCs hierarchy, newChildPos )
