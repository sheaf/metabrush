{-# LANGUAGE OverloadedStrings #-}

module MetaBrush.UI.ToolBar
  ( Tool(..), Mode(..)
  , ToolBar(..), createToolBar
  )
  where

-- base
import Control.Arrow
  ( second )
import Control.Monad
  ( void, when )
import Data.Foldable
  ( for_ )

-- gi-cairo-connector
import qualified GI.Cairo.Render.Connector as Cairo
  ( renderWithContext )

-- gi-gtk
import qualified GI.Gtk as GTK

-- stm
import qualified Control.Concurrent.STM      as STM
  ( atomically )
import qualified Control.Concurrent.STM.TVar as STM
  ( writeTVar )

-- MetaBrush
import MetaBrush.Application.Context
import MetaBrush.Asset.Colours
  ( Colours )
import MetaBrush.Asset.Cursor
  ( drawCursorIcon )
import MetaBrush.Asset.Tools
  ( drawBug, drawBrush, drawMeta, drawPath, drawPen )
import MetaBrush.GTK.Util
  ( widgetAddClass )

--------------------------------------------------------------------------------

createToolBar :: Variables -> Colours -> GTK.Box -> IO ToolBar
createToolBar ( Variables {..} ) colours toolBar = do

  widgetAddClass toolBar "toolBar"

  GTK.widgetSetValign  toolBar GTK.AlignStart
  GTK.widgetSetVexpand toolBar True

  selectionTool <- GTK.toggleButtonNew
  penTool       <- GTK.toggleButtonNew
  GTK.toggleButtonSetGroup penTool ( Just selectionTool )
  makeToggleGroup $
    fmap
      ( second \ toolVal -> STM.atomically do
        STM.writeTVar toolTVar toolVal
        STM.writeTVar partialPathTVar Nothing
        STM.writeTVar redrawStrokesTVar True
      )
      [ ( selectionTool, Selection ), ( penTool, Pen ) ]
  GTK.toggleButtonSetActive selectionTool True

  toolSep1 <- GTK.boxNew GTK.OrientationVertical 0

  pathTool  <- GTK.toggleButtonNew
  brushTool <- GTK.toggleButtonNew
  metaTool  <- GTK.toggleButtonNew
  GTK.toggleButtonSetGroup brushTool ( Just pathTool )
  GTK.toggleButtonSetGroup metaTool  ( Just pathTool )
  makeToggleGroup $
    fmap
      ( second \ modeVal -> STM.atomically do
        STM.writeTVar modeTVar modeVal
        STM.writeTVar partialPathTVar Nothing
        STM.writeTVar redrawStrokesTVar True
      )
      [ ( pathTool, PathMode ), ( brushTool, BrushMode ), ( metaTool, MetaMode ) ]
  GTK.toggleButtonSetActive pathTool True

  toolSep2 <- GTK.boxNew GTK.OrientationVertical 0

  debugTool <- GTK.toggleButtonNew

  _ <- GTK.onButtonClicked debugTool do
    clicked <- GTK.toggleButtonGetActive debugTool
    STM.atomically do
      STM.writeTVar debugTVar clicked
      STM.writeTVar partialPathTVar Nothing
      STM.writeTVar redrawStrokesTVar True

  GTK.boxAppend toolBar selectionTool
  GTK.boxAppend toolBar       penTool
  GTK.boxAppend toolBar      toolSep1
  GTK.boxAppend toolBar      pathTool
  GTK.boxAppend toolBar     brushTool
  GTK.boxAppend toolBar      metaTool
  GTK.boxAppend toolBar      toolSep2
  GTK.boxAppend toolBar     debugTool

  for_ [ selectionTool, penTool, pathTool, brushTool, metaTool, debugTool ] \ tool -> do
    widgetAddClass tool "toolItem"
    GTK.widgetSetFocusOnClick tool False
    GTK.widgetSetFocusable    tool False

  widgetAddClass debugTool "toolItem"

  for_ [ toolSep1, toolSep2 ] \ sep -> do
    widgetAddClass sep "toolBarSeparator"

  GTK.widgetSetTooltipText selectionTool ( Just "Select" )
  GTK.widgetSetTooltipText       penTool ( Just "Draw" )
  GTK.widgetSetTooltipText      pathTool ( Just "Brush path" )
  GTK.widgetSetTooltipText     brushTool ( Just "Brushes" )
  GTK.widgetSetTooltipText      metaTool ( Just "Meta-parameters" )
  GTK.widgetSetTooltipText     debugTool ( Just "Debug mode" )

  selectionToolArea <- GTK.drawingAreaNew
  penToolArea       <- GTK.drawingAreaNew
  pathToolArea      <- GTK.drawingAreaNew
  brushToolArea     <- GTK.drawingAreaNew
  metaToolArea      <- GTK.drawingAreaNew
  debugToolArea     <- GTK.drawingAreaNew

  GTK.buttonSetChild selectionTool ( Just selectionToolArea )
  GTK.buttonSetChild       penTool ( Just       penToolArea )
  GTK.buttonSetChild      pathTool ( Just      pathToolArea )
  GTK.buttonSetChild     brushTool ( Just     brushToolArea )
  GTK.buttonSetChild      metaTool ( Just      metaToolArea )
  GTK.buttonSetChild     debugTool ( Just     debugToolArea )

  GTK.drawingAreaSetDrawFunc selectionToolArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawCursorIcon colours ) cairoContext

  GTK.drawingAreaSetDrawFunc penToolArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawPen colours ) cairoContext

  GTK.drawingAreaSetDrawFunc pathToolArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawPath colours ) cairoContext

  GTK.drawingAreaSetDrawFunc brushToolArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawBrush colours ) cairoContext

  GTK.drawingAreaSetDrawFunc metaToolArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawMeta colours ) cairoContext

  GTK.drawingAreaSetDrawFunc debugToolArea $ Just \ _ cairoContext _ _ ->
    void $ Cairo.renderWithContext ( drawBug colours ) cairoContext

  pure ( ToolBar {..} )

makeToggleGroup :: [ ( GTK.ToggleButton, IO () ) ] -> IO ()
makeToggleGroup buttons =
  for_ buttons \ ( button, action ) ->
    GTK.afterToggleButtonToggled button do
      isActive <- GTK.toggleButtonGetActive button
      GTK.widgetSetSensitive button ( not isActive )
      when isActive action
