{-# LANGUAGE OverloadedStrings #-}

module MetaBrush.UI.Viewport
  ( Viewport(..), ViewportEventControllers(..)
  , createViewport
  , Ruler(..)
  )
  where

-- base
import Data.Foldable
  ( for_ )

-- gi-gtk
import qualified GI.Gtk as GTK

-- MetaBrush
import MetaBrush.GTK.Util
  ( widgetAddClass, widgetAddClasses )
import MetaBrush.Guide
  ( Ruler(..) )

--------------------------------------------------------------------------------

data ViewportEventControllers
  = ViewportEventControllers
  { viewportMotionController :: !GTK.EventControllerMotion
  , viewportScrollController :: !GTK.EventControllerScroll
  , viewportClicksController :: !GTK.GestureClick
  }

data Viewport
  = Viewport
  {    viewportDrawingArea
  , rulerCornerDrawingArea
  ,   leftRulerDrawingArea
  ,    topRulerDrawingArea
    :: !GTK.DrawingArea
  ,    viewportEventControllers
  , rulerCornerEventControllers
  ,   leftRulerEventControllers
  ,    topRulerEventControllers
    :: !ViewportEventControllers
  }

createViewport :: GTK.Grid -> IO Viewport
createViewport viewportGrid = do

  widgetAddClass viewportGrid "viewport"

  rvRulerCorner   <- GTK.revealerNew
  rvLeftRuler     <- GTK.revealerNew
  rvTopRuler      <- GTK.revealerNew
  viewportOverlay <- GTK.overlayNew

  GTK.gridAttach viewportGrid rvRulerCorner   0 0 1 1
  GTK.gridAttach viewportGrid rvLeftRuler     0 1 1 1
  GTK.gridAttach viewportGrid rvTopRuler      1 0 1 1
  GTK.gridAttach viewportGrid viewportOverlay 1 1 1 1

  ----------
  -- Rulers

  rulerCorner <- GTK.boxNew GTK.OrientationVertical   0
  leftRuler   <- GTK.boxNew GTK.OrientationVertical   0
  topRuler    <- GTK.boxNew GTK.OrientationHorizontal 0

  GTK.revealerSetChild rvRulerCorner ( Just rulerCorner )
  GTK.revealerSetChild rvLeftRuler   ( Just leftRuler   )
  GTK.revealerSetChild rvTopRuler    ( Just topRuler    )

  widgetAddClasses rulerCorner [ "ruler", "rulerCorner" ]
  widgetAddClasses leftRuler   [ "ruler", "leftRuler" ]
  widgetAddClasses topRuler    [ "ruler",  "topRuler" ]

  GTK.revealerSetRevealChild rvRulerCorner True
  GTK.revealerSetRevealChild rvLeftRuler   True
  GTK.revealerSetRevealChild rvTopRuler    True

  GTK.revealerSetTransitionType rvRulerCorner GTK.RevealerTransitionTypeSlideLeft
  GTK.revealerSetTransitionType rvLeftRuler   GTK.RevealerTransitionTypeSlideLeft
  GTK.revealerSetTransitionType rvTopRuler    GTK.RevealerTransitionTypeSlideUp

  rulerCornerDrawingArea <- GTK.drawingAreaNew
  GTK.boxAppend rulerCorner rulerCornerDrawingArea

  leftRulerDrawingArea   <- GTK.drawingAreaNew
  GTK.boxAppend leftRuler   leftRulerDrawingArea

  topRulerDrawingArea    <- GTK.drawingAreaNew
  GTK.boxAppend topRuler    topRulerDrawingArea

  GTK.widgetSetHexpand rulerCorner     False
  GTK.widgetSetVexpand rulerCorner     False
  GTK.widgetSetHexpand leftRuler       False
  GTK.widgetSetVexpand leftRuler       True
  GTK.widgetSetHexpand topRuler        True
  GTK.widgetSetVexpand topRuler        False
  GTK.widgetSetHexpand viewportOverlay True
  GTK.widgetSetVexpand viewportOverlay True

  viewportDrawingArea <- GTK.drawingAreaNew
  GTK.overlaySetChild viewportOverlay ( Just viewportDrawingArea )

  -- Adding mouse event controllers to all drawing areas.
  viewportMotion <- GTK.eventControllerMotionNew
  viewportScroll <- GTK.eventControllerScrollNew [ GTK.EventControllerScrollFlagsBothAxes ]
  viewportClicks <- GTK.gestureClickNew
  GTK.gestureSingleSetButton viewportClicks 0
  let
    viewportEventControllers :: ViewportEventControllers
    viewportEventControllers =
      ViewportEventControllers viewportMotion viewportScroll viewportClicks

  rulerCornerMotion <- GTK.eventControllerMotionNew
  rulerCornerScroll <- GTK.eventControllerScrollNew [ GTK.EventControllerScrollFlagsBothAxes ]
  rulerCornerClicks <- GTK.gestureClickNew
  GTK.gestureSingleSetButton rulerCornerClicks 0
  let
    rulerCornerEventControllers :: ViewportEventControllers
    rulerCornerEventControllers =
      ViewportEventControllers rulerCornerMotion rulerCornerScroll rulerCornerClicks

  leftRulerMotion <- GTK.eventControllerMotionNew
  leftRulerScroll <- GTK.eventControllerScrollNew [ GTK.EventControllerScrollFlagsBothAxes ]
  leftRulerClicks <- GTK.gestureClickNew
  GTK.gestureSingleSetButton leftRulerClicks 0
  let
    leftRulerEventControllers :: ViewportEventControllers
    leftRulerEventControllers =
      ViewportEventControllers leftRulerMotion leftRulerScroll leftRulerClicks

  topRulerMotion <- GTK.eventControllerMotionNew
  topRulerScroll <- GTK.eventControllerScrollNew [ GTK.EventControllerScrollFlagsBothAxes ]
  topRulerClicks <- GTK.gestureClickNew
  GTK.gestureSingleSetButton topRulerClicks 0
  let
    topRulerEventControllers :: ViewportEventControllers
    topRulerEventControllers =
      ViewportEventControllers topRulerMotion topRulerScroll topRulerClicks

  for_ [ viewportDrawingArea, rulerCornerDrawingArea,  topRulerDrawingArea ]
    ( `GTK.widgetSetHexpand` True )
  for_ [ viewportDrawingArea, rulerCornerDrawingArea, leftRulerDrawingArea ]
    ( `GTK.widgetSetVexpand` True )

  {-
  -----------------
  -- Viewport scrolling

  viewportScrollbarGrid <- GTK.gridNew
  GTK.overlayAddOverlay viewportOverlay viewportScrollbarGrid
  GTK.overlaySetOverlayPassThrough viewportOverlay viewportScrollbarGrid True

  viewportHScrollbar <- GTK.scrollbarNew GTK.OrientationHorizontal ( Nothing @GTK.Adjustment )
  viewportVScrollbar <- GTK.scrollbarNew GTK.OrientationVertical   ( Nothing @GTK.Adjustment )
  GTK.widgetSetValign  viewportHScrollbar GTK.AlignEnd
  GTK.widgetSetHalign  viewportVScrollbar GTK.AlignEnd
  GTK.widgetSetVexpand viewportVScrollbar True
  GTK.widgetSetHexpand viewportHScrollbar True
  GTK.gridAttach viewportScrollbarGrid viewportHScrollbar 0 1 1 1
  GTK.gridAttach viewportScrollbarGrid viewportVScrollbar 1 0 1 1
  widgetAddClass viewportHScrollbar "viewportScrollbar"
  widgetAddClass viewportVScrollbar "viewportScrollbar"

  -}

  pure ( Viewport {..} )
