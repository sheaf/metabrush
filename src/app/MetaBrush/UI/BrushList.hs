{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}

module MetaBrush.UI.BrushList
  ( newBrushListModel
  , newBrushView
  , getSelectedBrush
  )
  where

-- base
import Control.Monad
  ( void )
import Data.IORef
  ( newIORef, atomicWriteIORef, readIORef )
import Data.Maybe
  ( fromMaybe )
import Data.Typeable
  ( Typeable )

-- gi-gdk
import qualified GI.Gdk as GDK

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gtk
import qualified GI.Gtk as GTK

-- haskell-gi-base
import qualified Data.GI.Base as GI
import qualified Data.GI.Base.GValue as GI

-- stm
import qualified Control.Concurrent.STM as STM

-- text
import Data.Text
  ( Text )
import qualified Data.Text as Text
  ( null, toLower )

-- unordered-containers
import qualified Data.HashMap.Strict as HashMap

-- MetaBrush
import MetaBrush.Application.Context
  ( Variables(selectedBrushTVar) )
import MetaBrush.Brush
  ( SomeBrush, someBrushName )
import qualified MetaBrush.Asset.Brushes as Brushes
  ( brushes, brushesList )
import MetaBrush.GTK.Util
  ( widgetAddClasses )


--------------------------------------------------------------------------------
-- Brush lookup --
------------------

brushFromName :: Text -> Maybe SomeBrush
brushFromName str
  | Text.null str || Text.toLower str == "no brush"
  = Nothing
  | otherwise
  = HashMap.lookup str Brushes.brushes

--------------------------------------------------------------------------------
-- GTK StringList --
--------------------

-- | Create a new 'GTK.StringList' with 'GTK.SingleSelection' to hold
-- the given brush names.
newBrushListModel :: Variables -> [ Text ] -> IO GTK.SingleSelection
newBrushListModel vars brushNames = do

  stringList <- GTK.stringListNew ( Just brushNames )

  selectionModel <- GI.withManagedPtr stringList $ \ slPtr ->
                    GI.withNewObject slPtr $ \ slCopy ->
                    GTK.singleSelectionNew ( Just slCopy )

  void $ GI.after selectionModel ( GI.PropertyNotify #selected ) $ \ _ -> do
    selObj <- GTK.singleSelectionGetSelectedItem selectionModel
    selBrush <-
      case selObj of
        Nothing -> return Nothing
        Just obj -> do
          strObj <- GTK.unsafeCastTo GTK.StringObject obj
          str <- GTK.stringObjectGetString strObj
          return $ brushFromName str
    STM.atomically $ STM.writeTVar ( selectedBrushTVar vars ) selBrush

  return selectionModel

------------------
-- GTK ListView --
------------------

-- | Create a new 'GTK.ListView' that displays brush names.
newBrushView :: Typeable dnd_data
             => GTK.ApplicationWindow
             -> Variables
             -> ( Maybe Text -> dnd_data )
                 -- ^ drag and drop data for the brush
             -> IO GTK.ListView
newBrushView rootWindow vars mkDND_data = mdo

  brushListModel <- newBrushListModel vars ( "No brush" : map someBrushName Brushes.brushesList )

  brushesListFactory <- GTK.signalListItemFactoryNew

  dragPosRef <- newIORef ( 0, 0 )

  -- Connect to "setup" signal to create generic widgets for viewing brush names.
  _ <- GTK.onSignalListItemFactorySetup brushesListFactory $ \ listItem0 -> do

        listItem <- GTK.unsafeCastTo GTK.ListItem listItem0
        GTK.listItemSetFocusable listItem False

        brushBox <- GTK.boxNew GTK.OrientationHorizontal 10
        GTK.listItemSetChild listItem ( Just brushBox )
        widgetAddClasses brushBox [ "brush-item", "monospace" ]

        brushLabel <- GTK.labelNew ( Nothing @Text )
        GTK.boxAppend brushBox brushLabel

        ----------------
        -- DragSource --
        ----------------

        -- Connect signals for starting a drag from this widget.
        dragSource <- GTK.dragSourceNew
        GTK.dragSourceSetActions dragSource [ GDK.DragActionCopy ]

        void $ GTK.onDragSourcePrepare dragSource $ \ x y -> do
          atomicWriteIORef dragPosRef ( x, y )
          brushName <- getBrushName listItem
          val <- GDK.contentProviderNewForValue =<< GIO.toGValue ( GI.HValue $ mkDND_data brushName )
          GTK.widgetAddCssClass rootWindow "dragging-brush"
          return $ Just val
        void $ GTK.onDragSourceDragBegin dragSource $ \ _drag -> do
          ( x, y ) <- readIORef dragPosRef
          paintable <- GTK.widgetPaintableNew ( Just brushBox )
          GTK.dragSourceSetIcon ?self ( Just paintable ) ( round x ) ( round y )
          GTK.widgetAddCssClass brushBox "dragged-brush"
          -- TODO: add "dragged" class for all descendants as well.
        void $ GTK.onDragSourceDragCancel dragSource $ \ _drag _reason -> do
          GTK.widgetRemoveCssClass rootWindow "dragging-brush"
          GTK.widgetRemoveCssClass brushBox "dragged-brush"
          return True
              -- ^^^^ Important. Setting this to 'False' stops GDK
              -- from properly clearing the drag cursor.
        void $ GTK.onDragSourceDragEnd dragSource $ \ _drag _deleteData -> do
          GTK.widgetRemoveCssClass rootWindow "dragging-brush"
          GTK.widgetRemoveCssClass brushBox "dragged-brush"

        GTK.widgetAddController brushBox dragSource

  -- Connect to "bind" signal to modify the generic widget to display the data for this list item.
  _ <- GTK.onSignalListItemFactoryBind brushesListFactory $ \ listItem0 -> do

        listItem <- GTK.unsafeCastTo GTK.ListItem listItem0
        brushName <- getBrushName listItem

        mbBrushBox <- GTK.listItemGetChild listItem
        brushBox <-
          case mbBrushBox of
            Nothing -> error "brushList onBind: list item has no child"
            Just box0 -> GTK.unsafeCastTo GTK.Box box0

        mbBrushLabel <- GTK.widgetGetFirstChild brushBox
        brushLabel <-
          case mbBrushLabel of
            Nothing -> error "brushList onBind: brush box has no child"
            Just lbl0 -> GTK.unsafeCastTo GTK.Label lbl0

        GTK.labelSetLabel brushLabel ( fromMaybe "no brush" brushName )

  listView <- GTK.listViewNew ( Nothing @GTK.SingleSelection ) ( Just brushesListFactory )

  GTK.listViewSetModel listView ( Just brushListModel )

  return listView

getBrushName :: GTK.ListItem -> IO ( Maybe Text )
getBrushName listItem = do
  mbStrObj <- GTK.listItemGetItem listItem
  case mbStrObj of
    Nothing -> error "getBrushName: ListItem has no item"
    Just strObj0 -> do
      strObj <- GTK.unsafeCastTo GTK.StringObject strObj0
      str <- GTK.stringObjectGetString strObj
      return $
        if str == "" || Text.toLower str == "no brush"
        then Nothing
        else Just str

-- | Get the name of the selected brush in the 'ListView', if any.
getSelectedBrush :: GTK.ListView -> IO ( Maybe Text )
getSelectedBrush layersView = do
  mbSelectionModel <- GTK.listViewGetModel layersView
  case mbSelectionModel of
    Nothing -> error "getSelectedBrush: no SelectionModel"
    Just selModel0 -> do
      selModel <- GTK.unsafeCastTo GTK.SingleSelection selModel0
      mbRow <- GTK.singleSelectionGetSelectedItem selModel
      case mbRow of
        Nothing -> return Nothing
        Just row0 -> do
          row <- GTK.unsafeCastTo GTK.TreeListRow row0
          mbItem <- GTK.treeListRowGetItem row
          case mbItem of
            Nothing -> error "getSelectedBrush: row has no item"
            Just item0 -> do
              strObj <- GTK.unsafeCastTo GTK.StringObject item0
              str <- GTK.stringObjectGetString strObj
              return $
                if str == "" || Text.toLower str == "no brush"
                then Nothing
                else Just str
