{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MetaBrush.Brush.Widget
  ( Widget(..)
  , WidgetElements(..)
  , widgetElements
  , widgetUpdate
  , widgetUpdateSync
  , WhatScale(..)
  , WidgetAction(..)
  , describeWidgetAction
  )
  where

-- base
import Data.Fixed
  ( mod' )
import Data.Kind
  ( Type )
import GHC.TypeLits
  ( Symbol )
import GHC.Generics
  ( Generic )

-- acts
import Data.Act
  ( Act((•)), Torsor((-->)) )

-- deepseq
import Control.DeepSeq
  ( NFData )

-- text
import Data.Text
  ( Text )

-- brush-strokes
import Math.Linear
  ( ℝ(..), T(..), Segment(..)
  , rotate
  )
import Math.Module
  ( (^.^), (×)
  , norm
  )

-- metabrushes
import MetaBrush.Records
  ( Record(..) )

--------------------------------------------------------------------------------

-- | A description of a widget to control brush parameters interactively.
--
-- For example, a rotatable rectangle widget to control the size of an
-- elliptical brush.
type Widget :: [ Symbol ] -> Type
data Widget flds where
  SquareWidget
    :: Widget '[ r ]
  RotatableRectangleWidget
    :: !Double -- ^ height offset factor
    -> Widget '[ w, h, phi ]
deriving stock instance Eq   ( Widget flds )
deriving stock instance Ord  ( Widget flds )
deriving stock instance Show ( Widget flds )

-- | Components of a widget that can be displayed visually.
data WidgetElements
  = WidgetElements
  { widgetCentre :: !( T ( ℝ 2 ) )
  , widgetPoints :: ![ T ( ℝ 2 ) ]
  , widgetLines :: ![ ( Segment ( T ( ℝ 2 ) ), Bool ) ]
  }

-- | All UI elements associated to a 'Widget'.
widgetElements :: Widget flds -> Record flds -> WidgetElements
widgetElements widget ( MkR flds ) =
  case widget of
    SquareWidget
      | ℝ1 r <- flds
      -> rectElements 0 r r 0
    RotatableRectangleWidget hOffset
      | ℝ3 w h θ <- flds
      -> rectElements hOffset w h θ
  where
    rectElements hOffset w h θ =
      WidgetElements
        { widgetCentre = rot $ T $ ℝ2 0 ( -hOffset * h )
        , widgetPoints = [ p1, p2, p3, p4 ]
        , widgetLines =
            [ ( Segment p1 p2, True  )
            , ( Segment p2 p3, False )
            , ( Segment p3 p4, True  )
            , ( Segment p4 p1, False )
            ]
        }
      where
        p1 = rot $ T $ ℝ2 -w -h
        p2 = rot $ T $ ℝ2  w -h
        p3 = rot $ T $ ℝ2  w  h
        p4 = rot $ T $ ℝ2 -w  h
        rot = rotate ( cos θ ) ( sin θ )

data WhatScale
  = ScaleXY
  | ScaleX
  | ScaleY
  deriving stock ( Eq, Ord, Show, Generic )
  deriving anyclass NFData

-- | Keep track of state in a brush widget action, e.g.
-- scaling or rotating a brush.
data WidgetAction
  = ScaleAction WhatScale
  | RotateAction
    --{ windingNumber :: Int }
  deriving stock ( Eq, Ord, Show, Generic )
  deriving anyclass NFData

describeWidgetAction :: WidgetAction -> Text
describeWidgetAction ( ScaleAction {} ) = "scaling"
describeWidgetAction RotateAction       = "rotation"

-- | Given an UI action (moving a widget control element),
-- how should we update the brush parameters?
widgetUpdate :: Widget flds
             -> WidgetAction
             -> ( T ( ℝ 2 ), T ( ℝ 2 ) )
               -- ^ ( oldPt, newPt )
             -> Record flds
             -> Record flds
widgetUpdate widget mode ( oldPt, newPt ) ( MkR oldFlds ) =
  case widget of
    SquareWidget
      | T ( ℝ2 x y ) <- newPt
      -> MkR $ ℝ1 ( max 1e-6 $ max ( abs x ) ( abs y ) )
    RotatableRectangleWidget {}
      | ℝ3 w h θ <- oldFlds
      -> case mode of
        ScaleAction whatScale ->
          let T ( ℝ2 w0 h0 ) = rotate ( cos -θ ) ( sin -θ ) newPt
              -- Don't allow width/height to become 0.
              w' = max 1e-6 ( abs w0 )
              h' = max 1e-6 ( abs h0 )
          in case whatScale of
                ScaleXY -> MkR $ ℝ3 w' h' θ
                ScaleX  -> MkR $ ℝ3 w' h  θ
                ScaleY  -> MkR $ ℝ3 w  h' θ
        RotateAction {}
          | norm newPt < 1e-6
          -> MkR oldFlds
          | otherwise
          ->
          let θ1 = atan2 ( oldPt × newPt ) ( oldPt ^.^ newPt )
              θ' = θ + nearestAngle 0 θ1
          in MkR ( ℝ3 w h θ' )

nearestAngle :: Double -> Double -> Double
nearestAngle θ0 θ = θ0 + ( ( θ - θ0 + pi ) `mod'` ( 2 * pi ) - pi )

-- | Synchronise a brush widget update across other points.
widgetUpdateSync
  :: forall flds
  .  ( Show ( Record flds )
     , Torsor ( T ( Record flds ) ) ( Record flds )
     )
  => Bool -> Widget flds -> WidgetAction
  -> ( Record flds, Record flds )
  -> Record flds
  -> Record flds
widgetUpdateSync pressingAlt widget mode ( params0, params1 ) currentParams =
  let
    diff :: T ( Record flds )
    diff = params0 --> params1
    newParams :: Record flds
    newParams =
      if pressingAlt
      then
        -- Alternate mode: synchronise the diff.
        diff • currentParams
      else
        -- Normal mode: synchronise the fields.
        params1
  in
    case widget of
      SquareWidget ->
        -- Synchronise the radius.
        let
          MkR ( ℝ1 r' ) = newParams
        in
          MkR ( ℝ1 $ max 1e-6 r' )
      RotatableRectangleWidget {} ->
        case mode of
          ScaleAction whatScale ->
            -- When scaling, only synchronise width/height.
            let
              MkR ( ℝ3 a b θ   ) = currentParams
              MkR ( ℝ3 a' b' _ ) = newParams
            in
              MkR $
                ℝ3 ( if whatScale == ScaleY then a else max 1e-6 a' )
                   ( if whatScale == ScaleX then b else max 1e-6 b' )
                   θ
          RotateAction {} ->
            -- When rotating, only synchronise the angle.
            let
              MkR ( ℝ3 a b _  ) = currentParams
              MkR ( ℝ3 _ _ θ' ) = newParams
            in
              MkR $ ℝ3 a b θ'
