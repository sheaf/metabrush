module MetaBrush.Util
  ( Exists(..) )
  where

--------------------------------------------------------------------------------

data Exists c where
  Exists :: c a => a -> Exists c
