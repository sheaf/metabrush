{-# OPTIONS_GHC -Wno-orphans #-}

module MetaBrush.Guide where

-- base
import Data.Semigroup
  ( Arg(..), Min(..), ArgMin )

-- acts
import Data.Act
  ( Torsor(..) )

-- containers
import Data.Map.Strict
  ( Map )
import qualified Data.Map.Strict as Map

-- generic-lens
import Data.Generics.Product.Fields
  ( field' )

-- stm
import Control.Concurrent.STM
  ( STM )

-- transformers
import Control.Monad.Trans.Reader
  ( ReaderT, runReaderT )

-- brush-strokes
import Math.Module
  ( Inner((^.^))
  , squaredNorm
  )
import Math.Linear
  ( ℝ(..), T(..) )

-- MetaBrush
import MetaBrush.Document
import MetaBrush.Hover
import MetaBrush.Unique
  ( UniqueSupply, Unique, freshUnique )

--------------------------------------------------------------------------------
-- Guides.

data Ruler
  = RulerCorner
  | LeftRuler
  | TopRuler
  deriving stock Show

-- | Try to select a guide at the given document coordinates.
selectedGuide :: ℝ 2 -> Zoom -> Map Unique Guide -> Maybe ( Unique, Guide )
selectedGuide c zoom guides =
  \case { Min ( Arg _ g ) -> g } <$> Map.foldMapWithKey ( selectGuide_maybe c zoom ) guides

selectGuide_maybe :: ℝ 2 -> Zoom -> unique -> Guide -> Maybe ( ArgMin Double ( unique, Guide ) )
selectGuide_maybe c ( Zoom { zoomFactor } ) u guide@( Guide { guidePoint = p, guideNormal = n } )
  | sqDist * zoomFactor ^ ( 2 :: Int ) < 4
  = Just ( Min ( Arg sqDist ( u, guide ) ) )
  | otherwise
  = Nothing
  where
    t :: Double
    t = ( c --> p ) ^.^ n
    sqDist :: Double
    sqDist = t ^ ( 2 :: Int ) / squaredNorm n

-- | Add new guide after a mouse drag from a ruler area.
addGuide :: UniqueSupply -> Ruler -> ℝ 2 -> Document -> STM Document
addGuide uniqueSupply ruler p doc = ( `runReaderT` uniqueSupply ) $ ( field' @"documentMetadata" . field' @"documentGuides" ) insertNewGuides doc
  where
    insertNewGuides :: Map Unique Guide -> ReaderT UniqueSupply STM ( Map Unique Guide )
    insertNewGuides gs = case ruler of
      RulerCorner
        -> do
          uniq1 <- freshUnique
          uniq2 <- freshUnique
          let
            guide1, guide2 :: Guide
            guide1 = Guide { guidePoint = p, guideNormal = V2 0 1 }
            guide2 = Guide { guidePoint = p, guideNormal = V2 1 0 }
          pure ( Map.insert uniq2 guide2 . Map.insert uniq1 guide1 $ gs )
      TopRuler
        -> do
          uniq1 <- freshUnique
          let
            guide1 :: Guide
            guide1 = Guide { guidePoint = p, guideNormal = V2 0 1 }
          pure ( Map.insert uniq1 guide1 gs )
      LeftRuler
        -> do
          uniq2 <- freshUnique
          let
            guide2 :: Guide
            guide2 = Guide { guidePoint = p, guideNormal = V2 1 0 }
          pure ( Map.insert uniq2 guide2 gs )

instance Hoverable Guide where
  hovered ( MouseHover c ) zoom guide
    | Just {} <- selectGuide_maybe c zoom () guide
    = True
    | otherwise
    = False
  hovered ( RectangleHover {} ) _ _
    = False
