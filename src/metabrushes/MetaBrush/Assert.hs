{-# LANGUAGE CPP #-}

module MetaBrush.Assert
  ( assert )
  where

-- base
#ifdef ASSERTS
import Control.Exception
  ( AssertionFailed(..), throw )
#endif

--------------------------------------------------------------------------------

{-# INLINE assert #-}
assert :: Bool -> String -> a -> a
#ifdef ASSERTS
assert False message _ = throw ( AssertionFailed message )
#else
assert _ _ a = a
#endif
