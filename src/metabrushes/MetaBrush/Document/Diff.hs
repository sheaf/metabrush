{-# LANGUAGE DuplicateRecordFields #-}

module MetaBrush.Document.Diff where

-- base
import qualified Data.List.NonEmpty as NE
import Data.Word
  ( Word32 )
import GHC.Generics
  ( Generic )

-- containers
import Data.Map.Strict
  ( Map )

-- deepseq
import Control.DeepSeq
  ( NFData(..) )

-- text
import Data.Text
  ( Text )

-- brush-strokes
import Math.Bezier.Spline
import Math.Linear
  ( ℝ(..), T(..) )

-- MetaBrush
import qualified MetaBrush.Brush.Widget as Brush
  ( WidgetAction(..) )
import MetaBrush.Document
  ( StrokePoints )
import MetaBrush.Layer
import MetaBrush.Stroke
import MetaBrush.Unique
  ( Unique )

--------------------------------------------------------------------------------

-- | A change to a document.
data Diff
  = TrivialDiff
  | HistoryDiff !HistoryDiff
  deriving stock ( Show, Generic )
  deriving anyclass NFData

-- | A change to a document that affects history.
data HistoryDiff
  = DocumentDiff  !DocumentDiff
  | HierarchyDiff !HierarchyDiff
  | ContentDiff   !ContentDiff
  deriving stock ( Show, Generic )
  deriving anyclass NFData

-- | A change of document (e.g. new document).
data DocumentDiff
  = DocumentCreated
  | DocumentOpened
  deriving stock ( Show, Generic )
  deriving anyclass NFData

-- | A change in the layer hierarchy of a document.
data HierarchyDiff
  = NewLayer
      { newUnique   :: !Unique
      , newIsGroup  :: !Bool
      , newPosition :: !ChildLayerPosition
      }
  | DeleteLayers
    { deletedPoints :: !StrokePoints
    , deletedLayers :: !( Map ( Parent Unique ) [ DeleteLayer ] )
    }
  | MoveLayer
      { moveUnique :: !Unique
      , srcPos :: !ChildLayerPosition
      , dstPos :: !ChildLayerPosition
      }
  | StrokeSetBrush
      { changedBrushStroke :: !ChildLayerPosition
      , newBrushName       :: !( Maybe Text )
      }
  deriving stock ( Show, Generic )
  deriving anyclass NFData

data DeleteLayer
  = DeleteLayer
      { delUnique   :: !Unique
      , delIsGroup  :: !Bool
      , delPosition :: !Word32
      }
  deriving stock ( Show, Generic )
  deriving anyclass NFData

-- | A subdivision of a single stroke.
data Subdivision
  = Subdivision
  { subdividedStroke :: !Unique
  , subdivisions     :: !( NE.NonEmpty ( Rational, Double ) )
  }
  deriving stock ( Show, Generic )
  deriving anyclass NFData

data SelectionMode
  = New
  | Add
  | Subtract
  deriving stock ( Show, Eq, Generic )
  deriving anyclass NFData

instance Semigroup SelectionMode where
  Subtract <> _ = Subtract
  _ <> Subtract = Subtract
  New <> m      = m
  m <> New      = m
  Add <> Add    = Add

instance Monoid SelectionMode where
  mempty = New

-- | A change in the content of strokes in the document.
data ContentDiff
  = Translation
      { translationVector :: !( T ( ℝ 2 ) )
      , translatedPoints  :: !StrokePoints
      }
  | DragMove
      { dragMoveSelection :: !DragMoveSelect
      , dragVector        :: !( T ( ℝ 2 ) )
      , draggedPoints     :: !StrokePoints
      }
  | CloseStroke
      { closedStroke :: !Unique }
  | DeletePoints
      { deletedPoints :: !StrokePoints }
  | ContinueStroke
      { continuedStroke :: !Unique
      , newSegment :: !( Spline Open () ( PointData () ) )
      }
  | UpdateBrushParameters
      { updateBrushStroke :: !Unique
      , updateBrushPoint  :: !PointIndex
      , updateBrushAction :: !Brush.WidgetAction
      }
  | SubdivideStroke !Subdivision
  deriving stock ( Show, Generic )
  deriving anyclass NFData

-- | Type of a drag move selection: point drag or curve drag.
data DragMoveSelect
  -- | User initiated drag by clicking on a point.
  = ClickedOnPoint
    { dragPoint :: !( Unique, PointIndex )
    , dragPointWasSelected :: !Bool
       -- ^ Whether the drag point was already selected
       -- before beginning the drag.
    }
  -- | User initiated drag by clicking on an interior point of a curve;
  -- start a curve drag, selection is preserved.
  | ClickedOnCurve
    { dragStrokeUnique   :: !Unique
    , dragCurve          :: !Rational
    , dragCurveIndex     :: !Int
    , dragCurveParameter :: !Double
    }
  deriving stock ( Show, Eq, Generic )
  deriving anyclass NFData

{-
changeText :: LayerMetadata -> ChangeDescription -> Text
changeText metadata = \case
  TrivialChange -> "(trivial change)"
  HistoryChange ch -> historyChangeText metadata ch

historyChangeText :: LayerMetadata -> HistoryChangeDescription -> Text
historyChangeText metadata = \case
  DocumentCreated -> "Create document"
  DocumentOpened  -> "Open document"
  Translation pathPointsAffected controlPointsAffected strokesAffected ->
    let
      ppMv, cpMv :: Maybe Text
      ppMv
        | pathPointsAffected == 0
        = Nothing
        | otherwise
        = Just ( Text.pack ( show pathPointsAffected ) <> " path points" )
      cpMv
        | controlPointsAffected == 0
        = Nothing
        | otherwise
        = Just ( Text.pack ( show controlPointsAffected ) <> " control points" )
    in "Translate " <> Text.intercalate " and " ( catMaybes [ ppMv, cpMv ] )
    <> " across " <> Text.pack ( show $ length strokesAffected ) <> " strokes"
  DragCurveSegment strokeUnique ->
    "Drag curve segment of stroke " <> ( layerNames metadata Map.! strokeUnique )
-}