{-# LANGUAGE OverloadedStrings #-}

module Main
  ( main )
  where

-- base
import Control.Monad
  ( void, when )
import Data.Maybe
  ( isNothing )
import System.Environment
  ( lookupEnv, setEnv )
import System.Exit
  ( ExitCode(..), exitSuccess, exitWith )
import GHC.Conc
  ( getNumProcessors, setNumCapabilities )

-- code-page
import System.IO.CodePage
  ( withCP65001 )

-- gi-gio
import qualified GI.Gio as GIO

-- gi-gobject
import qualified GI.GObject as GObject

-- gi-gtk
import qualified GI.Gtk as GTK

-- MetaBrush
import MetaBrush.Application
  ( runApplication )

--------------------------------------------------------------------------------

main :: IO ()
main = withCP65001 do

  procs <- getNumProcessors
  let
    caps :: Int
    caps
      | procs >= 6
      = procs - 2
      | procs >= 2
      = procs - 1
      | otherwise
      = procs
  setNumCapabilities caps

  -- Set GDK_SCALE = 2 by default,
  -- unless GDK_SCALE is already set.
  mbGdkScale <- lookupEnv "GDK_SCALE"
  when (isNothing mbGdkScale) $
    setEnv "GDK_SCALE" "2"

  setEnv "GSK_RENDERER" "vulkan"

  ---------------------------------------------------------
  -- Run GTK application

  application <- GTK.applicationNew ( Just "com.calligraphy.MetaBrush" ) [ GIO.ApplicationFlagsNonUnique ]
  GIO.applicationRegister application ( Nothing @GIO.Cancellable )
  void $ GIO.onApplicationActivate application ( runApplication application )
  exitCode <- GIO.applicationRun application Nothing
  GObject.objectUnref application

  case exitCode of
    0 -> exitSuccess
    _ -> exitWith ( ExitFailure $ fromIntegral exitCode )
