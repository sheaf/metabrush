let pkgs = import <nixpkgs> {};
in with pkgs; pkgs.mkShell
  { packages =
  [ zlib
    pkg-config
    gobject-introspection
    gtk4
    pcre
    pcre2
    cairo
    glib
    atk
    util-linux
    libselinux
    libsepol
    xorg.libXdmcp
    libthai
    libdatrie
    epoxy
    fribidi
    xorg.libXi
    xorg.libXrandr
    xorg.libXcursor
    xorg.libXdamage
    xorg.libXinerama
    libxkbcommon ];
  shellHook = ''
    export LC_ALL=C.UTF-8
    export XDG_DATA_DIRS=${gsettings-desktop-schemas}/share/gsettings-schemas/${gsettings-desktop-schemas.name}:${gtk4}/share/gsettings-schemas/${gtk4.name}:$XDG_DATA_DIRS
  '';
}
